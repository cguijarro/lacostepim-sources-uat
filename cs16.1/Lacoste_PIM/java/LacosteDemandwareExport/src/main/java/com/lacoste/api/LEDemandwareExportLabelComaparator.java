package com.lacoste.api;

import java.util.Comparator;
import java.util.Map;


/**
 * This comparator is used to sort the map by values where values started with a number.
 * null or empty labels always wins (nullLast search).
 *
 * @param <E> Type of Value of Map
 *
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class LEDemandwareExportLabelComaparator<E> implements Comparator<Map.Entry<E, String>>
{


  @Override
  public int compare(Map.Entry<E, String> entry1, Map.Entry<E, String> entry2)
  {
    int result = 0;

    if(entry1 == null)
    {
      result = (entry2 == null) ? 0 : 1;
    }
    else if(entry2 == null)
    {
      result = -1;
    }
    else
    {
      String firstLabel = entry1.getValue();
      String secondLabel = entry2.getValue();

      if(empty(firstLabel))
      {
        result = ((empty(secondLabel)) ? 0 : 1);
      }
      else if(empty(secondLabel))
      {
        result = -1;
      }
      else
      {
        boolean isCompared = false;

        int firstIndex = 0;
        int secondIndex = 0;

        int firstLabelLength = firstLabel.length();
        int secondLabelLength = secondLabel.length();

        while(firstIndex < firstLabelLength && secondIndex < secondLabelLength)
        {
          char char1 = firstLabel.charAt(firstIndex);
          char char2 = secondLabel.charAt(secondIndex);

          if(!Character.isDigit(char1) && Character.isDigit(char2))
          {
            result = 1;
            isCompared = true;
            break;
          }
          else if (Character.isDigit(char1) && !Character.isDigit(char2))
          {
            result = -1;
            isCompared = true;
            break;
          }
          else if (!Character.isDigit(char1) && !Character.isDigit(char2))
          {
            if((char1 - char2) != 0)
            {
              result = (char1 - char2);
              isCompared = true;
              break;
            }
          }
          else
          {
            int currentFirstIndex = firstIndex;
            String firstNumber = "";
            while(currentFirstIndex < firstLabelLength && Character.isDigit(firstLabel.charAt(currentFirstIndex)))
            {
              firstNumber += firstLabel.charAt(currentFirstIndex);
              currentFirstIndex++;
            }

            int currentSecondIndex = secondIndex;
            String secondNumber = "";
            while(currentSecondIndex < secondLabelLength && Character.isDigit(secondLabel.charAt(currentSecondIndex)))
            {
              secondNumber += secondLabel.charAt(currentSecondIndex);
              currentSecondIndex++;
            }

            firstIndex = currentFirstIndex;
            secondIndex = currentSecondIndex;

            long first = Long.parseLong(firstNumber);
            long second = Long.parseLong(secondNumber);

            if(first != second)
            {
              result = ((first < second) ? -1 : 1);
              isCompared = true;
              break;
            }
          }

          firstIndex++;
          secondIndex++;
        }

        if (!isCompared)
        {
          result = ((firstLabelLength < secondLabelLength)
            ? -1 :
              ((firstLabelLength > secondLabelLength)
                 ? 1 : 0));
        }
      }
    }

    return result;
  }


  /**
   * Check if string is empty or null.
   *
   * @param text String to be checked.
   *
   * @return TRUE if text is empty or null else FALSE.
   */
  private boolean empty(String text)
  {
    if(text != null && !text.isEmpty())
    {
      return false;
    }
    else
    {
      return true;
    }
  }
}

