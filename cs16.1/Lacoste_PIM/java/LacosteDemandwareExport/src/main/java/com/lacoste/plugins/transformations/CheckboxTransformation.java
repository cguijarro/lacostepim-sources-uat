package com.lacoste.plugins.transformations;

import com.lacoste.api.LETransformationPlugin;
import com.lacoste.api.TreeNode;

/**
 * Transformation plugin to transform the value from number to boolean
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class CheckboxTransformation extends LETransformationPlugin
{


  @Override
  public int getPriority()
  {
    return 1000;
  }


  @Override
  public String transform(TreeNode node, String nodeValue)
  {
    String value = nodeValue;

    if(node.isTransformationFlag())
    {
      if(this.getValue().equalsIgnoreCase("true"))
      {
        value = convertToBoolean(nodeValue);
      }
    }

    return value;
  }


  @Override
  public String transform(TreeNode node, String nodeName, String nodeValue)
  {
    String value = nodeValue;
    if(node.isAttributeTransformationFlag())
    {
      if(this.getValue().equalsIgnoreCase("true"))
      {
        value = convertToBoolean(nodeValue);
      }
    }

    return value;
  }


  /**
   * The value of a check box in CONTENTSERV is "0" or "1".
   * For the export this method transform the value into "false" and "true".
   * 
   * @param value number "0", "1" or ""
   * 
   * @return transformed value
   */
  private String convertToBoolean(String value)
  {
    value = value.trim();

    if(value.equals("0") || value.equals(""))
    {
      return "false";
    }
    else if(value.equals("1"))
    {
      return "true";
    }
    else 
    {
      return "false";
    }
  }
}
