package com.lacoste.api;

import java.util.Arrays;
import java.util.List;

/**
 * Wrapper which holds the list of Item IDs for a batch
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class LEDemandwareExportBatch
{
  private List<String> sizeItemsForCurrentBatch;
  private List<String> productItemsForCurrentBatch;

  public List<String> getSizeItemsForCurrentBatch() {
    return sizeItemsForCurrentBatch;
  }

  public List<String> getProductItemsForCurrentBatch() {
    return productItemsForCurrentBatch;
  }

  public void setSizeItemsForCurrentBatch(List<String> sizeItemsForCurrentBatch) {
    this.sizeItemsForCurrentBatch = sizeItemsForCurrentBatch;
  }

  public void setProductItemsForCurrentBatch(List<String> productItemsForCurrentBatch) {
    this.productItemsForCurrentBatch = productItemsForCurrentBatch;
  }


  /**
   * Gets the exportable items in String format
   * 
   * @return String
   */
  public String getExportedItemIDs()
  {
    return Arrays.toString(LEDemandwareExportXmlApi.exportedItemIDs.toArray());
  }
}
