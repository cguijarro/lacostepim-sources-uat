package com.lacoste.plugins.multiple;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import com.exportstaging.api.domain.Reference;
import com.exportstaging.api.wraper.ItemWrapper;
import com.lacoste.api.LEDemandwareExportApi;
import com.lacoste.api.LEDemandwareExportXmlApi;
import com.lacoste.api.LEMultiplePlugin;
import com.lacoste.api.TreeNode;


/**
 * Multiple Tag Plugin for variation-attribute tag for color attribute
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class ColorVariationAttributeMultiple extends LEMultiplePlugin
{

  private static Map<String, Long> colorData = new HashMap<>();


  @Override
  public List<String> getData(TreeNode node)
  {
    List<String> targetIDs = new ArrayList<>();

    Map<String, Long> colorsPosition = new HashMap<>();

    String defaultColorPositionAttribute = LEDemandwareExportApi.getAttributeColorDefaultPosition();

    try
    {
      String attributeID = node.getValue();
      String idProduct = LEDemandwareExportXmlApi.getValue(attributeID, "");

      List<String> colors = this.getColors(idProduct, attributeID);

      for(String colorID : colors)
      {
        ItemWrapper product = LEDemandwareExportApi.getProductByID(Long.parseLong(colorID));
        if(product != null) {
          List<Reference> references = product.getReferencesByID(LEDemandwareExportApi.getAttributeColor());
          if(references != null) {
            for(Reference reference : references)
            {
              long colorPosition = getDefaultColorPositionByColorID(reference.getTargetID(), defaultColorPositionAttribute);
              colorsPosition.put(reference.getTargetID() + "", colorPosition);
            }
          }
        }
      }
    }
    catch(NumberFormatException exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
      LEDemandwareExportApi.logError(exception.getMessage());
    }

    colorsPosition = sortByValue(colorsPosition);

    targetIDs.addAll(colorsPosition.keySet());

    return targetIDs;
  }


  /**
   * Gets the IDs of PIM product are attached in "color" attribute at colors.
   * 
   * @param idProduct            Value of ID product
   * @param idProductAttributeID "ID product" attribute ID
   * 
   * @return List of IDs
   */
  private List<String> getColors(String idProduct, String idProductAttributeID)
  {
    List<String> data = new LinkedList<>();

    try {
      data = LEDemandwareExportApi.getColorsForIDProduct(
        idProductAttributeID,
        idProduct
      );
    }
    catch(Exception e)
    {
      LEDemandwareExportApi.printStackTrace(e);
    }

    return data;
  }


  /**
   * Gets the default position of color item
   *
   * @param colorElementID                ID of PIM product which is used as a Color item
   * @param defaultColorPositionAttribute Attribute ID which stores the default position of color
   *
   * @return Position of Color
   */
  private long getDefaultColorPositionByColorID(Long colorElementID, String defaultColorPositionAttribute)
  {
    long defaultColorPosition = Long.MAX_VALUE;

    if(!colorData.containsKey(colorElementID + "")) 
    {
      String colorPositionInText = LEDemandwareExportApi.getTargetByID(colorElementID).get(defaultColorPositionAttribute);

      if(colorPositionInText != null && !colorPositionInText.isEmpty())
      {
        try
        {
          defaultColorPosition = Integer.parseInt(colorPositionInText);
        }
        catch (Exception exception) { }
      }

      colorData.put(colorElementID + "", defaultColorPosition);
    }
    else
    {
      defaultColorPosition = colorData.get(colorElementID + "");
    }

    return defaultColorPosition;
  }


  /**
   * Sort Map Keys by Values
   * 
   * @param unsortMap Unsorted Map for sorting
   * 
   * @return Sorted Map by values
   */
  private Map<String, Long> sortByValue(Map<String, Long> unsortMap) {

    List<Map.Entry<String, Long>> list =
            new LinkedList<Map.Entry<String, Long>>(unsortMap.entrySet());

    Collections.sort(list, new Comparator<Map.Entry<String, Long>>() {
      @Override
      public int compare(Entry<String, Long> entry1, Entry<String, Long> entry2)
      {
        return entry1.getValue().compareTo(entry2.getValue());
      }
    });

    Map<String, Long> sortedMap = new LinkedHashMap<String, Long>();
    for (Map.Entry<String, Long> entry : list) {
      sortedMap.put(entry.getKey(), entry.getValue());
    }

    return sortedMap;
  }


  /**
   * Clears Cached data after every batch.
   */
  public static void clearCache()
  {
    colorData.clear();
  }
}
