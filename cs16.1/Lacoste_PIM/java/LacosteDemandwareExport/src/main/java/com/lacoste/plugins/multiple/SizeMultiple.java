package com.lacoste.plugins.multiple;

import java.util.ArrayList;
import java.util.List;

import com.lacoste.api.LEDemandwareExportApi;
import com.lacoste.api.LEDemandwareExportLogger;
import com.lacoste.api.LEDemandwareExportXmlApi;
import com.lacoste.api.LEMultiplePlugin;
import com.lacoste.api.TreeNode;


/**
 * Multiple Tag plugin for product tag to export SIZE items
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class SizeMultiple extends LEMultiplePlugin
{

  private int processingType = 0;

  @Override
  public List<String> getData(TreeNode node)
  {
    String attributesToCheck[] = new String[0];
    List<String> itemIDs;

    if(getAttributesToCheck() == null || getAttributesToCheck().isEmpty())
    {
      itemIDs = LEDemandwareExportXmlApi.getSizeIDs();

      LEDemandwareExportXmlApi.addExportedItemIDs(itemIDs);

      return itemIDs;
    }

    attributesToCheck = getAttributesToCheck().split(",");

    itemIDs = filterSizeIDs(attributesToCheck, this.processingType);

    LEDemandwareExportXmlApi.addExportedItemIDs(itemIDs);

    logSkippedItems(itemIDs);

    return itemIDs;
  }

  /**
   * Gets the comma separated list of attributes.
   *
   * @return String Attribute IDs
   */
  private String getAttributesToCheck()
  {
    return this.getPluginConf().get("AttributesToCheck");
  }

  /**
   * Gets the comma separated list of divisions.
   *
   * @return String Division IDs
   */
  private String getDivisionsForCheck()
  {
    return this.getPluginConf().get("CheckForDivisions");
  }


  /**
   * Gets the comma separated list of valid divisions.
   *
   * @return String Division IDs
   */
  private String getValidDivisions()
  {
    return this.getPluginConf().get("ValidDivisions");
  }


  /**
   * Gets the comma separated list of invalid divisions.
   *
   * @return String Division IDs
   */
  private String getInvalidDivisions()
  {
    return this.getPluginConf().get("InvalidDivisions");
  }


  /**
   * Apply filter on Size IDs bases on Plugin Configurations
   *
   * @param attributesToCheck List of EAN & UPC attributes
   * @param processingType    Type of divisions: 1 - No Divisions 2 - CheckForDivisions, 4 - InvalidDivisions, 3 - ValidDivisions
   *
   * @return List<String> List of filtered Size IDs
   */
  private List<String> filterSizeIDs(String attributesToCheck[], int processingType)
  {
    List<String> sizeIDs = LEDemandwareExportXmlApi.getSizeIDs();
    String divisions[];

    try
    {
      switch(processingType)
      {

        //Filter by No Divisions
        case 1:
          divisions = new String[0];
          sizeIDs = LEDemandwareExportApi.filterSizeIDsForCurrentConfigurations(
            attributesToCheck,
            divisions,
            sizeIDs,
            LEDemandwareExportApi.getAttributeIdDivisions()
          );

        break;

        //Filter by CheckForDivisions
        case 2:
          divisions = getDivisionsForCheck().split(",");
          sizeIDs = LEDemandwareExportApi.filterSizeIDsForCurrentConfigurations(
            attributesToCheck,
            divisions,
            sizeIDs,
            LEDemandwareExportApi.getAttributeIdDivisions()
          );

        break;

      //Filter by ValidDivisions
        case 3:
          divisions = getValidDivisions().split(",");
          sizeIDs = LEDemandwareExportApi.filterSizeIDsByDivisions(
            attributesToCheck,
            divisions,
            sizeIDs,
            true
          );

        break;

        //Filter by InvalidDivisions
        case 4:
          divisions = getInvalidDivisions().split(",");
          sizeIDs = LEDemandwareExportApi.filterSizeIDsByDivisions(
            attributesToCheck,
            divisions,
            sizeIDs,
            false
          );

        break;

        default:
        break;
      }
    }
    catch (Exception e)
    {
      sizeIDs = new ArrayList<>();
    }

    return sizeIDs;
  }


  /**
   * Log skipped items in Process Logs
   *
   * @param exportedItems Exported Items
   */
  private void logSkippedItems(List<String> exportedItems)
  {
    List<String> skippedItems = new ArrayList<String>(LEDemandwareExportXmlApi.getSizeIDs());
    skippedItems.removeAll(exportedItems);

    if(!skippedItems.isEmpty()) {
      LEDemandwareExportLogger.log("Skipped Size items for " + pluginConf.toString() + " : " + skippedItems);
    }
  }


  @Override
  public void parsePluginConf()
  {
    String sPluginConf = getValue();

    if(sPluginConf != null && !sPluginConf.isEmpty())
    {
      String[] confs = sPluginConf.split(";");

      for(String config : confs)
      {
        String configs[] = config.split(":");

        if(configs.length == 2)
        {
          if(configs[1] != null && !configs[1].isEmpty() && configs[0] != null && !configs[0].isEmpty()) {
            this.pluginConf.put(configs[0].trim(), configs[1]);
            this.setProcessingTypeByParamName(configs[0].trim());
          }
        }
      }
    }
  }


  /**
   * Set Division Type by Parameter Name
   *
   * @param paramName
   */
  private void setProcessingTypeByParamName(String paramName)
  {
    switch(paramName)
    {
      case "AttributesToCheck":
        this.setProcessingType(1);
      break;

      case "CheckForDivisions":
        this.setProcessingType(2);
        break;

      case "ValidDivisions":
        this.setProcessingType(3);
        break;

      case "InvalidDivisions":
        this.setProcessingType(4);
        break;
    }
  }


  /**
   * Set processing Type
   *
   * @param divisionType
   */
  private void setProcessingType(int processingType)
  {
    if(this.processingType < processingType)
    {
      this.processingType = processingType;
    }
  }
}
