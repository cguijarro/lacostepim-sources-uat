package com.lacoste.plugins.transformations;

import com.lacoste.api.LEDemandwareExportApi;
import com.lacoste.api.LEDemandwareExportXmlApi;
import com.lacoste.api.LETransformationPlugin;
import com.lacoste.api.TreeNode;


/**
 * Transformation plugin to get the seasonal list for a product for a particular language.
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class SeasonListTransformation extends LETransformationPlugin
{

  @Override
  public int getPriority()
  {
    return 0;
  }


  @Override
  public String transform(TreeNode node, String nodeValue)
  {
    String value = "";
    try
    {
      String ids[] = node.getValue().split(",");
      String seasonListAttributeID = ids[0];
      String codeAttributeID = ids[1];
      String siteID = LEDemandwareExportXmlApi.sites.get(node.getLanguage());
      String periodID = LEDemandwareExportXmlApi.getValue("ActivePeriod" + siteID, "");

      if(periodID != null && !periodID.isEmpty())
      {
        String seasonID = LEDemandwareExportApi.getTargetByID(Long.parseLong(periodID)).get(seasonListAttributeID);
        if(seasonID != null && !seasonID.isEmpty())
        {
          value = LEDemandwareExportApi.getTargetByID(Long.parseLong(seasonID)).get(codeAttributeID);
          value = ((value == null) ? "" : value);
        }
      }
    }
    catch(Exception exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
    }
    return value;
  }


  @Override
  public String transform(TreeNode node, String nodeName, String nodeValue)
  {
    return nodeValue;
  }
}
