package com.lacoste.api;

import java.nio.file.Paths;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.lacoste.exception.LEDemandwareExportException;

/**
 * Base plugin for Stream Export. 
 * Provides generic method to implement Stream specific configurations and Export.
 *
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public abstract class LEStreamExportPlugin
{

  public static final String ConfigurationsOutputLocation = "Configurations.OutputLocation";
  public LEDemandwareExportXmlApi api = null;


  /////////////////////////////////////ABSTRACT METHODS/////////////////////////////////////////////////////////////

  /**
   * Gets the Items batches as per the batch size
   *
   * @return Object of LEDemandwareExportBatch
   *
   * @throws LEDemandwareExportException on error
   */
  public abstract List<LEDemandwareExportBatch> getExportableItemBatches() throws LEDemandwareExportException;


  /**
   * Gets the name of output file as per the name conventions
   *
   * @param batchID Batch ID
   *
   * @return Name of output XML file
   */
  public abstract String getOuputXmlName(int batchID);


  /////////////////////////////////////PUBLIC METHODS///////////////////////////////////////////////////////////////

  /**
   * Gets the Name of Stream will be configurable in configurations XML
   *
   * @return Name of stream
   */
  public abstract String getName();


  /**
   * Reads stream specific configuration
   *
   * @throws LEDemandwareExportException on error
   */
  public void parseConfigXml() throws LEDemandwareExportException
  {
    api.parseConfigXml(this.getName());
  }


  /**
   * Generate XML files for exportable items
   *
   * @throws LEDemandwareExportException  on error
   * @throws ParserConfigurationException on parsing error
   * @throws TransformerException         on transformer error
   */
  public void generateXML() throws LEDemandwareExportException, ParserConfigurationException, TransformerException
  {
    LEDemandwareExportLogger.log("-------------------------------------------------------------------");
    LEDemandwareExportLogger.log("Started Demandware Export for Stream: '" + this.getName() + "'");

    List<LEDemandwareExportBatch> exportableItemBatches = this.getExportableItemBatches();

    LEDemandwareExportXmlTree sampleXml = LEDemandwareExportXmlApi.structure;

    int batchID = 1;
    for(LEDemandwareExportBatch exportableItemBatch : exportableItemBatches)
    {
      String outputXmlName = this.getOuputXmlName(batchID);

      LEDemandwareExportLogger.log("Processing Batch ID - " + batchID);

      LEDemandwareExportXmlApi demandwareExportXmlApi = new LEDemandwareExportXmlApi();
      LEDemandwareExportXmlApi.setSizeIDs(exportableItemBatch.getSizeItemsForCurrentBatch());
      LEDemandwareExportXmlApi.setProductIDs(exportableItemBatch.getProductItemsForCurrentBatch());

      Document xmlDocument = demandwareExportXmlApi.getXmlDocument();
      Element element = sampleXml.createXmlFile(sampleXml.getRoot(), demandwareExportXmlApi, xmlDocument);
      xmlDocument.appendChild(element);
      demandwareExportXmlApi.transferXmlToFile(xmlDocument, outputXmlName);

      LEDemandwareExportLogger.log("Exported Item IDs: " + exportableItemBatch.getExportedItemIDs());
      LEDemandwareExportLogger.log("XML created: " + Paths.get(outputXmlName).toString());
      LEDemandwareExportLogger.log("End Processing Batch ID - " + batchID);

      LEDemandwareExportXmlApi.clearCache();
      batchID++;
    }
    LEDemandwareExportLogger.log("Ended Demandware Export for Stream: '" + this.getName() + "'");
    LEDemandwareExportLogger.log("-------------------------------------------------------------------");
  }


  /**
   * Gets the API object
   *
   * @return LEDemandwareExportXmlApi instance
   */
  public LEDemandwareExportXmlApi getApi() {
    return api;
  }


  /**
   * Sets the API object
   *
   * @param api LEDemandwareExportXmlApi
   */
  public void setApi(LEDemandwareExportXmlApi api) {
    this.api = api;
  }
}
