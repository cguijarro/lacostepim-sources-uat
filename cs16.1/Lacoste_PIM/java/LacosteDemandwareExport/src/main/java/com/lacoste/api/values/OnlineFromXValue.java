package com.lacoste.api.values;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class provides APIs to extract this information for value and saves it in a object for future use.
 * 
 * The X-VALUE information of a online-from tag for a given site-id
 * contains following information:
 *   1. Active Attribute to search Period items to calculate begin date and end date
 *   2. Begin Date attribute for given site ID
 *   3. End Date attribute for given site ID
 *   4. Site ID
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class OnlineFromXValue
{

  private static Map<String, OnlineFromXValue> onlineFromXValueCache = new HashMap<>(); 

  String startKey;
  String endKey;
  String siteID;
  List<String> activeAttributes;
  String startDate;
  String endDate;
  String activePeriod;


  /**
   * Extract the various information from value & stored it in a object
   * 
   * @param value Value of online-from tag for a site ID (For eg. ITonline-from1174|1175|1176.1063-1062)
   */
  public static void parseSeasonalDataValue(String value)
  {
    OnlineFromXValue onlineFrom = new OnlineFromXValue();
    String values[]             = value.split("_");
    String startKey             = values[0];

    if(value.contains("online-from"))
    {
      String result[]      = startKey.split("online-from");
      String siteID        = "SeasonalData" + result[0];
      String endDateKey    = startKey;
      endDateKey           = endDateKey.replaceAll("online-from", "online-to");
      String attList[]     = values[1].split("\\.");
      String activeAtts[]  = attList[0].split("\\|");
      String startEndAtt[] = attList[1].split("-");

      onlineFrom.setStartKey(startKey);
      onlineFrom.setEndKey(endDateKey);
      onlineFrom.setStartDate(startEndAtt[0]);
      onlineFrom.setEndDate(startEndAtt[1]);
      onlineFrom.setSiteID(siteID);
      onlineFrom.setActivePeriod("ActivePeriod" + result[0]);
      List<String> list = Arrays.asList(activeAtts);
      list = ((list != null) ? list : new ArrayList<String>());
      onlineFrom.setActiveAttributes(list);

      onlineFromXValueCache.put(value, onlineFrom);
    }
  }


  /**
   * Gets the instance of OnlineFromXValue from cache
   * 
   * @param value Value of online-from attribute for given ID
   * 
   * @return object of OnlineFromXValue
   */
  public static OnlineFromXValue getOnlineFromXValueComponentsFromCache(String value)
  {

    if(!onlineFromXValueCache.containsKey(value))
    {
      parseSeasonalDataValue(value);
    }

    return onlineFromXValueCache.get(value);
  }


  public String getStartKey()
  {
    return startKey;
  }


  public String getEndKey() {
    return endKey;
  }


  public String getSiteID() {
    return siteID;
  }


  public List<String> getActiveAttributes() {
    return activeAttributes;
  }


  public String getStartDate() {
    return startDate;
  }


  public String getEndDate() {
    return endDate;
  }


  public void setStartKey(String startKey) {
    this.startKey = startKey;
  }


  public void setEndKey(String endKey) {
    this.endKey = endKey;
  }


  public void setSiteID(String siteID) {
    this.siteID = siteID;
  }


  public void setActiveAttributes(List<String> activeAttributes) {
    this.activeAttributes = activeAttributes;
  }


  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }


  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }


  public String getActivePeriod() {
    return activePeriod;
  }


  public void setActivePeriod(String activePeriod) {
    this.activePeriod = activePeriod;
  }


  public static void clearCache()
  {
    onlineFromXValueCache = new HashMap<>(); 
  }
}
