package com.lacoste.plugins.transformations;

import com.lacoste.api.LEDemandwareExportXmlApi;
import com.lacoste.api.LETransformationPlugin;
import com.lacoste.api.TreeNode;

/**
 * Transformation plugin to append static text to the value of Tag
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class AppendTransformation extends LETransformationPlugin
{


  @Override
  public int getPriority()
  {
    return 1000;
  }


  @Override
  public String transform(TreeNode node, String nodeValue)
  {
    String value = nodeValue;
    if(node.isTransformationFlag())
    {
      if(!this.getValue().isEmpty() && !value.isEmpty())
      {
        value += LEDemandwareExportXmlApi.getValue(this.getValue(), node.getNodeName());
      }
    }

    return value;
  }


  @Override
  public String transform(TreeNode node, String nodeName, String nodeValue)
  {
    String value = nodeValue;
    if(node.isAttributeTransformationFlag())
    {
      if(!this.getValue().isEmpty() && !value.isEmpty())
      {
        value += LEDemandwareExportXmlApi.getValue(this.getValue(), nodeName, node.getLanguage());
      }
    }

    return value;
  }
}
