package com.lacoste.plugins.multiple;

import java.util.ArrayList;
import java.util.List;

import com.lacoste.api.LEDemandwareExportApi;
import com.lacoste.api.LEDemandwareExportLogger;
import com.lacoste.api.LEDemandwareExportXmlApi;
import com.lacoste.api.LEMultiplePlugin;
import com.lacoste.api.TreeNode;


/**
 * Multiple Tag plugin for product tag to export PRODUCT items
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class ProductMultiple extends LEMultiplePlugin
{

  private static String ATTRIBUTES_TO_CHECK_IN_SKU = "AttributesToCkeckInSku";
  private static String VALID_DIVISION             = "ValidDivisions";
  private static String INVALID_DIVISIONS          = "InvalidDivisions";


  @Override
  public List<String> getData(TreeNode node)
  {
    List<String> itemIDs;
    if(getPluginConf().isEmpty())
    {
      itemIDs = LEDemandwareExportXmlApi.getProductIDs();
    }
    else
    {
      if(!isEmpty(getInvalidDivisions()))
      {
        String[] attributesToCheck = split(getAttributesToCheckInSku());
        String[] invalidDivisions  = split(getInvalidDivisions());

        itemIDs = LEDemandwareExportApi.filterProductIDsForCurrentConfigurations(
          attributesToCheck,
          invalidDivisions,
          LEDemandwareExportXmlApi.getProductIDs(),
          false
        );
      }
      else
      {
        String[] attributesToCheck = split(getAttributesToCheckInSku());
        String[] validDivisions    = split(getValidDivisions());

        itemIDs = LEDemandwareExportApi.filterProductIDsForCurrentConfigurations(
          attributesToCheck,
          validDivisions,
          LEDemandwareExportXmlApi.getProductIDs(),
          true
        );
      }
    }

    LEDemandwareExportXmlApi.addExportedItemIDs(itemIDs);

    logSkippedItems(itemIDs);

    return itemIDs;
  }


  /**
   * Gets the comma separated list of attributes to check in SKU.
   *
   * @return String Attributes IDs
   */
  public String getAttributesToCheckInSku()
  {
    return this.getPluginConf().get(ATTRIBUTES_TO_CHECK_IN_SKU);
  }


  /**
   * Gets the comma separated list of valid divisions.
   *
   * @return String Division IDs
   */
  public String getValidDivisions()
  {
    return this.getPluginConf().get(VALID_DIVISION);
  }


  /**
   * Gets the comma separated list of invalid divisions.
   *
   * @return String Invalid Division IDs
   */
  public String getInvalidDivisions()
  {
    return this.getPluginConf().get(INVALID_DIVISIONS);
  }


  /**
   * Checks of string in empty or null
   *
   * @param stringToCheck String to check
   *
   * @return boolean TRUE is string is empty ELSE false
   */
  public boolean isEmpty(String stringToCheck)
  {
    return (stringToCheck == null || stringToCheck.isEmpty());
  }


  /**
   * Splits string to array
   *
   * @param stringToSplit String to split in which data is separated by comma.
   *
   * @return boolean Array of Strings
   */
  public String[] split(String stringToSplit)
  {
    return (isEmpty(stringToSplit) ? new String[0] : stringToSplit.split(","));
  }


  /**
   * Log skipped items in Process Logs
   *
   * @param exportedItems Exported Items
   */
  private void logSkippedItems(List<String> exportedItems)
  {
    List<String> skippedItems = new ArrayList<String>(LEDemandwareExportXmlApi.getProductIDs());
    skippedItems.removeAll(exportedItems);

    if(!skippedItems.isEmpty()) {
      LEDemandwareExportLogger.log("Skipped Product items for " + pluginConf.toString() + " : " + skippedItems);
    }
  }
}
