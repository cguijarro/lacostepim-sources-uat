package com.lacoste.plugins.transformations;

import com.lacoste.api.LEDemandwareExportApi;
import com.lacoste.api.LEDemandwareExportXmlApi;
import com.lacoste.api.LETransformationPlugin;
import com.lacoste.api.TreeNode;

/**
 * Transformation plugin to get the value of a attribute in only english language
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class GetDefaultValueTransformation extends LETransformationPlugin
{


  @Override
  public int getPriority()
  {
    return 0;
  }


  @Override
  public String transform(TreeNode node, String nodeValue)
  {
    String value = nodeValue;
    if(node.isTransformationFlag())
    {
      if(!this.getValue().isEmpty() && !value.isEmpty())
      {
        value = LEDemandwareExportXmlApi.getValue(node.getValue(), node.getNodeName(), LEDemandwareExportApi.LANGUAGE_CODE);
      }
    }

    return value;
  }


  @Override
  public String transform(TreeNode node, String nodeName, String nodeValue)
  {
    String value = "";
    if(node.isAttributeTransformationFlag())
    {
      if(!this.getValue().equals("true"))
      {
        value = LEDemandwareExportXmlApi.getValue(node.getAttributes().get(nodeName), node.getNodeName(), LEDemandwareExportApi.LANGUAGE_CODE);
      }
    }

    return value;
  }
}
