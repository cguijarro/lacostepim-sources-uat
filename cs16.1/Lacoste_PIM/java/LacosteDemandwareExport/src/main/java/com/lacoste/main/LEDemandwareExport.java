package com.lacoste.main;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.lacoste.api.LEDemandwareExportApi;
import com.lacoste.api.LEDemandwareExportLogger;
import com.lacoste.api.LEDemandwareExportMutex;
import com.lacoste.api.LEDemandwareExportXmlApi;
import com.lacoste.api.LEStreamExportPlugin;


/**
 * Main class for Demandware Export process
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class LEDemandwareExport
{


  /**
   * Starting point of the Demandware Export Process.
   * 
   * @param args
   */
  public static void main(String[] args)
  {
    boolean isLockAcquired = false;
    try
    {
      isLockAcquired = LEDemandwareExportMutex.acquire();

      LEDemandwareExportLogger.log("Started Demadware Export XML creation Process");

      LEDemandwareExportXmlApi demandwareExportXmlApi = new LEDemandwareExportXmlApi();
      demandwareExportXmlApi.parseMainConfigXml();

      List<String> sizeIDs = LEDemandwareExportApi.getUpdatedSizeElements();
      LEDemandwareExportApi.getExportableProductItems(sizeIDs);
      
      LinkedHashMap<String, LEStreamExportPlugin> sortedStreams = new LinkedHashMap<>();
      
      LEDemandwareExportXmlApi.streams.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEachOrdered(x -> sortedStreams.put(x.getKey(), x.getValue()));

      for(LEStreamExportPlugin stream : sortedStreams.values())
      {
        stream.setApi(demandwareExportXmlApi);
        stream.parseConfigXml();
        stream.generateXML();

        LEDemandwareExportXmlApi.clearCacheAfterStream();
        LEDemandwareExportApi.clearCacheManager();
      }

      demandwareExportXmlApi.storeConfig(LEDemandwareExportApi.getRunStartTimestamp());
    }
    catch(InterruptedException e)
    {
      LEDemandwareExportLogger.log(e.getMessage());
      LEDemandwareExportApi.printStackTrace(e);
      return;
    }
    catch(Exception exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
      LEDemandwareExportLogger.log(exception.getMessage());
    }
    finally
    {
      if(isLockAcquired)
      {
        LEDemandwareExportMutex.release();
      }

      LEDemandwareExportApi.closeCacheManager();
      LEDemandwareExportApi.closeExportApi();
      LEDemandwareExportLogger.log("Demandware Export XML creation finished");
    }
  }
}