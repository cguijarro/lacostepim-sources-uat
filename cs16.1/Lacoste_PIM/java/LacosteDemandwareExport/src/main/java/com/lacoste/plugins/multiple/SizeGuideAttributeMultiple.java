package com.lacoste.plugins.multiple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.lacoste.api.LEDemandwareExportApi;
import com.lacoste.api.LEDemandwareExportLogger;
import com.lacoste.api.LEDemandwareExportXmlApi;
import com.lacoste.api.LEMultiplePlugin;
import com.lacoste.api.TreeNode;

public class SizeGuideAttributeMultiple extends LEMultiplePlugin
{

  @Override
  public List<String> getData(TreeNode node)
  {
    List<String> subNodeIDs = new ArrayList<>();

    try
    {
      String attributeIDs = node.getValue();
      String attributeList[] = attributeIDs.split(",");
      String attributeID = attributeList[0];
      String sortingAttribute = attributeList[1];

      long productID = Long.parseLong(LEDemandwareExportXmlApi.getData().get("PdmarticleID"));
      Map<String, String> product = LEDemandwareExportApi.getTargetByID(productID);

      if(product.get(attributeID) == null)
      {
        return subNodeIDs;
      }
      else
      {
        for(String referenceID : Arrays.asList(product.get(attributeID).split(",")))
        {
          subNodeIDs.addAll(LEDemandwareExportApi.getItemFirstLevelNodesIDs(referenceID, sortingAttribute));
        }
      }

    }
    catch(Exception exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
      LEDemandwareExportLogger.log("Invalid configurations for 'SizeGuideAttribute' multiple plugin.");
    }

    return subNodeIDs;
  }
}
