package com.lacoste.api;

import org.apache.log4j.Logger;

/**
 * Logger to write Demandware Export logs into process logs
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class LEDemandwareExportLogger
{

  private static Logger logger = Logger.getLogger(LEDemandwareExportApi.class.getName());


  /**
   * Writes the DemandwareExport logs in Process Logs
   * 
   * @param message Log message
   */
  public static void log(String message)
  {
    logger.error(message);
  }
}
