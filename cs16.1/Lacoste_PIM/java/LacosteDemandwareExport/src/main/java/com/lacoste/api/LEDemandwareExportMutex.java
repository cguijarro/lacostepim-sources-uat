package com.lacoste.api;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;


/**
 * Add Mutex Lock on Demandware export process & releases at the End 
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class LEDemandwareExportMutex
{

  /**
   * Acquire lock on Demandware export process
   * 
   * @throws InterruptedException if process is already running
   */
   public static boolean acquire() throws InterruptedException
   {
     String lockFileName = LEDemandwareExportConstants.LOCK_FILE;
     boolean isLockAcquired = false;
     try
     {
       File file = new File(lockFileName);

       if(!file.exists())
       {
         file.getParentFile().mkdirs();
         String lockMessage = "";
         Files.write(
           Paths.get(lockFileName),
           lockMessage.getBytes(),
           StandardOpenOption.APPEND,
           StandardOpenOption.CREATE
         );
         isLockAcquired = true;
       }
       else
       {
         throw new InterruptedException("Demandware Export could not be executed"
           + " as it is already running in another process."
           + " Please check the lock file : "
           + file.getCanonicalPath());
       }
     }
     catch(IOException e)
     {
        throw new InterruptedException(e.getMessage());
     }

     return isLockAcquired;
   }


   /**
    * Release lock on Demandware export process
    */
   public static void release()
   {
     String lockFileName = LEDemandwareExportConstants.LOCK_FILE;
     File file = new File(lockFileName);

     file.delete();
   }
}