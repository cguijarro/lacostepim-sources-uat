package com.lacoste.plugins.transformations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.lacoste.api.LEDemandwareExportApi;
import com.lacoste.api.LEDemandwareExportXmlApi;
import com.lacoste.api.LETransformationPlugin;
import com.lacoste.api.TreeNode;


/**
 * Transformation plugin which works on two levels deep (e.g. "71.1028")
 * and then retrieves data from the third level (e.g. "1056").
 * User need to configure the attribute IDs separated by "." i.e. "71.1028.1056".
 *
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class ColorGroupingTransformation extends LETransformationPlugin
{

  private static Map<String, List<String>> data = new HashMap<>();

  @Override
  public int getPriority()
  {
    return 0;
  }


  @Override
  public String transform(TreeNode node, String nodeValue)
  {
    String value = node.getValue();
    if(node.isTransformationFlag())
    {
      if(!this.getValue().isEmpty() && this.getValue().equalsIgnoreCase("true"))
      {
        value = value.replaceAll(node.getLanguage(), "");
        value = getTransformedValue(value, node.getLanguage());
      }
    }

    return value;
  }


  @Override
  public String transform(TreeNode node, String nodeName, String nodeValue)
  {
    String attributeValue = node.getAttributes().get(nodeName);
    if(node.isAttributeTransformationFlag())
    {
      if(this.getValue().equalsIgnoreCase("true") && !attributeValue.isEmpty())
      {
        attributeValue = attributeValue.replaceAll(node.getLanguage(), "");
        attributeValue = getTransformedValue(attributeValue, node.getLanguage());
      }
    }

    return attributeValue;
  }


  /**
   * Gets the transformed node value
   *
   * @param nodeValue    Three attribute IDs separated by dot (.)
   * @param languageCode Language Code
   *
   * @return Transformed node value.
   */
  private String getTransformedValue(String nodeValue, String languageCode)
  {
    List<String> list       = getAttributes(nodeValue);
    String transformedValue = "";

    if(list != null) {
      list = appendLanguageCode(list, languageCode);
      String firstLevelID  = list.get(0);
      String secondLevelID = list.get(1);
      String thirdLevelID  = list.get(2);
      String targetID      = getFirst(LEDemandwareExportXmlApi.getValue(firstLevelID, ""));
      if(!empty(targetID)) {
        String itemValue = getItemValue(targetID, secondLevelID);
        String division = getFirst(itemValue);
        if(!empty(division)) {
          transformedValue = getItemValue(division, thirdLevelID);
        }
      }
    }

    return transformedValue;
  }


  /**
   * Split attribute IDs
   *
   * @param nodeValue Three attribute IDs separated by dot (.)
   *
   * @return List containing three IDs
   */
  private List<String> getAttributes(String nodeValue)
  {
    List<String> list = null;
    if(data.containsKey(nodeValue))
    {
      return data.get(nodeValue);
    }
    else
    {
      if(nodeValue != null && !nodeValue.isEmpty())
      {
        String attributes[]  = nodeValue.split("\\.");

        if(attributes.length == 3) {
          list = new ArrayList<>();

          list.add(attributes[0]);
          list.add(attributes[1]);
          list.add(attributes[2]);

          data.put(nodeValue, list);
        }
      }

      return list;
    }
  }


  /**
   * Append Language code to all attribute IDs
   *
   * @param list         List of attribute IDs
   * @param languageCode Language ID
   *
   * @return List of formatted attribute IDs
   */
  private List<String> appendLanguageCode(List<String> list, String languageCode)
  {

    if(list != null) {
      list = list.stream()
      .map(
        s-> {
          String n = new String(s);
          return n + languageCode;
        }
      )
     .collect(Collectors.toList());
    }

    return list;
  }


  /**
   * Gets the first item IDs from List of IDs
   *
   * @param itemIDs Item IDs separated by comma (,).
   *
   * @return Item ID First item ID from list
   */
  private String getFirst(String itemIDs)
  {
    if(itemIDs != null)
    {
      String array[] = itemIDs.split(",");
      if(array.length != 0)
      {
        return array[0];
      }
    }

    return "";
  }


  /**
   * Gets the value of attribute for given item ID
   *
   * @param itemID      Product ID
   * @param attributeID Attribute ID
   *
   * @return Attribute value
   */
  private String getItemValue(String itemID, String attributeID)
  {
    String value = "";
    try {
      value = LEDemandwareExportApi.getTargetByID(Long.parseLong(itemID)).get(attributeID);
    }
    catch(Exception exception) {
      LEDemandwareExportApi.printStackTrace(exception);
    }

    return ((value != null) ? value : "");
  }


  /**
   * Check if text is empty or null
   *
   * @param text String
   *
   * @return TRUE if text is null or empty, else FALSE
   */
  private boolean empty(String text)
  {
    if(text == null || text.isEmpty())
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}
