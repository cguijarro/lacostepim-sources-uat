package com.lacoste.api;


/**
 * Base class provides functions definition for Transformation Plugin.
 * New transformation plugin must extend this class.
 * The new transformation plugin must be located in package "com.lacoste.plugins.transformations".
 * Name of the new transformation plugin must ends with "Transformation".
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public abstract class LETransformationPlugin
{

  /**
   * Value of transformation must be configured in Config XML.
   */
  String value = "";

  /**
   * Need to override in subclass to execute transformation logic.
   * 
   * @param node      Node represents a XML tag.
   * @param nodeValue Node value.
   * 
   * @return Transformed node value.
   */
  public abstract String transform(TreeNode node, String nodeValue);


  /**
   * Need to override in subclass to execute transformation logic.
   * 
   * @param node      Node represents a XML tag.
   * @param nodeName  Node name for attribute transformation
   * @param nodeValue Node value.
   * 
   * @return Transformed node value.
   */
  public abstract String transform(TreeNode node, String nodeName, String nodeValue);

  /**
   * Need to override to set Priority of transformation plugin over other plugin.
   * 
   * @return Priority of the plugin.
   */
  public abstract int getPriority();


  /**
   * Gets the value of transformation plugin configured in Config XML
   * 
   * @return value of transformation plugin
   */
  public String getValue() {
    return this.value;
  }


  /**
   * Sets the value of transformation plugin configured in Config XML
   * 
   * @param value value of transformation plugin
   */
  public void setValue(String value) {
    this.value = value;
  }
}
