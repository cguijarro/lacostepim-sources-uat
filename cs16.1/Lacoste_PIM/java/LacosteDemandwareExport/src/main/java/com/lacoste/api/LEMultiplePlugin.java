package com.lacoste.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Base interface provides functions definition for Multiple tag Plugin.
 * New Multiple tag plugin must implement this interface.
 * The new Multiple tag plugin must be located in package "com.lacoste.plugins.multiple".
 * Name of the new transformation plugin must ends with "Multiple".
 * User can also provide plugin specific parameters.
 *
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public abstract class LEMultiplePlugin
{

  protected Map<String, String> pluginConf = new HashMap<>();

  protected String value = "";

  public abstract List<String> getData(TreeNode node);

  /**
   * Gets Plugin value configured in XML structure
   * Format: <key>:<value>;<key>:<value>;...
   * Eg. X-MULTIPLE="Size=AttributesToCheck:1155,1156;CheckForDivisions=23555"
   * Plugin Name: Size
   * Plugin value: AttributesToCheck:1155,1156;CheckForDivisions=23555
   *
   * @return String Plugin Value
   */
  public String getValue()
  {
    return value;
  }

  /**
   * Sets Plugin Value
   *
   * @param value String Plugin value
   */
  public void setValue(String value)
  {
    this.value = value;
  }


  /**
   * Parse plugin configuarations String to Map
   */
  public void parsePluginConf()
  {
    String sPluginConf = getValue();

    if(sPluginConf != null && !sPluginConf.isEmpty())
    {
      String[] confs = sPluginConf.split(";");

      for(String config : confs)
      {
        String configs[] = config.split(":");

        if(configs.length == 2)
        {
          this.pluginConf.put(configs[0], configs[1]);
        }
      }
    }
  }


  /**
   * Gets the Map which stores plugin configurations
   *
   * @return Plugin configurations
   */
  public Map<String, String> getPluginConf()
  {
    return pluginConf;
  }
}
