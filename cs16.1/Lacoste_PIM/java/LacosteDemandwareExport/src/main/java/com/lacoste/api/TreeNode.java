package com.lacoste.api;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Represents a XML tag
 *
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class TreeNode
{

  private String nodeName;
  private String value    = "";
  private String language = "";

  private boolean isLeafNode          = false;
  private boolean isMultiple          = false;
  private boolean isLanguageDependent = false;
  private boolean transformationFlag  = false;
  private boolean attributeTransformationFlag = false;

  private Map<String, String> attributes      = new HashMap<>();
  private List<LETransformationPlugin> transformMatrix = null;
  private Map<String, List<LETransformationPlugin>> attributeTransformMatrix = null;
  private List<TreeNode> childNodes           = new LinkedList<>();

  private LEMultiplePlugin multiplePlugin = null;


  public boolean isMultiple() {
    return isMultiple;
  }


  public void setMultiple(boolean isMultiple) {
    this.isMultiple = isMultiple;
  }


  public boolean isLanguageDependent() {
    return isLanguageDependent;
  }


  public void setLanguageDependent(boolean isLanguageDependent) {
    this.isLanguageDependent = isLanguageDependent;
  }


  public String getNodeName() {
    return nodeName;
  }


  public void setNodeName(String nodeName) {
    this.nodeName = nodeName;
  }


  public Map<String, String> getAttributes() {
    return attributes;
  }


  public void setAttributes(Map<String, String> attributes) {
    this.attributes = attributes;
  }


  public List<TreeNode> getChildNodes() {
    return childNodes;
  }


  public void setChildNodes(List<TreeNode> childNodes) {
    this.childNodes = childNodes;
  }


  public boolean isLeafNode() {
    return isLeafNode;
  }


  public void setLeafNode(boolean isLeafNode) {
    this.isLeafNode = isLeafNode;
  }


  public String getValue() {
    return ((value == null) ? "" : value);
  }


  public void setValue(String value) {
    this.value = value;
  }


  public List<LETransformationPlugin> getTransformMatrix() {
    return transformMatrix;
  }


  public void setTransformMatrix(List<LETransformationPlugin> transformMatrix) {
    this.transformMatrix = transformMatrix;
  }


  public boolean isTransformationFlag() {
    return transformationFlag;
  }


  public void setTransformationFlag(boolean transformationFlag) {
    this.transformationFlag = transformationFlag;
  }


  public String getLanguage() {
    return language;
  }


  public void setLanguage(String language) {
    this.language = language;
  }


  public LEMultiplePlugin getMultiplePlugin() {
    return multiplePlugin;
  }


  public void setMultiplePlugin(LEMultiplePlugin multiplePlugin) {
    this.multiplePlugin = multiplePlugin;
  }


  public boolean isAttributeTransformationFlag() {
    return attributeTransformationFlag;
  }


  public Map<String, List<LETransformationPlugin>> getAttributeTransformMatrix() {
    return attributeTransformMatrix;
  }


  public void setAttributeTransformationFlag(boolean attributeTransformationFlag) {
    this.attributeTransformationFlag = attributeTransformationFlag;
  }


  public void setAttributeTransformMatrix(Map<String, List<LETransformationPlugin>> attributeTransformMatrix) {
    this.attributeTransformMatrix = attributeTransformMatrix;
  }
}