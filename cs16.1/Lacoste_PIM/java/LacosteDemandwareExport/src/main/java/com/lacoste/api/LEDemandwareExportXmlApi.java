package com.lacoste.api;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.lacoste.exception.LEDemandwareExportException;
import com.lacoste.plugins.multiple.ColorVariationAttributeMultiple;
import com.lacoste.plugins.transformations.CareInstructionsTransformation;

/**
 * Provides XML APIs for Demandware Export process
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class LEDemandwareExportXmlApi {

  public static LEDemandwareExportXmlTree structure;
  public static Map<String, String> dataLanguages = new HashMap<>();
  public static Set<String> languageCodes = new HashSet<>();
  public static Map<String, String> sites = new HashMap<>();
  public static Map<String, String> config = new HashMap<>();
  public static Map<String, String> commonConfigurations = new HashMap<>();
  public static Map<String, LEStreamExportPlugin> streams = new HashMap<>();

  public static Set<String> staticElements = new HashSet<>();

  public static Set<String> productLangDependentReferenceAttributeIDs = new HashSet<String>();
  public static Set<String> productReferenceAttributeIDs = new HashSet<String>();
  public static Set<String> productLangDependentThreeLevelReferenceAttributeIDs = new HashSet<String>();
  public static Set<String> productThreeLevelReferenceAttributeIDs = new HashSet<String>();
  public static Set<String> productActiveAttributeIDs = new HashSet<>();
  public static Map<String, Map<String, String>> targetAttributesValues = new HashMap<>();
  public static Map<String, List<String>> sizeGuideSubNodeCache = new HashMap<>();

  public static Map<String, LETransformationPlugin> transformationPlugins = new HashMap<>();

  private static List<String> sizeIDs;
  private static List<String> productIDs;

  public static Map<String, List<String>> productSizeCache = new HashMap<>();

  private static Map<String, String> data = new HashMap<>();

  public static Set<String> exportedItemIDs = new HashSet<>();;


  private DocumentBuilderFactory documentBuilderFactory = null;
  private DocumentBuilder documentBuilder = null;

  public LEDemandwareExportXmlApi() {}


  /**
   * Gets the Size IDs
   * 
   * @return list of IDs
   */
  public static List<String> getSizeIDs() {
    return new ArrayList<>(LEDemandwareExportXmlApi.sizeIDs);
  }


  /**
   * Sets the Size IDs
   * 
   * @param sizeIDs list of IDs
   */
  public static void setSizeIDs(List<String> sizeIDs) {
    LEDemandwareExportXmlApi.sizeIDs = sizeIDs;
  }


  public static List<String> getProductIDs() {
    return new ArrayList<>(productIDs);
  }


  public static void setProductIDs(List<String> productIDs) {
    LEDemandwareExportXmlApi.productIDs = productIDs;
  }


  /**
   * Set the data
   * 
   * @param data all config
   */
  public static void setData(Map<String, String> data) {
    LEDemandwareExportXmlApi.data = data;
  }


  /**
   * Gets the data
   * 
   * @return Map<String, String>
   */
  public static Map<String, String> getData() {
    return LEDemandwareExportXmlApi.data;
  }


  public void parseMainConfigXml() throws LEDemandwareExportException
  {
    try
    {
      File configXmlFile = new File(LEDemandwareExportConstants.CONFIG_XML);
      DocumentBuilder dBuilder = getDocumentBuilder();
      Document configXml = dBuilder.parse(configXmlFile);

      Node demadwareExportConfig = configXml.getElementsByTagName("CommonConfigurations").item(0);
      NodeList nodeList = demadwareExportConfig.getChildNodes();

      for(int i=0; i<nodeList.getLength(); i++)
      {
        Node node = nodeList.item(i);
        if(node.getNodeType() != Node.TEXT_NODE)
        {
          NamedNodeMap attributes = node.getAttributes();
          if(attributes != null)
          {
            String name = "";
            String value = "";
            for(int j = 0; j < attributes.getLength(); j++)
            {
              Node d = attributes.item(j);

              if(d.getNodeName() == "name")
              {
                name = d.getNodeValue();
              }
              if(d.getNodeName() == "value")
              {
                value = d.getNodeValue();
              }
            }
            LEDemandwareExportXmlApi.commonConfigurations.put("Configurations." + name,  value);
          }
        }
      }

      LEDemandwareExportXmlApi.commonConfigurations.put("Configurations.EndTimestamp", LEDemandwareExportProperties.getDate(null, "yyyy-MM-dd HH:mm:ss"));

      LEDemandwareExportXmlApi.config = new HashMap<>(LEDemandwareExportXmlApi.commonConfigurations);

      Node streamConfigs = configXml.getElementsByTagName("Streams").item(0);
      nodeList = streamConfigs.getChildNodes();

      for(int i=0; i<nodeList.getLength(); i++)
      {
        Node node = nodeList.item(i);
        if(node.getNodeName().equals("Stream") && node.getNodeType() != Node.TEXT_NODE)
        {
          NamedNodeMap attributes = node.getAttributes();
          if(attributes != null)
          {
            String name = "";
            for(int j = 0; j < attributes.getLength(); j++)
            {
              Node d = attributes.item(j);

              if(d.getNodeName() == "name")
              {
                name = d.getNodeValue();
                LEStreamExportPlugin streamPlugin = getStreamExportPluginByName(name);
                if(streamPlugin == null) {
                  throw new Exception("Stream '" + name + "' Not Supported");
                }
                else
                {
                  streamPlugin.getName();
                }
                streams.put(name, streamPlugin);
              }
            }
          }
        }
      }

      LEDemandwareExportApi.setDemandwareExportProperties();

    }
    catch(Exception exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
      throw new LEDemandwareExportException(exception.getMessage());
    }
  }


  private LEStreamExportPlugin getStreamExportPluginByName(String pluginName)
  {
    LEStreamExportPlugin plugin = null;
    String pluginFullName = "com.lacoste.plugins.streams." + pluginName + "Stream";
    try
    {
      Object pluginObject = Class.forName(pluginFullName).newInstance();
      if(pluginObject instanceof LEStreamExportPlugin)
      {
        plugin = (LEStreamExportPlugin) pluginObject;
      }
    }
    catch(ClassNotFoundException exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
    }
    catch (InstantiationException exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
    }
    catch (IllegalAccessException exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
    }

    return plugin;
  }


  /**
   * Parse configuration XML file. Prepares tree structure. Stores Configurations & Mappings parameters in Map.
   * 
   * @throws LEDemandwareExportException on failure
   */
  public void parseConfigXml(String streamName) throws LEDemandwareExportException
  {
    try
    {
      File configXmlFile = new File(LEDemandwareExportConstants.CONFIG_XML);
      DocumentBuilder dBuilder = getDocumentBuilder();
      Document configXml = dBuilder.parse(configXmlFile);

      XPath xPath = XPathFactory.newInstance().newXPath();

      XPathExpression xExpress = xPath.compile("DemandwareExport/Streams/Stream[@name='" + streamName + "']");//"//*[@name=\"" + streamName + "\"]");
      NodeList streamNodeList = (NodeList)xExpress.evaluate(configXml, XPathConstants.NODESET);

      if(streamNodeList.getLength() == 0)
      {
        throw new LEDemandwareExportException("");
      }

      Node streamNode = streamNodeList.item(0);

      NodeList streamConfigList = streamNode.getChildNodes();

      for(int i=0; i< streamConfigList.getLength(); i++)
      {
        Node node = streamConfigList.item(i);
        if(node.getNodeType() != Node.TEXT_NODE)
        {
          switch(node.getNodeName())
          {
            case "Configurations" :
                this.parseStreamConfigurations(node);
              break;

            case "Mappings" :
                this.parseStreamMappings(node);
              break;

            case "Structure" :
                this.parseStreamStructure(node);
              break;

            default:
              LEDemandwareExportLogger.log("Invalid Config Block For Stream: " + streamName);
          }
        }
      }

      LEDemandwareExportApi.setDemandwareExportProperties();
    }
    catch(Exception exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
      throw new LEDemandwareExportException(exception.getMessage());
    }
  }


  /**
   * Reads the stream specific configurations & merge with the common configurations.
   *
   * @param demadwareExportConfig Configurations Node
   */
  public void parseStreamConfigurations(Node demadwareExportConfig)
  {
    LEDemandwareExportXmlApi.config = new HashMap<>(commonConfigurations);

    NodeList nodeList = demadwareExportConfig.getChildNodes();
    for(int i=0; i<nodeList.getLength(); i++)
    {
      Node node = nodeList.item(i);
      if(node.getNodeType() != Node.TEXT_NODE)
      {
        NamedNodeMap attributes = node.getAttributes();
        if(attributes != null)
        {
          String name = "";
          String value = "";
          for(int j = 0; j < attributes.getLength(); j++)
          {
            Node d = attributes.item(j);

            if(d.getNodeName() == "name")
            {
              name = d.getNodeValue();
            }
            if(d.getNodeName() == "value")
            {
              value = d.getNodeValue();
            }
          }
          LEDemandwareExportXmlApi.config.put("Configurations." + name,  value);
        }
      }
    }

    LEDemandwareExportXmlApi.setData(config);

    LEDemandwareExportXmlApi.setStaticElements();
  }


  /**
   * Reads the stream specific mappings
   *
   * @param demadwareExportMappings mappings node for a stream
   */
  public void parseStreamMappings(Node demadwareExportMappings)
  {

    NodeList mappingNodeList = demadwareExportMappings.getChildNodes();

    for(int i=0; i<mappingNodeList.getLength(); i++)
    {
      Node node = mappingNodeList.item(i);
      NodeList mappingAttrList = node.getChildNodes();
      for(int j=0; j<mappingAttrList.getLength(); j++)
      {
        Node attr = mappingAttrList.item(j);
        if(attr.getNodeType() != Node.TEXT_NODE)
        {
          NamedNodeMap attributes = attr.getAttributes();
          if(attributes != null)
          {
            String name = "";
            String value = "";
            String siteID = "";
            boolean leading = true;
            for(int jk = 0; jk < attributes.getLength(); jk++)
            {
              Node d = attributes.item(jk);

              if(d.getNodeName() == "dw-value")
              {
                name = d.getNodeValue();
              }
              if(d.getNodeName() == "cs-value")
              {
                value = d.getNodeValue();
              }
              if(d.getNodeName() == LEDemandwareExportXmlTree.SITEID)
              {
                siteID = d.getNodeValue();
              }
              if(d.getNodeName() == "leading")
              {
                if(d.getNodeValue().toString().equalsIgnoreCase("no"))
                {
                  leading = false;
                }
                else if(d.getNodeValue().toString().equalsIgnoreCase("yes"))
                {
                  leading = true;
                }
                else
                {
                  leading = false;
                }
              }

              LEDemandwareExportXmlApi.config.put("Configurations." + name, name);
            }
            this.storeMappings(node.getNodeName(), name, value, siteID, leading);
          }
        }
      }
    }
  }


  /**
   * Reads the XML structure of Stream
   *
   * @param demadwareExportStructure structure node for a stream
   */
  public void parseStreamStructure(Node demadwareExportStructure)
  {
    NodeList structure = demadwareExportStructure.getChildNodes();
    for(int structure_counter = 0; structure_counter < structure.getLength(); structure_counter++) {
      Node demadwareExportMappingStructure = structure.item(structure_counter);
      if(demadwareExportMappingStructure.getNodeType() != Node.TEXT_NODE)
      {
        TreeNode root = LEDemandwareExportXmlTree.createTree(demadwareExportMappingStructure);
        LEDemandwareExportXmlApi.structure = new LEDemandwareExportXmlTree(root);
      }
    }
  }


  /**
   * Stores the mapping configurations in the local variables
   * 
   * @param nodeName Mapping Node Name
   * @param name     Mapping name
   * @param value    Value
   * @param siteID   Site ID for DataLangauge Mappings
   * @param leading  store "sites" mapping
   */
  private void storeMappings(String nodeName, String name, String value, String siteID, boolean leading)
  {

    switch(nodeName)
    {
      case "DataLanguages" :
        LEDemandwareExportXmlApi.dataLanguages.put(name, value);
        LEDemandwareExportXmlApi.languageCodes.addAll(dataLanguages.values());
        if(leading) {
          LEDemandwareExportXmlApi.sites.put(value, siteID);
        }
        break;
    }
  }


  /**
   * Initialises Document Builder to be used in XML creation process
   * 
   * @return object of DocumentBuilder
   * 
   * @throws ParserConfigurationException
   */
  public DocumentBuilder getDocumentBuilder() throws ParserConfigurationException
  {

    if(documentBuilder == null)
    {
      documentBuilder = getDocumentBuilderFactory().newDocumentBuilder();
    }

    return documentBuilder;
  }


  /**
   * Initialises Document Builder Factory to be used in XML creation process
   * 
   * @return Document Builder Factory
   */
  public DocumentBuilderFactory getDocumentBuilderFactory()
  {
    if(documentBuilderFactory == null)
    {
      documentBuilderFactory = DocumentBuilderFactory.newInstance();
    }

    return documentBuilderFactory;
  }


  /**
   * Gets the Document object
   * 
   * @return Document
   * 
   * @throws ParserConfigurationException
   */
  public Document getXmlDocument() throws ParserConfigurationException
  {
    return getDocumentBuilder().newDocument();
  }


  /**
   * Creates XML element
   * 
   * @param document    Document object
   * @param elementName XML element name
   * @param value       Value
   * @param attributes  Attributes of XML tag
   * 
   * @return XML tag
   */
  public Element createXmlElement(Document document, String elementName, String value, Map<String, String> attributes)
  {

    Element element = document.createElement(elementName);
    element.appendChild(document.createTextNode(value));

    if(attributes != null)
    {
      Iterator<Entry<String, String>> attributeMapIterator = attributes.entrySet().iterator();
      while(attributeMapIterator.hasNext())
      {
        Entry<String, String> entry = attributeMapIterator.next();
        element.setAttribute(entry.getKey(), entry.getValue());
      }
    }

    return element;
  }


  /**
   * Transfer XML Element to XML file
   * 
   * @param document      Document
   * @param outputXmlName File name
   * 
   * @throws TransformerException
   */
  public void transferXmlToFile(Document document, String outputXmlName) throws TransformerException
  {
    File f = new File(outputXmlName);
    f.getParentFile().mkdirs();
    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    Transformer transformer = transformerFactory.newTransformer();
    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
    document.setXmlStandalone(true);
    DOMSource source = new DOMSource(document);
    StreamResult result = new StreamResult(new File(outputXmlName));
    transformer.transform(source, result);
  }


  /**
   * Adds a XML-tag inside parent XML-tag if none of tag is null
   * 
   * @param parent Parent XML tag
   * @param child  Child XML tag
   */
  public void appendChild(Element parent, Element child)
  {
    if(parent != null && child != null) {
      parent.appendChild(child);
    }
  }


  /**
   * Collect attribute IDs
   * 
   * @param attributeID
   */
  public static void collectAttributes(String attributeID)
  {
    if(!attributeID.startsWith("Configurations")
      && !attributeID.startsWith("Mappings")
      && !attributeID.equals("true")
      && !attributeID.equals("false")
    )
    {
      String attributeList[] = attributeID.split(",");
      for(String attribute : attributeList)
      {
        if(!attribute.contains("-"))
        {
          if(attribute.contains("."))
          {
            String switchingAttributes[] = attribute.split("\\.");
            if(switchingAttributes.length == 2) {
              if(attribute.contains("|")) {
                String referenceAttribute = switchingAttributes[0];
                String textAttributes[] = switchingAttributes[1].split("\\|");
                for(String textAttribute : textAttributes)
                {
                  productReferenceAttributeIDs.add(referenceAttribute + "." + textAttribute);
                }
              }
              else {
                productReferenceAttributeIDs.add(attribute);
              }
            }
            else
            {
              //productThreeLevelReferenceAttributeIDs.add(attribute);
            }
          }
        }
      }
    }
  }


  /**
   * Stores Language dependent attribute in Cache
   * 
   * @param attributeID Language dependent Attribute ID
   */
  public static void collectLanguageAttributes(String attributeID)
  {
    if(!attributeID.startsWith("Configurations")
      && !attributeID.startsWith("Mappings")
      && !attributeID.equalsIgnoreCase("true")
      && !attributeID.equalsIgnoreCase("false")
    )
    {
      String attributeList[] = attributeID.split(",");
      for(String attribute : attributeList)
      {
        if(!attribute.contains("-"))
        {
          if(attribute.contains("."))
          {
            String switchingAttributes[] = attribute.split("\\.");
            if(switchingAttributes.length == 2)
            {
              if(attribute.contains("|"))
              {
                String referenceAttribute = switchingAttributes[0];
                String textAttributes[] = switchingAttributes[1].split("\\|");
                for(String textAttribute : textAttributes)
                {
                  productLangDependentReferenceAttributeIDs.add(referenceAttribute + "." + textAttribute);
                }
              }
              else
              {
                productLangDependentReferenceAttributeIDs.add(attribute);
              }
            }
            else
            {
              //productLangDependentThreeLevelReferenceAttributeIDs.add(attribute);
            }
          }
        }
      }
    }
  }


  /**
   * Stores Active attribute in Cache
   * 
   * @param attributeID Active Attribute ID
   */
  public static void collectActiveAttributes(String attributeID)
  {
    if(!attributeID.startsWith("Configurations")
      && !attributeID.startsWith("Mappings")
      && !attributeID.equalsIgnoreCase("true")
      && !attributeID.equalsIgnoreCase("false")
    )
    {
      if(attributeID.contains("-"))
      {
        productActiveAttributeIDs.add(attributeID);
      }
    }
  }


  /**
   * Sets Language code Cache
   * 
   * @return Set containing CONTENTSERV language codes 
   */
  public static Set<String> getLanguageCodes()
  {
    return languageCodes;
  }


  /**
   * Stores current run start time stamp in configuration XML file
   * 
   * @param timeStamp current run start time stamp (format : yyyy-MM-dd HH:mm:ss)
   */
  public void storeConfig(String timeStamp)
  {
    try
    {
      File configXmlFile = new File(LEDemandwareExportConstants.CONFIG_XML);
      DocumentBuilder dBuilder = getDocumentBuilder();
      Document configXml = dBuilder.parse(configXmlFile);

      XPathFactory xPathfactory = XPathFactory.newInstance();
      XPath xpath = xPathfactory.newXPath();
      XPathExpression xpathExpression = xpath.compile("DemandwareExport/CommonConfigurations/config[@name='StartTimestamp']");
      NodeList configNodeList = (NodeList) xpathExpression.evaluate(configXml, XPathConstants.NODESET);

      Element startTimestampNode = (Element) configNodeList.item(0);

      startTimestampNode.setAttribute("value", timeStamp);

      transferXmlToFile(configXml, LEDemandwareExportConstants.CONFIG_XML);
    }
    catch(Exception exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
      exception.printStackTrace();
    }
  }


  /**
   * Prepares Data to be added in XML file
   */
  public static void prepareData(HashMap<String, String> productValues)
  {
    data = new HashMap<>();

    data.putAll(productValues);
    data.putAll(config);
  }


  /**
   * Returns value from the data cache for given key
   * 
   * @param key Unique key
   * 
   * @return if data cache contains the key the return its value else will return empty string.
   */
  public static String getValue(String key, String nodeName)
  {
    if(!staticElements.contains(nodeName))
    {
      String value = data.get(key);

      return ((value == null) ? "" : value);
    }

    return key;
  }


  /**
   * Returns value from the data cache for given key
   * 
   * @param key Unique key
   * @param nodeName 
   * @param langCode
   * 
   * @return if data cache contains the key the return its value else will return empty string.
   */
  public static String getValue(String key, String nodeName, String langCode)
  {
    if(nodeName.equals("xml:lang"))
    {
      String value = data.get(key);

      return ((value == null) ? "" : value);
    }
    else if(!staticElements.contains(nodeName))
    {
      String value = data.get(key + langCode);

      return ((value == null) ? "" : value);
    }

    return key;
  }


  /**
   * Sets the static elements whose values should be displayed as it is.
   */
  public static void setStaticElements()
  {
    String tags = config.get("Configurations.StaticElements");

    if(tags != null)
    {
      LEDemandwareExportXmlApi.staticElements.addAll(Arrays.asList(tags.split(",")));
    }
  }


  /**
   * Clear cache for a batch
   */
  public static void clearCache()
  {
    targetAttributesValues.clear();
    CareInstructionsTransformation.clearCache();
    ColorVariationAttributeMultiple.clearCache();
    exportedItemIDs = new HashSet<>();
  }


  /**
   *Clear cache after a stream
   */
  public static void clearCacheAfterStream()
  {
    dataLanguages = new HashMap<>();
    languageCodes = new HashSet<>();
    sites = new HashMap<>();
    config = new HashMap<>();

    productLangDependentReferenceAttributeIDs = new HashSet<String>();
    productReferenceAttributeIDs = new HashSet<String>();
    productLangDependentThreeLevelReferenceAttributeIDs = new HashSet<String>();
    productThreeLevelReferenceAttributeIDs = new HashSet<String>();
    productActiveAttributeIDs = new HashSet<>();

    LEDemandwareExportApi.exportedSizeIDs = new ArrayList<>();
  }


  public static void addExportedItemIDs(List<String> exportedItemsForCurrentBatch)
  {
    exportedItemIDs.addAll(exportedItemsForCurrentBatch);
  }
}
