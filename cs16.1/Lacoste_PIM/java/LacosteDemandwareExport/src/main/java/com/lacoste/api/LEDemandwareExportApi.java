package com.lacoste.api;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.exportstaging.api.ExternalItemAPI;
import com.exportstaging.api.ItemAPIs;
import com.exportstaging.api.domain.Attribute;
import com.exportstaging.api.domain.AttributeValue;
import com.exportstaging.api.domain.ExportValues;
import com.exportstaging.api.domain.Reference;
import com.exportstaging.api.exception.ExportStagingException;
import com.exportstaging.api.resultset.ItemsResultSet;
import com.exportstaging.api.searchfilter.SearchFilterCriteria;
import com.exportstaging.api.wraper.ItemWrapper;
import com.exportstaging.utils.ExportCassandraConfigurator;
import com.google.common.collect.Lists;
import com.lacoste.api.cache.LECacheManager;
/*import com.lacoste.api.cache.LECacheManager;*/
import com.lacoste.api.values.OnlineFromXValue;
import com.lacoste.exception.LEDemandwareExportException;

/**
 * Export database APIs class for Demandware Export process
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class LEDemandwareExportApi
{
  public static final String ERROR_LOG = "demandware_export_error.log";
  public static final String LANGUAGE_ID = "1";
  public static final String LANGUAGE_CODE = "en_en";

  private static final String ATTRIBUTETYPE_ARTICLEREFERENCE = "articlereference";

  private static LEElasticsearchApi elasticsearchApi = null;
  private static LEDemandwareExportProperties exportdatabaseProperties = null;
  private static ExternalItemAPI externalItemEsaAPI       = null;

  public static List<String> updatedSizeIDs = new ArrayList<>();
  public static List<String> updatedProductIDs = new ArrayList<>();
  public static List<String> exportedSizeIDs = new ArrayList<>();

  private static LECacheManager cacheManager = new LECacheManager();

  /**
   * Gets the Cassandra Export configurator
   * 
   * @throws LEPublicationListException When Cassandra key space & address is not configured 
   */
  public static void getCassandraExportConfigurator()
  {
    String cassandrAddress  = exportdatabaseProperties.getCassandraAddress();
    String cassandrKeyspace = exportdatabaseProperties.getCassandraKeyspace();

    new ExportCassandraConfigurator(cassandrAddress, cassandrKeyspace);
  }


  /**
   * Gets the instance of Export database External Item API
   * 
   * @return External Item API
   * 
   * @throws LEPublicationListException 
   */
  public static ExternalItemAPI getExternalItemEsaAPI() throws LEDemandwareExportException
  {
    try
    {
      if(externalItemEsaAPI == null)
      {
        getCassandraExportConfigurator();
        externalItemEsaAPI = new ExternalItemAPI(ExternalItemAPI.ITEM_TYPE_PRODUCT);
      }
    }
    catch(ExportStagingException e)
    {
      throw new LEDemandwareExportException(e.getMessage());
    }

    return externalItemEsaAPI;
  }


  /**
   * Gets the elastic search API
   * 
   * @return instance of LEElasticsearchApi class
   * 
   * @throws LEDemandwareExportException On failure
   */
  private static LEElasticsearchApi getElasticsearchApi() throws Exception
  {

    if(elasticsearchApi == null)
    {
      elasticsearchApi = new LEElasticsearchApi();
      try{
        elasticsearchApi.setConnection(
          exportdatabaseProperties.getElasticsearchCluster(),
          exportdatabaseProperties.getElasticsearchAddress(),
          exportdatabaseProperties.getElasticsearchTcpPort(),
          exportdatabaseProperties.getElasticsearchIndex()
        );
      }
      catch(Exception e)
      {
        throw e;
      }
    }

    return elasticsearchApi;
  }


  /**
   * Gets the Updated Size Products since Last run start time.
   * 
   * @return List of Size Products IDs
   * 
   * @throws LEDemandwareExportException
   */
  public static List<String> getUpdatedSizeElements() throws LEDemandwareExportException
  {
    List<String> updatedSizeIds = new ArrayList<>();
    try
    {
      LEElasticsearchApi elasticsearchApi = getElasticsearchApi();

      String startDate    = exportdatabaseProperties.getStartDate();
      String endDate      = exportdatabaseProperties.getEndDate();
      String itemType     = exportdatabaseProperties.getAttributeItemType();
      String itemTypeSize = exportdatabaseProperties.getItemTypeSize();
      String sizeClassIDs = exportdatabaseProperties.getSizeClassIDs();
      String allowedStates = exportdatabaseProperties.getAllowedWorkflowStates();

      List<String> updatedSizeIdsSinceLastRun = elasticsearchApi.getUpdatedSizeIDs(
        startDate,
        endDate,
        LANGUAGE_ID,
        itemType,
        itemTypeSize,
        Arrays.asList(allowedStates.split(","))
      );

      Set<String> updatedSizeIdsDueToReferenceLink = new HashSet<>();
      String startDateFromConfig = exportdatabaseProperties.getStartDateForReferenceUpdateLink();
      if(startDateFromConfig != null && !startDateFromConfig.isEmpty()) {
        updatedSizeIdsDueToReferenceLink = elasticsearchApi.getUpdatedSizeIDsDueToReferenceLink(
          startDate,
          endDate,
          LANGUAGE_ID,
          itemType,
          itemTypeSize,
          getArticleReferenceAttributeIDsForClassIDs(sizeClassIDs),
          Arrays.asList(allowedStates.split(","))
        );
      }

      Set<String> uniqueSizeIDs = new HashSet<>();
      uniqueSizeIDs.addAll(updatedSizeIdsSinceLastRun);
      uniqueSizeIDs.addAll(updatedSizeIdsDueToReferenceLink);

      updatedSizeIds.addAll(uniqueSizeIDs);

      LEDemandwareExportApi.updatedSizeIDs = updatedSizeIds;
    }
    catch(Exception e)
    {
      LEDemandwareExportApi.printStackTrace(e);
      throw new LEDemandwareExportException(e.getMessage());
    }

    return updatedSizeIds;
  }


  /**
   * Gets the formatted date
   * 
   * @param date   Date object or null for current date
   * @param format Format or empty string for format "yyyy-MM-dd HH:mm:ss"
   * 
   * @return Formatted date
   */
  public static String getDate(Date date, String format)
  {
    if(date == null)
    {
      date = new Date();
    }

    format = format.isEmpty() ? "yyyy-MM-dd HH:mm:ss" : format;
    String formattedDate = new SimpleDateFormat(format).format(date);

    return formattedDate;
  }


  /**
   * Gets the ExportDatabase PIM Item Wrapper object
   * 
   * @param productID PIM item's ID
   * 
   * @return ExportDatabase PIM Item Wrapper object for given PIM product ID
   */
  public static ItemWrapper getProductByID(long productID)
  {
    try {
      return getExternalItemEsaAPI().getItemById(productID);
    }
    catch(Exception exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
      return null;
    }
  }


  /**
   * Gets the ExportDatabase PIM Item Wrapper object
   * 
   * @param productID PIM item's ID
   * 
   * @return Map of attribute ID & its value
   */
  public static Map<String, String> getTargetByID(long productID)
  {
    Map<String, String> data = new HashMap<String, String>();

    try
    {
      if(LEDemandwareExportXmlApi.targetAttributesValues.containsKey(productID + ""))
      {
        data = LEDemandwareExportXmlApi.targetAttributesValues.get(productID + "");
      }
      else
      {
        ItemWrapper target = getExternalItemEsaAPI().getItemById(productID);
        if(target != null)
        {
          data = getValues(target);
          if(!updatedProductIDs.contains(productID + "")
            && !updatedSizeIDs.contains(productID + "")
          )
          {
            LEDemandwareExportXmlApi.targetAttributesValues.put(productID + "", data);
          }
        }
      }
    }
    catch(Exception exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
    }
    
    return data;
  }


  /**
   * Gets product batch size. Used to limit the size of output XML file.
   * 
   * @return Batch size
   */
  public static int getProductBatchSize()
  {
    int batchSize = exportdatabaseProperties.getProductBatchSize();

    return (batchSize != 0) ? batchSize : 100;
  }


  /**
   * Gets the name of output XML file
   * Format: "DemandwareExport_<yy_MM_dd>_<HH_mm_ss>_<BatchID>.xml"
   * 
   * @param batchID ID of export batch
   * 
   * @return Export file name
   */
  public static String getOuputXmlName(int batchID)
  {
    Date currentDate = new Date();
    String outputXmlName =  exportdatabaseProperties.getXmlOutputLocation() + File.separator
      + "xml" + File.separator
      + "DemandwareExport_" + LEDemandwareExportProperties.getDate(currentDate, "yy")
      + "_" + LEDemandwareExportProperties.getDate(currentDate, "MM")
      + "_" + LEDemandwareExportProperties.getDate(currentDate, "dd")
      + "_" + LEDemandwareExportProperties.getDate(currentDate, "HH")
      + "_" + LEDemandwareExportProperties.getDate(currentDate, "mm")
      + "_" + LEDemandwareExportProperties.getDate(currentDate, "ss")
      + "_" + batchID + ".xml";

    outputXmlName = Paths.get(outputXmlName).toString();

    return outputXmlName;
  }


  public static String getAttributeInfo(String attributeID, String infoAttribute)
  {
    try {
      Attribute attribute =  getExternalItemEsaAPI().getAttributeByID(Long.parseLong(attributeID));
      String value = "";
      if(infoAttribute.equals("Label"))
      {
        value = attribute.getLabel();
      }
      else
      {
        value = attribute.getValue(infoAttribute);
      }
      return value;
    }
    catch(Exception e)
    {
      e.printStackTrace();
      System.err.println(e.getMessage());
      return null;
    }
  }


  /**
   * Sets Product attribute values Cache "LEDemandwareExportXmlApi.productValues"
   * 
   * @param productID ID of product
   * 
   * @throws LEDemandwareExportException
   */
  public static void setProductValues(long productID) throws LEDemandwareExportException
  {
      HashMap<String, String> productValues = new HashMap<>();

      if(isKeyInCache(productID))
      {
        productValues = getValuesFromCache(productID);
      }
      else
      {
      productValues.put("PdmarticleID", productID + "");
      Map<String, String> data = getTargetByID(productID);
      try
      {
        productValues.putAll(data);
        for(String attributeID : LEDemandwareExportXmlApi.productReferenceAttributeIDs)
        {
          String attList[] = attributeID.split("\\.");
          String ref = data.get(attList[0]);
          if(ref != null && !ref.isEmpty()) {
            List<String> list = Arrays.asList(ref.split(","));
            long targetID = Long.parseLong(list.get(0));
            Map<String, String> target = getTargetByID(targetID);
            if(target != null) {
              productValues.put(attributeID, target.get(attList[1]));
            }
          }
        }
        for(String attributeID : LEDemandwareExportXmlApi.productLangDependentReferenceAttributeIDs)
        {
          String attList[] = attributeID.split("\\.");
  
          for(String languageCode : LEDemandwareExportXmlApi.getLanguageCodes())
          {
            String ref = data.get(attList[0] + languageCode) != null ? data.get(attList[0] + languageCode) : "" ;
            if(!ref.isEmpty()) {
            List<String> list = Arrays.asList(ref.split(","));
            if(list != null && list.size() != 0) {
              long targetID = Long.parseLong(list.get(0));
              Map<String, String> target = getTargetByID(targetID);
              if(target != null) {
                productValues.put(attributeID + languageCode, target.get(attList[1] + languageCode));
              }
            }
            }
          }
        }

        if(LEDemandwareExportApi.updatedSizeIDs.contains(productID + "")
            && !LEDemandwareExportApi.exportedSizeIDs.contains(productID + "")
        )
        {
          String currentDate = getDate(null, "yyyy-MM-dd");
          for(String attributeID : LEDemandwareExportXmlApi.productActiveAttributeIDs)
          {
            if(attributeID.contains("online-from"))
            {
              String startDate = "";
              String endDate = "";
              String seasonalDataAttribute = LEDemandwareExportXmlApi.config.get("Configurations.SeasonalDataAttribute");
              OnlineFromXValue onlineFromXValueComponent = OnlineFromXValue.getOnlineFromXValueComponentsFromCache(attributeID);
              String startDateKey = onlineFromXValueComponent.getStartKey();
              String endDateKey = onlineFromXValueComponent.getEndKey();
              String siteID = onlineFromXValueComponent.getSiteID();
              String startDateAttID = onlineFromXValueComponent.getStartDate();
              String endDateAttID = onlineFromXValueComponent.getEndDate();
              String activeAttributeKey = onlineFromXValueComponent.getActivePeriod();
              List<String> activeAttributes = onlineFromXValueComponent.getActiveAttributes();

              for(String activeAtt : activeAttributes)
              {
                String attr = data.get(activeAtt) != null ? data.get(activeAtt) : "";
                String list[] = attr.split(",");
                if(list != null && list.length != 0)
                {
                  for(String ref : list)
                  {
                    if(ref.isEmpty())
                    {
                      continue;
                    }
                    Map<String, String> target = getTargetByID(Long.parseLong(ref));
                    if(target != null)
                    {
                      String tempEndDate   = target.get(endDateAttID);
 
                      if(compareDates(tempEndDate, currentDate))
                      {
                        continue;
                      }

                      String tempStartDate = target.get(startDateAttID);
                      tempStartDate = ((tempStartDate == null) ? "" : tempStartDate);
                      String seasonalData  = target.get(seasonalDataAttribute);
                      startDate = ((startDate == null) ? "" : startDate);

                      if (startDate.isEmpty() || (!tempStartDate.isEmpty() && compareDates(tempStartDate, startDate)))
                      {
                        if(tempStartDate != null && !tempStartDate.isEmpty())
                        {
                          String calEndDate = tempEndDate;
                          if(!calEndDate.isEmpty())
                          {
                            productValues.put(startDateKey, tempStartDate);
                            productValues.put(endDateKey, calEndDate);
                            productValues.put(siteID, seasonalData);
                            productValues.put(activeAttributeKey, ref);
                            startDate = tempStartDate;
                            endDate = calEndDate;
                          }
                        }
                      }
                      else if(equalsDates(tempStartDate, startDate) && compareDates(endDate, tempEndDate))
                      {
                        productValues.put(endDateKey, tempEndDate);
                        endDate = tempEndDate;
                        productValues.put(siteID, seasonalData);
                        productValues.put(activeAttributeKey, ref);
                      }
                    }
                  }
                }
              }
            }
          }
          LEDemandwareExportApi.exportedSizeIDs.add(productID + "");
          putValuesInCache(productID, productValues);
        }
      }
      catch(Exception exception)
      {
        LEDemandwareExportApi.printStackTrace(exception);
        LEDemandwareExportApi.logError(exception.getMessage());
      }
      }
      LEDemandwareExportXmlApi.prepareData(productValues);
  }


  /**
   * Gets the all values of a product
   * 
   * @param item
   * 
   * @return
   * 
   * @throws LEDemandwareExportException
   */
  public static Map<String, String> getValues(ItemWrapper item) throws LEDemandwareExportException
  {
    Map<String, String> data = new HashMap<> ();

    try {
      Set<String> langNames  = LEDemandwareExportXmlApi.getLanguageCodes();

      ExportValues defaultValues = item.getValues();

      for(AttributeValue value : defaultValues.getAttributeList())
      {
        data.put(value.getId(), value.getFormattedValue());
      }
      for(Reference reference : defaultValues.getReferenceList())
      {
        if(!data.containsKey(reference.getAttributeID() + ""))
        {
          data.put(reference.getAttributeID() + "", reference.getTargetID() + "");
        }
        else
        {
          String targets = data.get(reference.getAttributeID() + "");
          if(!targets.isEmpty())
          data.put(reference.getAttributeID() + "", targets + "," + reference.getTargetID());
        }
      }

      for(String langID : langNames)
      {
        ExportValues values = item.getValues(langID);
        for(AttributeValue value : values.getAttributeList())
        {
          data.put(value.getId() + langID, value.getFormattedValue());
        }
        for(Reference reference : values.getReferenceList())
        {
          String key = reference.getAttributeID() + langID;
          if(!data.containsKey(key)) {
            data.put(key, reference.getTargetID() + "");
          }
          else
          {
            String targets = data.get(key);
            data.put(key + "", targets + "," + reference.getTargetID());
          }
        }
      }
    }
    catch(Exception e)
    {
      LEDemandwareExportApi.printStackTrace(e);
      throw new LEDemandwareExportException(e.getMessage());
    }
    return data;
  }


  /**
   * Compare two valid String dates of format: "yyyy-MM-dd"
   * 
   * @param firstDate  Date 1
   * @param secondDate Date 2
   * 
   * @return true if Date 1 is smaller that Date 2 else false
   */
  public static boolean compareDates(String firstDate, String secondDate)
  {
    try
    {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      return sdf.parse(firstDate).before(sdf.parse(secondDate));
    }
    catch(Exception exception)
    {
      return false;
    }
  }


  /**
   * Compare two valid String dates of format: "yyyy-MM-dd"
   * 
   * @param firstDate  Date 1
   * @param secondDate Date 2
   * 
   * @return true if Date 1 is equals to Date 2 else false
   */
  public static boolean equalsDates(String firstDate, String secondDate)
  {
    try
    {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      return sdf.parse(firstDate).equals(sdf.parse(secondDate));
    }
    catch(Exception exception)
    {
      return false;
    }
  }


  /**
   * Gets the current run start time
   * 
   * @return Current run start time
   */
  public static String getRunStartTimestamp()
  {
    return exportdatabaseProperties.getEndDate();
  }


  /**
   * Close externalItemEsaAPI var
   */
  public static void closeExportApi()
  {
    if(externalItemEsaAPI != null)
    {
      externalItemEsaAPI.close();
    }
  }


  /**
   * Writes stack trace to file
   * 
   * @param ex Exception
   */
  public static void printStackTrace(Exception exception)
  {
    StringWriter errors = new StringWriter();
    exception.printStackTrace(new PrintWriter(errors));
    logError(errors.toString());
  }


  /**
   * Error message
   * 
   * @param errorMessage Error message
   */
  public static void logError(String errorMessage)
  {
    String errorLogFileName = LEDemandwareExportConstants.ERROR_LOG;
    String logMessage = "[" + LEDemandwareExportProperties.getDate(null, "yyyy-MM-dd HH:mm:ss") + "] " + errorMessage + "\n";

    File file = new File(errorLogFileName);
    file.getParentFile().mkdirs();

    try
    {
      Files.write(
        Paths.get(errorLogFileName),
        logMessage.getBytes(),
        StandardOpenOption.APPEND,
        StandardOpenOption.CREATE
      );
    }
    catch (IOException exception) {}
  }


  /**
   * Gets the IDs of PIM products.
   * 
   * @param idProductAttributeID Attribute ID
   * @param idProduct            value of attribute
   * 
   * @return List of Product IDs
   */
  public static List<String> getColorsForIDProduct(
    String idProductAttributeID,
    String idProduct
  )
  {
    String itemTypeColor       = exportdatabaseProperties.getItemTypeColor();
    String activeAttributeEuID = exportdatabaseProperties.getAttributeActiveEu();
    String itemTypeAttributID  = exportdatabaseProperties.getAttributeItemType();

    List<String> data          = null;

    try
    {
      data = elasticsearchApi.getColorsForIDProduct(
        idProductAttributeID,
        idProduct,
        itemTypeAttributID,
        itemTypeColor,
        activeAttributeEuID,
        LANGUAGE_ID
      );
    }
    catch(Exception exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
    }

    if(data == null) {
      data = new ArrayList<String>();
    }

    return data;
  }


  /**
   * Gets ID attribute color from Configurations
   * 
   * @return ID of color
   */
  public static long getAttributeColor()
  {
    return Long.parseLong(exportdatabaseProperties.getAttributeColor());
  }


  /**
   * List of reference to PIM attribute IDs present in given Class IDs.
   * 
   * @param classIDs IDs of PIM classes separated by comma (,) to search attributes of type "articlereference".
   * 
   * @return Set of attribute IDs
   */
  public static Set<Long> getArticleReferenceAttributeIDsForClassIDs(String classIDs) throws LEDemandwareExportException
  {
    Set<Long> articlereferenceAttibuteIDs = new HashSet<>();

    try
    {
      classIDs = ((classIDs != null) ? classIDs : "");
      String sizeClassIDs[] = classIDs.split(",");
      for(String classID : sizeClassIDs)
      {
        long sizeClassID = Long.parseLong(classID);
        List<Long> attributes = LEDemandwareExportApi.getExternalItemEsaAPI().getAttributeIDsByClassID(sizeClassID);
        for(long attributeID : attributes)
        {
          Attribute attribute = LEDemandwareExportApi.getExternalItemEsaAPI().getAttributeByID(attributeID);
          if(attribute.getType().equals(ATTRIBUTETYPE_ARTICLEREFERENCE))
          {
            articlereferenceAttibuteIDs.add(attributeID);
          }
        }
      }
    }
    catch(NumberFormatException exception)
    {
      throw new LEDemandwareExportException("Error " + exception.getMessage() + " in ''Configurations.SizeClassIDs' must be an Integer.");
    }
    catch(NullPointerException | ExportStagingException | LEDemandwareExportException exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
      throw new LEDemandwareExportException(exception.getMessage());
    }

    return articlereferenceAttibuteIDs;
  }


  /**
   * Gets the list of exportable Product items for current run
   * 
   * @param sizeIDs Exportable Size IDs
   * 
   * @return List of product item IDs. Empty list on error. 
   */
  public static List<String> getExportableProductItems(List<String> sizeIDs)
  {
    String itemTypeProduct = exportdatabaseProperties.getItemTypeProduct();
    String itemTypeAttributeID = exportdatabaseProperties.getAttributeItemType();
    String productIDAttributeID = exportdatabaseProperties.getAttributeProductID();
    List<String> productIDs = new ArrayList<>();

    try
    {
      if(sizeIDs.size() > 0) {
        LEElasticsearchApi elasticsearchApi = getElasticsearchApi();
        productIDs = elasticsearchApi.getProductItems(
          itemTypeProduct,
          itemTypeAttributeID,
          productIDAttributeID,
          sizeIDs
        );
      }
    }
    catch (Exception exception)
    {
      System.out.println("ERRROR");
      LEDemandwareExportApi.printStackTrace(exception);
    }

    LEDemandwareExportApi.updatedProductIDs = productIDs;

    return productIDs;
  }


  /**
   * Gets the list of batches for Export. Each batch will contain "Configurations.ProductBatchSize" items.
   *
   * @param sizeIDs    List of updated size IDs
   * @param productIDs List of updated product IDs
   *
   * @return List of LEDemandwareExportBatch
   *
   * @throws LEDemandwareExportException on error
   */
  public static List<LEDemandwareExportBatch> getExportableItemBatches(List<String> sizeIDs, List<String> productIDs) throws LEDemandwareExportException
  {
    List<LEDemandwareExportBatch> exportableBatches = new ArrayList<>();
    try {
      List<List<String>> list = Lists.partition(sizeIDs, LEDemandwareExportApi.getProductBatchSize());

      int batchSize = LEDemandwareExportApi.getProductBatchSize();

      int totalSizes = sizeIDs.size();
      int totalProducts = productIDs.size();
      int totalItems = (totalSizes + totalProducts);

      LEDemandwareExportLogger.log("Last run start time: " + exportdatabaseProperties.getStartDate());
      LEDemandwareExportLogger.log("Total Item count - " + totalItems);
      LEDemandwareExportLogger.log("Item IDs:" + Arrays.toString(Stream.concat(sizeIDs.stream(), productIDs.stream()).collect(Collectors.toList()).toArray()));

      int totalBatches = (int) Math.ceil( (double) totalItems / (double) batchSize);

      int productIndex = 0;

      for(int i = 0; i < totalBatches; i++)
      {
        LEDemandwareExportBatch exportableBatch = new LEDemandwareExportBatch();

        if(i < (list.size()-1))
        {
          exportableBatch.setSizeItemsForCurrentBatch(list.get(i));
          exportableBatch.setProductItemsForCurrentBatch(new ArrayList<>());
        }
        else 
        {
          if((list.size() - 1) == i)
          {
            List<String> s = list.get(i);
            if(s.size() < batchSize)
            {
              int remaining = (batchSize - s.size());
              remaining = remaining < totalProducts ? remaining : totalProducts;
              exportableBatch.setSizeItemsForCurrentBatch(s);
              exportableBatch.setProductItemsForCurrentBatch(productIDs.subList(0, remaining));
              productIndex = remaining;
            }
            else
            {
              exportableBatch.setSizeItemsForCurrentBatch(s);
              exportableBatch.setProductItemsForCurrentBatch(new ArrayList<String>());
            }
          }
          else
          {
            if(productIndex < totalProducts)
            {
              int newProductIndex = productIndex + batchSize;
              newProductIndex = newProductIndex > totalProducts ? totalProducts : newProductIndex;
              exportableBatch.setProductItemsForCurrentBatch(productIDs.subList(productIndex, newProductIndex));
              productIndex = newProductIndex; 
            }
            exportableBatch.setSizeItemsForCurrentBatch(new ArrayList<String>());
          }
        }

        exportableBatches.add(exportableBatch);
      }
    }
    catch(Exception exception)
    {
      throw exception;
    }

    return exportableBatches;
  }


  /**
   * Gets the resultset of the child node of given Item.
   * 
   * @param itemID Parent node ID
   * 
   * @return Resultset of child node IDs
   */
  private static ItemsResultSet getItemFirstLevelNodes(String itemID)
  {
    ItemAPIs             itemAPI              = null;
    ItemsResultSet       itemsResultSet       = null;
    SearchFilterCriteria searchFilterCriteria = null;

    LEDemandwareExportApi.getCassandraExportConfigurator();
    try
    {
      itemAPI = getExternalItemEsaAPI();
      searchFilterCriteria = new SearchFilterCriteria();
      searchFilterCriteria.setParentId(itemID);
      itemsResultSet = itemAPI.getItemIdByFilterFromMaterializeView(searchFilterCriteria);
    }
    catch (ExportStagingException | LEDemandwareExportException e)
    {
      LEDemandwareExportApi.printStackTrace(e);
    }

    return itemsResultSet;
  }


  /**
   * Gets the list of the child node of given Item.
   * 
   * @param itemID Parent node ID
   * @param sortingAttribute Attribute ID to sort Size guide items
   * 
   * @return List of child node IDs
   */
  public static List<String> getItemFirstLevelNodesIDs(String itemID, String sortingAttribute)
  {
    List<String> subNodes = new ArrayList<>();

    if(LEDemandwareExportXmlApi.sizeGuideSubNodeCache.containsKey(itemID))
    {
      subNodes = LEDemandwareExportXmlApi.sizeGuideSubNodeCache.get(itemID);
    }
    else
    {
      try {
        ItemsResultSet firstLevelNodes = getItemFirstLevelNodes(itemID);
        Map<Long, String> data = new HashMap<>();
        for(int i = 0; i < firstLevelNodes.count(); i++)
        {
          ItemWrapper item = firstLevelNodes.nextItem();

          Long productID = item.getItemID();
          String label = item.getFormattedValue("Label");
          data.put(productID, label);
        }

        Set<Long> sortedIDs = sortByValue(data).keySet();

        for(Long subNodeID : sortedIDs)
        {
          subNodes.add(subNodeID + "");
        }

        LEDemandwareExportXmlApi.sizeGuideSubNodeCache.put(itemID, subNodes);
      }
      catch(Exception exception)
      {
        LEDemandwareExportApi.printStackTrace(exception);
      }
    }

    return subNodes;
  }


  /**
   * Sort Map Keys by Values
   * 
   * @param unsortMap Unsorted Map for sorting
   * 
   * @return Sorted Map by values
   */
  private static Map<Long, String> sortByValue(Map<Long, String> unsortMap) {

    List<Map.Entry<Long, String>> list =
            new LinkedList<Map.Entry<Long, String>>(unsortMap.entrySet());

    Collections.sort(list, new LEDemandwareExportLabelComaparator<Long>());

    Map<Long, String> sortedMap = new LinkedHashMap<Long, String>();
    for (Map.Entry<Long, String> entry : list) {
      sortedMap.put(entry.getKey(), entry.getValue());
    }

    return sortedMap;
  }


  /**
   * Gets the list valid division for "ConditionalFinishingCode" transformation.
   * 
   * @return List of transformation plugins
   */
  public static List<String> getValidDivisions()
  {
    return Arrays.asList(exportdatabaseProperties.getValidDivisions().split(","));
  }


  /**
   * Gets the Attribute ID of Color Default position from Configurations
   *
   * @return Attribute ID of Color Default position
   */
  public static String getAttributeColorDefaultPosition()
  {
    return exportdatabaseProperties.getAttributeColorDefaultPosition();
  }


  /**
   * Gets the instance of LETransformationPlugin by its name
   * 
   * @param pluginName Plugin Name
   * 
   * @return Object of transformation Plugin
   */
  public static LETransformationPlugin getTransformationPluginByName(String pluginName)
  {
    LETransformationPlugin plugin = null;
    String pluginFullName = "com.lacoste.plugins.transformations." + pluginName + "Transformation";
    try
    {
      Object pluginObject = Class.forName(pluginFullName).newInstance();
      if(pluginObject instanceof LETransformationPlugin)
      {
        plugin = (LETransformationPlugin) pluginObject;
      }
      else
      {
        throw new Exception();
      }
    }
    catch(Exception exception)
    {
      LEDemandwareExportLogger.log("'" + pluginName + "' Transformation not supported.");
    }

    return plugin;
  }


  /**
   * Gets the filtered list of Product items
   * 
   * @param attributesToCheck   Attribute IDs to check
   * @param validDivisions      Valid Divisions IDs
   * @param sizeIDs             Size IDs
   * @param attributeIdDivision Attribute ID division
   *
   * @return List of product IDs
   *
   * @throws LEDemandwareExportException
   */
  public static List<String> filterSizeIDsForCurrentConfigurations(
    String attributesToCheck[],
    String validDivisions[],
    List<String> sizeIDs,
    String attributeIdDivision
  ) throws LEDemandwareExportException
  {
    try
    {
      return elasticsearchApi.filterSizeIDsForCurrentConfigurations(attributesToCheck, validDivisions, sizeIDs, attributeIdDivision);
    }
    catch (ExportStagingException exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
      throw new LEDemandwareExportException(exception.getMessage());
    }
  }


  /**
   * Gets the Attribute ID of Divisions from Common Configurations
   *
   * @return Attribute ID of Divisions
   */
  public static String getAttributeIdDivisions() {
    return exportdatabaseProperties.getAttributeIdDivisions();
  }


  /**
   * Filtered product ID for current configurations
   *
   * @param attributesToCheck           Attribute IDs to check
   * @param divisions                   Valid Divisions
   * @param productIDs                  Product IDs
   * @param isCheckingForValidDivisions Is Checking For Valid Divisions
   *
   * @return List of filtered product IDs
   */
  public static List<String> filterProductIDsForCurrentConfigurations(
    String attributesToCheck[],
    String divisions[],
    List<String> productIDs,
    boolean isCheckingForValidDivisions
  )
  {
    List<String> filteredProductIDs = new ArrayList<>();
    try
    {
      filteredProductIDs = elasticsearchApi.filterProductIDsByPluginConfigurations(
        productIDs,
        attributesToCheck,
        divisions,
        isCheckingForValidDivisions,
        exportdatabaseProperties.getAttributeProductID()
      );
    }
    catch (ExportStagingException exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
    }

    return filteredProductIDs;
  }


  /**
   * Gets the filtered list of Size items
   * 
   * @param attributesToCheck   Attribute IDs to check
   * @param divisions           Divisions IDs
   * @param sizeIDs             Size IDs
   * @param isValidDivisions    Valid or Invalid case
   *
   * @return List of product IDs
   *
   * @throws LEDemandwareExportException
   */
  public static List<String> filterSizeIDsByDivisions(
    String attributesToCheck[],
    String divisions[],
    List<String> sizeIDs,
    boolean isValidDivisions
  ) throws LEDemandwareExportException
  {
    try
    {
      return elasticsearchApi.filterSizeIDsByDivisions(attributesToCheck, divisions, sizeIDs, isValidDivisions);
    }
    catch (ExportStagingException exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
      throw new LEDemandwareExportException(exception.getMessage());
    }
  }


  /**
   * Checks of product information is available in cache
   *
   * @param productID Product ID
   *
   * @return TRUE on cache hit else FALSE
   */
  public static boolean isKeyInCache(long productID)
  {
    return cacheManager.isContainsKey(productID);
  }


  /**
   * Gets the products information from Cache
   *
   * @param productID Product ID
   *
   * @return Products information
   */
  public static HashMap<String, String> getValuesFromCache(long productID)
  {
    return cacheManager.getDataFromCache(productID);
  }


  /**
   * Puts items information in cache
   *
   * @param productID Product ID as cache key
   * @param values    Products information
   */
  public static void putValuesInCache(long productID, HashMap<String, String> values)
  {
    cacheManager.putDataInCache(productID, values);
  }


  /**
   * Closes the instance of cache manager
   */
  public static void closeCacheManager()
  {
    cacheManager.close();
  }


  /**
   * Clears the mappings between cache key and cach values
   */
  public static void clearCacheManager()
  {
    cacheManager.clear();
  }


  /**
   * Sets Demandware Export properties.
   *
   * @throws LEDemandwareExportException
   */
  public static void setDemandwareExportProperties() throws LEDemandwareExportException
  {
    exportdatabaseProperties = new LEDemandwareExportProperties();
  }
}
