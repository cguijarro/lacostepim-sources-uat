package com.lacoste.api;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.lacoste.exception.LEDemandwareExportException;


/**
 * This class contains user configurations for Demandware Export process
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class LEDemandwareExportProperties
{
  public static final String ConfigurationsCassandraAddress = "Configurations.CassandraAddress";
  public static final String ConfigurationsCassandraKeyspace = "Configurations.CassandraKeyspace";

  public static final String ConfigurationsElasticsearchAddress = "Configurations.ElasticsearchAddress";
  public static final String ConfigurationsElasticsearchCluster = "Configurations.ElasticsearchCluster";
  public static final String ConfigurationsElasticsearchIndexName = "Configurations.ElasticsearchIndexName";
  public static final String ConfigurationsElasticsearchTcpPort = "Configurations.ElasticsearchTcpPort";

  public static final String ConfigurationsStartTimestamp = "Configurations.StartTimestamp";
  public static final String ConfigurationsEndTimestamp = "Configurations.EndTimestamp";

  public static final String ConfigurationsProductBatchSize = "Configurations.ProductBatchSize";
  public static final String ConfigurationsAttributeItemType = "Configurations.AttributeItemType";
  public static final String ConfigurationsAttributeActivePeriods = "Configurations.AttributeActivePeriods";
  public static final String ConfigurationsAttributeColor = "Configurations.AttributeColor";
  public static final String ConfigurationsAttributeSize = "Configurations.AttributeSize";
  public static final String ConfigurationsAttributeEsbCode = "Configurations.AttributeEsbCode";
  public static final String ConfigurationsAttributeProductID = "Configurations.AttributeProductID";
  public static final String ConfigurationsAttributeColorDefaultPosition = "Configurations.AttributeColorDefaultPosition";
  public static final String ConfigurationsSeasonalDataAttribute = "Configurations.SeasonalDataAttribute";
  public static final String ConfigurationsSizeClassIDs = "Configurations.SizeClassIDs";
  public static final String ConfigurationsAllowedWorkflowStates = "Configurations.AllowedWorkflowStates";
  public static final String ConfigurationsValidDivisions = "Configurations.ValidDivisions";
  public static final String ConfigurationsAttributeDivision = "Configurations.AttributeDivision";

  public static final String ConfigurationsItemTypeSize = "Configurations.ItemTypeSize";
  public static final String ConfigurationsItemTypeColor = "Configurations.ItemTypeColor";
  public static final String ConfigurationsItemTypeProduct = "Configurations.ItemTypeProduct";

  private Map<String, String> configurations = new HashMap<String, String>();

  /**
   * Default constructor. Validates the user Configurations.
   * 
   * @throws LEDemandwareExportException on Invalid Configurations
   */
  public LEDemandwareExportProperties() throws LEDemandwareExportException
  {
    try
    {
      this.validateProperties(LEDemandwareExportXmlApi.config);
    }
    catch(Exception exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
      throw new LEDemandwareExportException(exception.getMessage());
    }
  }


  /**
   * Validates the Demandware Export properties
   * 
   * @param configProperties Map contains key, value pairs
   * 
   * @throws LEDemandwareExportException On invalid config
   * @throws ClassNotFoundException      On invalid config
   * @throws IllegalArgumentException    On invalid config
   * @throws IllegalAccessException      On invalid config
   */
  public void validateProperties(Map<String, String> configProperties) throws LEDemandwareExportException, ClassNotFoundException, IllegalArgumentException, IllegalAccessException
  {
    Class<?> controllerClass = null;

    try
    {
      controllerClass = Class.forName("com.lacoste.api.LEDemandwareExportProperties");
    }
    catch(ClassNotFoundException exception)
    {
      throw new LEDemandwareExportException(exception.getMessage());
    }

    Field[] fields = controllerClass.getDeclaredFields();


    for ( Field field : fields )
    {
      String propertyName = field.getName();

      if(propertyName.startsWith("Configurations"))
      {
        propertyName = field.get(null).toString();
        String propertyValue = configProperties.get(propertyName);

        if(!configProperties.containsKey(propertyName))
        {
          throw new LEDemandwareExportException(
            "Property '" + propertyName + "' missing in LacosteDemandwareExport.xml file"
          );
        }
        else if(propertyName.endsWith("Timestamp"))
        {
          if(!propertyValue.isEmpty() && !isValidDate(propertyValue, "yyyy-MM-dd HH:mm:ss"))
          {
            throw new LEDemandwareExportException(
              "Property '" + propertyName + "' has invalid date format in LacosteDemandwareExport.xml file. Valid Date format is 'yyyy-MM-dd HH:mm:ss'"
            );
          }
        }
        else if(propertyValue.isEmpty())
        {
          throw new LEDemandwareExportException(
            "Property '" + propertyName + "' empty in LacosteDemandwareExport.xml file."
          );
        }

        configurations.put(propertyName, propertyValue);
      }
    }
  }


  /**
   * Gets the Cassandra Address from configuration cache
   * 
   * @return Cassandra Address
   */
  public String getCassandraAddress()
  {
    return this.configurations.get(ConfigurationsCassandraAddress);
  }


  /**
   * Gets the Cassandra Keyspace from configuration cache
   * 
   * @return Cassandra Keyspace
   */
  public String getCassandraKeyspace()
  {
    return this.configurations.get(ConfigurationsCassandraKeyspace);
  }


  /**
   * Gets the Elasticsearch Address from configuration cache
   * 
   * @return Elasticsearch Address
   */
  public String getElasticsearchAddress()
  {
    return this.configurations.get(ConfigurationsElasticsearchAddress);
  }


  /**
   * Gets the Elasticsearch Tcp Port from configuration cache
   * 
   * @return Elasticsearch Tcp Port
   */
  public int getElasticsearchTcpPort()
  {
    return Integer.parseInt(this.configurations.get(ConfigurationsElasticsearchTcpPort));
  }


  /**
   * Gets the Elasticsearch Index from configuration cache
   * 
   * @return Elasticsearch Index
   */
  public String getElasticsearchIndex()
  {
    return this.configurations.get(ConfigurationsElasticsearchIndexName);
  }


  /**
   * Gets the Elasticsearch Cluster from configuration cache
   * 
   * @return Elasticsearch Cluster
   */
  public String getElasticsearchCluster()
  {
    return this.configurations.get(ConfigurationsElasticsearchCluster);
  }


  /**
   * Gets the Start Time stamp
   * 
   * @return Start Time stamp
   */
  public String getStartDate()
  {
    String startTime  = this.configurations.get(ConfigurationsStartTimestamp);

    startTime = (startTime.isEmpty())
      ? getDate(new Date(0), "yyyy-MM-dd HH:mm:ss")
      : startTime;

    return startTime;
  }


  /**
   * Gets the End Time stamp
   * 
   * @return End Time stamp
   */
  public String getEndDate()
  {
    String endTime  = this.configurations.get(ConfigurationsEndTimestamp);

    endTime = (endTime.isEmpty())
      ? getDate(new Date(), "yyyy-MM-dd HH:mm:ss")
      : endTime;

    return endTime;
  }


  /**
   * Gets the Product ID of item type size
   * 
   * @return Product ID of item type size
   */
  public String getItemTypeSize()
  {
    return this.configurations.get(ConfigurationsItemTypeSize);
  }


  /**
   * Gets the Product ID of item type Product
   * 
   * @return Product ID of item type Product
   */
  public String getItemTypeProduct()
  {
    return this.configurations.get(ConfigurationsItemTypeProduct);
  }


  /**
   * Gets the Product ID of item type Color
   * 
   * @return Product ID of item type size
   */
  public String getItemTypeColor()
  {
    return this.configurations.get(ConfigurationsItemTypeColor);
  }


  /**
   * Gets the XML output folder
   * 
   * @return folder name along with relative or absolute path 
   */
  public String getXmlOutputLocation()
  {
    return this.configurations.get("");
  }


  /**
   * Gets the Product batch size
   * 
   * @return Product batch size
   */
  public int getProductBatchSize()
  {
    return Integer.parseInt(this.configurations.get(ConfigurationsProductBatchSize));
  }


  /**
   * Gets the Attribute ID of item type
   * 
   * @return Attribute ID of item type
   */
  public String getAttributeItemType()
  {
    return this.configurations.get(ConfigurationsAttributeItemType);
  }


  /**
   * Gets the Attribute ID of active attribute Eu
   * 
   * @return Attribute ID of item type
   */
  public String getAttributeActiveEu()
  {
    return this.configurations.get(ConfigurationsAttributeActivePeriods);
  }


  /**
   * Gets the Class IDs of class Size
   * 
   * @return Class IDs of class Size
   */
  public String getSizeClassIDs()
  {
    return this.configurations.get(ConfigurationsSizeClassIDs);
  }


  /**
   * Gets the Attribute ID of active attribute Eu
   * 
   * @return Attribute ID of item type
   */
  public String getAttributeColor()
  {
    return this.configurations.get(ConfigurationsAttributeColor);
  }


  /**
   * Gets the allowed work flow state IDs
   * 
   * @return String State IDs separated by comma (,)
   */
  public String getAllowedWorkflowStates()
  {
    return this.configurations.get(ConfigurationsAllowedWorkflowStates);
  }


  /**
   * Gets the ID of product ID attribute from configurations.
   * 
   * @return String State IDs separated by comma (,)
   */
  public String getAttributeProductID()
  {
    return this.configurations.get(ConfigurationsAttributeProductID);
  }


  /**
   * Gets the string representation of the given date
   * 
   * @param date   Date object or null to get current date
   * @param format Date format or empty for "yyyy-MM-dd HH:mm:ss"
   * 
   * @return String representation of the date
   */
  public static String getDate(Date date, String format)
  {
    if(date == null)
    {
      date = new Date();
    }

    String formattedDate = new SimpleDateFormat(format).format(date);

    return formattedDate;
  }


  /**
   * Validates the date with given format
   * 
   * @param dateToValidate Date string
   * @param dateFromat     Date format
   * 
   * @return if valid date then true else false
   */
  public boolean isValidDate(String dateToValidate, String dateFromat)
  {

    if(dateToValidate == null) {
      return false;
    }

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFromat);
    simpleDateFormat.setLenient(false);

    try
    {
      simpleDateFormat.parse(dateToValidate);
    } catch (ParseException e) {
      return false;
    }

    return true;
  }


  /**
   * Gets the Start Time stamp
   * 
   * @return Start Time stamp
   */
  public String getStartDateForReferenceUpdateLink()
  {
    String startTime  = this.configurations.get(ConfigurationsStartTimestamp);

    return startTime;
  }


  /**
   * Gets the list of valid division IDs separated by comma.
   * 
   * @return valid division IDs
   */
  public String getValidDivisions()
  {
    return this.configurations.get(ConfigurationsValidDivisions);
  }


  /**
   * Gets the Attribute ID which stores Color's Default Position. The type of this attribute is number.
   * 
   * @return Attribute ID of color's default position
   */
  public String getAttributeColorDefaultPosition()
  {
    return this.configurations.get(ConfigurationsAttributeColorDefaultPosition);
  }


  /**
   * Gets the Attribute ID which stores Division. The type of this attribute is articlereference.
   * 
   * @return Attribute ID of Divisions
   */
  public String getAttributeIdDivisions()
  {
    return this.configurations.get(ConfigurationsAttributeDivision);
  }
}
