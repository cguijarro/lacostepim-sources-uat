package com.lacoste.plugins.transformations;

import com.lacoste.api.LEDemandwareExportXmlApi;
import com.lacoste.api.LETransformationPlugin;
import com.lacoste.api.TreeNode;


/**
 * Transformation plugin to get the seasonal data for a product for a particular site.
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class SeasonalDataTransformation extends LETransformationPlugin
{


  private final String SEASON_PRODUCT = "product";
  private final String SEASON_WINTER  = "winter";
  private final String SEASON_SUMMER  = "summer";


  @Override
  public int getPriority()
  {
    return 0;
  }


  @Override
  public String transform(TreeNode node, String nodeValue)
  {
    String value = "";

    if(this.getValue().equalsIgnoreCase("true"))
    {
      String attributeIds[] = node.getValue().split(",");
      if(attributeIds.length == 3)
      {
        String productAttribute = attributeIds[0];
        String summerAttribute  = attributeIds[1];
        String winterAttribute  = attributeIds[2];
        String season           = "SeasonalData" + LEDemandwareExportXmlApi.sites.get(node.getLanguage());
        season                  = (LEDemandwareExportXmlApi.getValue(season, "")).toLowerCase();

        switch(season)
        {
          case SEASON_PRODUCT :
            value = getDataForProduct(productAttribute);
            break;

          case SEASON_SUMMER :
            value = getSeasonalData(productAttribute, summerAttribute);
            break;

          case SEASON_WINTER :
            value = getSeasonalData(productAttribute, winterAttribute);
            break;

          default:
            value = "";
        }
      }
      else
      {
        value = "";
      }
    }

    return value;
  }


  @Override
  public String transform(TreeNode node, String nodeName, String nodeValue)
  {
    return nodeValue;
  }

  private String getDataForProduct(String productAttribute)
  {
    return LEDemandwareExportXmlApi.getValue(productAttribute, "");
  }


  private String getSeasonalData(String productAttribute, String seasonalAttribute)
  {
    String seasonalData = LEDemandwareExportXmlApi.getValue(seasonalAttribute, "");

    return seasonalData.equals("") ? LEDemandwareExportXmlApi.getValue(productAttribute, "") : seasonalData;
  }
}
