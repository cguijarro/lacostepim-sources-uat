package com.lacoste.exception;

/**
 * Exception class for Demandware Export process
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */

public class LEDemandwareExportException extends Exception
{

  private static final long serialVersionUID = 1L;

  public LEDemandwareExportException(String message)
  {
    super(message);
  }
}
