package com.lacoste.api;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.common.base.Splitter;
import com.lacoste.exception.LEDemandwareExportException;


/**
 * Defines the structure of XML file created in Demandware Export process.
 * Parse the XML structure and generates a tree.
 * Provides APIs to generate XML file using structure.
 *
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class LEDemandwareExportXmlTree
{

  TreeNode root;

  public static final String XVALUETRANSFORM = "X-VALUE-TRANSFORM";
  public static final String XVALUE          = "X-VALUE";
  public static final String XMULTIPLE       = "X-MULTIPLE";
  public static final String XMLLANG         = "xml:lang";
  public static final String SITEID          = "site-id";
  public static final String XATTRIBUTETRANSFORM = "X-ATTRIBUTE-TRANSFORM";


  public LEDemandwareExportXmlTree(TreeNode rootNode)
  {
    this.root = rootNode;
  }


  /**
   * Gets the root node of XML structure
   * 
   * @return root Object of TreeNode
   */
  public TreeNode getRoot() {
    return root;
  }


  /**
   * Sets the root node of XML structure
   * 
   * @param root Object of TreeNode
   */
  public void setRoot(TreeNode root) {
    this.root = root;
  }


  /**
   * create a tree of XML tags each tag in configuration XML is converted to a TreeNode
   * 
   * @param node XML node
   * 
   * @return TreeNode
   */
  public static TreeNode createTree(Node node)
  {
    TreeNode root = new TreeNode();
    root.setNodeName(node.getNodeName());

    Map<String, String> attrMap = new HashMap<>();
    NamedNodeMap map = node.getAttributes();

    if(map != null)
    {
      for(int j = 0; j < map.getLength(); j++)
      {
        Node d = map.item(j);
        if(!d.getNodeName().equals(XVALUETRANSFORM) && !d.getNodeName().equals(XVALUE)) {
          LEDemandwareExportXmlApi.collectAttributes(d.getNodeValue());
        }
        attrMap.put(d.getNodeName(), d.getNodeValue());
      }


      if(attrMap.containsKey(XVALUETRANSFORM))
      {
        String transformationData = attrMap.get(XVALUETRANSFORM);
        List<LETransformationPlugin> transformMatrix = getTransfomationPlugins(transformationData);
        root.setTransformMatrix(transformMatrix);
        root.setTransformationFlag(true);

        attrMap.remove(XVALUETRANSFORM);
      }

      if(attrMap.containsKey(XMULTIPLE))
      {
        if(!attrMap.get(XMULTIPLE).isEmpty())
        {
          LEMultiplePlugin plugin = getMultipleTagPluginByName(attrMap.get(XMULTIPLE));
          if(plugin != null) {
            root.setMultiple(true);
            root.setMultiplePlugin(plugin);
          }
        }

        attrMap.remove(XMULTIPLE);
      }

      if(attrMap.containsKey(XVALUE))
      {
        if(!attrMap.get(XVALUE).equals(""))
        {
          String value = attrMap.get(XVALUE);
          root.setValue(value);
        }

        attrMap.remove(XVALUE);
      }

      if(attrMap.containsKey(XMLLANG))
      {
        root.setLanguageDependent(true);
        LEDemandwareExportXmlApi.collectLanguageAttributes(root.getValue());
      }
      else
      {
        if(!root.getValue().isEmpty()) {
          LEDemandwareExportXmlApi.collectAttributes(root.getValue());
        }
      }

      if(attrMap.containsKey(SITEID))
      {
        String siteID = attrMap.get(SITEID);
        siteID += root.getNodeName();
        LEDemandwareExportXmlApi.collectActiveAttributes(siteID + "_" + root.getValue());
        root.setValue(siteID);
      }

      if(attrMap.containsKey(XATTRIBUTETRANSFORM))
      {
        String attributeTransformations = attrMap.get(XATTRIBUTETRANSFORM);
        Map<String, List<LETransformationPlugin>> attributeTransformationMatrix = parseAttributeTransformationPlugins(attributeTransformations);
        root.setAttributeTransformationFlag(true);
        root.setAttributeTransformMatrix(attributeTransformationMatrix);
        attrMap.remove(XATTRIBUTETRANSFORM);
      }
    }

    root.setAttributes(attrMap);
    NodeList childNodes = node.getChildNodes();

    if(childNodes.getLength() != 0)
    {
      List<TreeNode> nodeList = new LinkedList<>();

      for(int k = 0; k < childNodes.getLength(); k++)
      {
        Node childNode = childNodes.item(k);

        if(childNode.getNodeType() == Node.ELEMENT_NODE)
        {
          root.setLeafNode(true);
          nodeList.add(createTree(childNode));
        }
      }

      root.setChildNodes(nodeList);
      root.setLeafNode(false);
    }

    return root;
  }


  /**
   * Creates product specific XML tag from Structure
   * 
   * @param node                   TreeNode representing a XML node
   * @param demandwareExportXmlApi Object of Export APIs
   * @param document               Document object
   * @param productID              Product ID
   * 
   * @return XML element which can be transformed to XML file
   */
  public Element createXmlFile(
    TreeNode node,
    LEDemandwareExportXmlApi demandwareExportXmlApi,
    Document document,
    long productID
  )
  {
    String nodeValue = node.getValue();

    Map<String, String> attMap = new HashMap<>();
    nodeValue = LEDemandwareExportXmlApi.getValue(nodeValue, node.getNodeName());
    if(node.getAttributes() != null && node.getAttributes().containsKey(SITEID) && nodeValue.isEmpty())
    {
      return null;
    }

    if(node.getAttributes() != null) {
      Iterator<Entry<String, String>> attributeMapIterator = node.getAttributes().entrySet().iterator();
      while(attributeMapIterator.hasNext())
      {
        Entry<String, String> entry = attributeMapIterator.next();
        String attributeValue = entry.getValue();
        attributeValue = LEDemandwareExportXmlApi.getValue(attributeValue, entry.getKey(), node.getLanguage());
        attributeValue = getTransformedAttributeValue(node, entry.getKey(), attributeValue);
        attMap.put(entry.getKey(), attributeValue);
      }
    }

    Element element = demandwareExportXmlApi.createXmlElement(
      document,
      node.getNodeName(),
      ((node.isLeafNode()) ? "" : getTransformedNodeValue(node, nodeValue)),
      attMap
    );

    List<TreeNode> list = node.getChildNodes();

    if(list != null) {
      for(TreeNode childNode : list)
      {
        if(childNode.isMultiple())
        {
          List<String> multipleData = childNode.getMultiplePlugin().getData(childNode);
          for(String sizeID : multipleData)
          {
            Map<String, String> data = LEDemandwareExportXmlApi.getData();
            long sizeProductID = Long.parseLong(sizeID);
            try
            {
              LEDemandwareExportApi.setProductValues(sizeProductID);
              demandwareExportXmlApi.appendChild(element, createXmlFile(childNode, demandwareExportXmlApi, document, sizeProductID));
            }
            catch(LEDemandwareExportException exception)
            {
              LEDemandwareExportApi.printStackTrace(exception);
              LEDemandwareExportLogger.log(exception.getMessage());
            }
            LEDemandwareExportXmlApi.setData(data);
          }
        }
        else if(childNode.isLanguageDependent())
        {
          String value = childNode.getValue();
          for(String lang : LEDemandwareExportXmlApi.dataLanguages.keySet())
          {
            String csLang = LEDemandwareExportXmlApi.dataLanguages.get(lang);
            TreeNode newNode = new TreeNode();
            newNode.setNodeName(childNode.getNodeName());
            newNode.setValue(this.adjustValue(value, csLang));
            newNode.setTransformationFlag(childNode.isTransformationFlag());
            newNode.setTransformMatrix(childNode.getTransformMatrix());
            newNode.setLanguage(csLang);
            newNode.setAttributeTransformationFlag(childNode.isAttributeTransformationFlag());
            newNode.setAttributeTransformMatrix(childNode.getAttributeTransformMatrix());

            Map<String, String> attributes = childNode.getAttributes();
            attributes.put(XMLLANG, "Configurations." + lang);

            newNode.setAttributes(attributes);
            demandwareExportXmlApi.appendChild(element, createXmlFile(newNode, demandwareExportXmlApi, document, productID));
          }
        }
        else
        {
          demandwareExportXmlApi.appendChild(element, createXmlFile(childNode, demandwareExportXmlApi, document, productID));
        }
      }
    }

    return element;
  }


  /**
   * Append the language code to the attribute ID
   * 
   * @param value  Attribute ID
   * @param csLang CONTENTSERV language code
   * 
   * @return adjusted attribute IDs
   */
  public String adjustValue(String value, String csLang)
  {
    return (value.replaceAll(",", csLang + ",") + csLang);
  }


  /**
   * Creates product specific XML tag from Structure
   * 
   * @param node                   TreeNode representing a XML node
   * @param demandwareExportXmlApi Object of Export APIs
   * @param document               Document object
   * 
   * @return XML element which can be transformed to XML file
   */
  public Element createXmlFile(
    TreeNode node,
    LEDemandwareExportXmlApi demandwareExportXmlApi,
    Document document
  )
  {
    Map<String, String> attributesMap = new HashMap<>();

    if(node.getAttributes() != null)
    {
      for(String conf : node.getAttributes().keySet())
      {
        String nodeValue = node.getAttributes().get(conf);
        nodeValue = LEDemandwareExportXmlApi.getValue(nodeValue, conf, node.getLanguage());
        nodeValue = getTransformedAttributeValue(node, conf, nodeValue);
        attributesMap.put(conf, nodeValue);
      }
    }

    Element element = demandwareExportXmlApi.createXmlElement(
      document,
      node.getNodeName(),
      ((node.isLeafNode()) ? "" : getTransformedNodeValue(node, node.getValue())),
      attributesMap
    );

    List<TreeNode> list = node.getChildNodes();

    if(list != null) {
      for(TreeNode childNode : list)
      {
        if(childNode.isMultiple())
        {
          List<String> multipleData = childNode.getMultiplePlugin().getData(childNode);
          for(String sizeID : multipleData)
          {
            Map<String, String> data = LEDemandwareExportXmlApi.getData();
            long sizeProductID = Long.parseLong(sizeID);
            try
            {
              LEDemandwareExportApi.setProductValues(sizeProductID);
              demandwareExportXmlApi.appendChild(
                element,
                createXmlFile(childNode, demandwareExportXmlApi, document, sizeProductID)
              );
            }
            catch(LEDemandwareExportException exception)
            {
              LEDemandwareExportApi.printStackTrace(exception);
              LEDemandwareExportLogger.log(exception.getMessage());
            }
            LEDemandwareExportXmlApi.setData(data);
          }
        }
        else {
          demandwareExportXmlApi.appendChild(
            element,
            createXmlFile(childNode, demandwareExportXmlApi, document)
          );
        }
      }
    }

    return element;
  }


  /**
   * Converts the transformation parameters to Map
   * 
   * @param transformationParams
   * 
   * @return transformation matrix
   */
  public static Map<String, String> splitToMap(String transformationParams, String pluginSeperator, String keyValueSeperator)
  {
    return Splitter.on(pluginSeperator).withKeyValueSeparator(keyValueSeperator).split(transformationParams);
  }


  /**
   * Gets the transformed node value if transformation parameters are provided
   * 
   * @param node      TreeNode representing a XML tag
   * @param nodeValue Value of node which is to be transformed
   * 
   * @return Transformed node value
   */
  private static String getTransformedNodeValue(TreeNode node, String nodeValue)
  {
    String value = nodeValue;

    if(node.isTransformationFlag())
    {
      for(LETransformationPlugin plugin : node.getTransformMatrix())
      {
        value = plugin.transform(node, value);
      }
    }

    return value;
  }


  /**
   * Gets the instance of multiple plugin by its name.
   *
   * @param plugin Name of the Multiple Plugin.
   *
   * @return Instance of Multiple Plugin.
   */
  public static LEMultiplePlugin getMultipleTagPluginByName(String plugin)
  {
    String name = plugin;
    String pluginValue = "";

    if(plugin.contains("="))
    {
      String [] pluginsInfo = plugin.split("=");
      name = pluginsInfo[0];
      pluginValue = pluginsInfo[1];
    }

    String pluginName = "com.lacoste.plugins.multiple." + name + "Multiple";
    try
    {
      Object multiplePlugin = Class.forName(pluginName).newInstance();
      if(multiplePlugin instanceof LEMultiplePlugin)
      {
        LEMultiplePlugin pluginObj = (LEMultiplePlugin) multiplePlugin;
        pluginObj.setValue(pluginValue);
        pluginObj.parsePluginConf();

        return pluginObj;
      }
    }
    catch(Exception e)
    {
      LEDemandwareExportLogger.log("'" + plugin + "' Multiple Tags Plugin not supported.");
    }

    return null;
  }


  /**
   * Parse JSON for X-ATTRIBUTE-TRANSFORM to Map store into a static variable
   * 
   * @param attributeTransformations JSON attribute value transformation matrix
   * 
   * @return Map of transformations. format : { <ATTRIBUTE-NAME> => {<TRANSFORMATION-PLUGIN> : <VALUE>, ... } ... }
   */
  public static Map<String, List<LETransformationPlugin>> parseAttributeTransformationPlugins(String attributeTransformations)
  {
    attributeTransformations = attributeTransformations.replace('\'', '"');
    Map<String, Map<String, String>> transformationPlugins = new HashMap<>();
    Map<String, List<LETransformationPlugin>> transformPlugins = new HashMap<>();
    JSONParser parser = new JSONParser();
    try
    {
      JSONObject jsonObject = (JSONObject) parser.parse(attributeTransformations);
      for(Object attributeName : jsonObject.keySet())
      {
        Object transformations = jsonObject.get(attributeName.toString());
        Map<String, String> attributeTransfomationPlugins = new HashMap<>();
        List<LETransformationPlugin> attributeTransformPlugins = new ArrayList<>();
        if(transformations instanceof JSONArray)
        {
          JSONArray transformationArray = (JSONArray) transformations;
          for(int i = 0; i < transformationArray.size(); i++)
          {
            String trannfomationPlugin = transformationArray.get(i).toString();
            String pluginInfo[] = trannfomationPlugin.split("=");
            LETransformationPlugin plugin = LEDemandwareExportApi.getTransformationPluginByName(pluginInfo[0]);
            if(plugin != null) {
              plugin.setValue(pluginInfo[1]);
              attributeTransfomationPlugins.put(pluginInfo[0], pluginInfo[1]);
              attributeTransformPlugins.add(plugin);
            }
          }
        }
        transformationPlugins.put(attributeName.toString(), attributeTransfomationPlugins);
        attributeTransformPlugins.sort(Comparator.comparing(LETransformationPlugin::getPriority));
        transformPlugins.put(attributeName.toString(), attributeTransformPlugins);
      }
    }
    catch (ParseException e)
    {
      LEDemandwareExportLogger.log("Invalid Config JSON for Attribute Transformation plugin - " + attributeTransformations);
    }


    return transformPlugins;
  }


  /**
   * Gets the transformed attribute value if transformation parameters are provided
   * 
   * @param node           TreeNode representing a XML tag
   * @param attributeName  Value of attribute
   * @param attributeValue Value of node to be transformed
   * 
   * @return Transformed attribute value
   */
  private static String getTransformedAttributeValue(TreeNode node, String attributeName, String attributeValue)
  {
    String value = attributeValue;
    if(node.isAttributeTransformationFlag() && node.getAttributeTransformMatrix().containsKey(attributeName))
    {
      List<LETransformationPlugin> transformationMatrix = node.getAttributeTransformMatrix().get(attributeName);
      for(LETransformationPlugin plugin : transformationMatrix)
      {
        value = plugin.transform(node, attributeName, value);
      }
    }

    return value;
  }


  /**
   * Gets the sorted List of transformation plugins to transform node value
   * 
   * @param transformationData
   * 
   * @return
   */
  public static List<LETransformationPlugin> getTransfomationPlugins(String transformationData)
  {
    List<LETransformationPlugin> list = new ArrayList<>();
    Map<String, String> transformMatrix = splitToMap(transformationData, ";", "=");

    for(Entry <String, String> entry : transformMatrix.entrySet())
    {
      LETransformationPlugin plugin = LEDemandwareExportApi.getTransformationPluginByName(entry.getKey());
      if(plugin != null) {
        plugin.setValue(entry.getValue());
        list.add(plugin);
      }
    }

    list.sort(Comparator.comparing(LETransformationPlugin::getPriority));

    return list;
  }
}

