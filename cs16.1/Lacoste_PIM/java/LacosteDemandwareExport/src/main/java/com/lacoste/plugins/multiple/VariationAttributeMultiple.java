package com.lacoste.plugins.multiple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.lacoste.api.LEDemandwareExportApi;
import com.lacoste.api.LEDemandwareExportXmlApi;
import com.lacoste.api.LEMultiplePlugin;
import com.lacoste.api.TreeNode;


/**
 * Multiple Tag Plugin for Variation Attribute tag
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class VariationAttributeMultiple extends LEMultiplePlugin
{


  @Override
  public List<String> getData(TreeNode node)
  {
    List<String> targetIDs = new ArrayList<>();

    try
    {
      String attributeID          = node.getValue();
      long productID              = Long.parseLong(LEDemandwareExportXmlApi.getData().get("PdmarticleID"));
      Map<String, String> product = LEDemandwareExportApi.getTargetByID(productID);
      List<String> referenceList  = Arrays.asList(product.get(attributeID).split(","));
      targetIDs                   = ((referenceList != null) ? referenceList : targetIDs);
    }
    catch(Exception exception)
    {
    }

    return targetIDs;
  }
}
