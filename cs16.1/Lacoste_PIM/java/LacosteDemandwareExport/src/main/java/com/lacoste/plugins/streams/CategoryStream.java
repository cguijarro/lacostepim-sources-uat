package com.lacoste.plugins.streams;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.lacoste.api.LEDemandwareExportApi;
import com.lacoste.api.LEDemandwareExportBatch;
import com.lacoste.api.LEDemandwareExportProperties;
import com.lacoste.api.LEDemandwareExportXmlApi;
import com.lacoste.api.LEStreamExportPlugin;
import com.lacoste.exception.LEDemandwareExportException;


/**
 * Stream Export plugin implements the export for Category stream
 *
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class CategoryStream extends LEStreamExportPlugin
{

  @Override
  public List<LEDemandwareExportBatch> getExportableItemBatches() throws LEDemandwareExportException
  {
    return LEDemandwareExportApi.getExportableItemBatches(
      new ArrayList<>(),
      LEDemandwareExportApi.updatedProductIDs
    );
  }

  @Override
  public String getOuputXmlName(int batchID)
  {
    Date currentDate = new Date();
    String outputXmlName =  LEDemandwareExportXmlApi.config.get("Configurations.OutputLocation") + File.separator
      + "xml" + File.separator
      + "DemandwareExport_Category_" + LEDemandwareExportProperties.getDate(currentDate, "yy_MM_dd_HH_mm_ss")
      + "_" + batchID + ".xml";

    outputXmlName = Paths.get(outputXmlName).toString();

    return outputXmlName;
  }

  @Override
  public String getName()
  {
    return "Category";
  }
}
