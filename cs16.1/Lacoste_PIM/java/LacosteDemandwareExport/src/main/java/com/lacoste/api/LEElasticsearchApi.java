package com.lacoste.api;

import com.datastax.driver.core.exceptions.NoHostAvailableException;
import com.exportstaging.api.exception.ExportStagingException;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.existsQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;


/**
 * This class provides APIs to search in Elasticsearch
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class LEElasticsearchApi
{
  private static Client client;
  private String indexName;


  /**
   * Creates connection to Elasticsearch for current session. check elasticsearch.properties file for this parameter
   *
   * @param clusterName Name of the cluster
   * @param hostName    Host Address
   * @param hostPort    TCP address
   * @param indexName   Name of the Index. eg elasticsearch.index.prefix + "_" + itemType (Note: everything in lowercase) export_pdmarticle for PIM itemType
   * @throws UnknownHostException If unable to connect to elasticsearch
   */
  public void setConnection(String clusterName, String hostName, int hostPort, String indexName) throws UnknownHostException, ExportStagingException {
    try
    {
      if ((indexName == null) || (indexName == ""))
      {
        throw new ExportStagingException("[ESA Exception] IndexName cannot be null");
      }
      this.indexName = indexName;
      if ((clusterName == null) || (clusterName == "")) {
        throw new ExportStagingException("[ESA Exception] clusterName cannot be null");
      }
      Settings settings = Settings.settingsBuilder().put("cluster.name", clusterName).build();
      client = TransportClient.builder().settings(settings).build().addTransportAddress
      (new InetSocketTransportAddress(InetAddress.getByName(hostName), hostPort));
    }
    catch (NoHostAvailableException e)
    {
      throw new ExportStagingException("Unable to create connection to ElasticSearch. Error cause: " + e.getMessage());
    }
  }


  /**
   * Gets the Updated Size IDs between start time & end time
   * 
   * @param startTime   Start time
   * @param endTime     End Time
   * @param languageID  Language ID
   * @param attributeID Item Type Attribute ID
   * @param targetID    Target ID
   * 
   * @return List of updated PIM products IDs
   * 
   * @throws ExportStagingException
   */
  public List<String> getUpdatedSizeIDs(
    String startTime,
    String endTime,
    String languageID,
    String attributeID,
    String targetID,
    List<String> allowedStates
  ) throws ExportStagingException
  {

    return performSearch(
      createSearchQuery(startTime, endTime, true),
      addStateIDFilter(languageID, attributeID, targetID, allowedStates)
    );
  }


  public Set<String> getNotUpdatedSizeIDs(
      String startTime,
      String endTime,
      String languageID,
      String attributeID,
      String targetID,
      List<String> allowedStates,
      String[] sources,
      List<String> updatedNonSizeItems
    ) throws ExportStagingException
    {

      return performSearch(
        createSearchQuery(startTime, endTime, false),
        addStateIDFilter(languageID, attributeID, targetID, allowedStates),
        sources,
        updatedNonSizeItems
      );
    }

  /**
   * Generate filter
   * 
   * @param startTime Start time
   * @param endTime   End Time
   * 
   * @return BoolQueryBuilder start Date & End Date Filter
   */
  private BoolQueryBuilder startDateEndDateFilter(
    String startTime,
    String endTime,
    boolean must
  )
  {
    BoolQueryBuilder builders = new BoolQueryBuilder();

    if(must)
    {
      builders.should(QueryBuilders.boolQuery().must(
        QueryBuilders.rangeQuery("LastChange.date").lte(endTime).gte(startTime))
      );
    }
    else
    {
      builders.should(QueryBuilders.boolQuery().must(
        QueryBuilders.rangeQuery("LastChange.date").lte(startTime))
      );
    }

    return builders;
  }


  /**
   * Perform search in Elasticsearch
   * 
   * @param query  BoolQueryBuilder object represents the Elasticsearch query
   * @param filter QueryBuilder object represents the filters to be applied on search results
   * 
   * @return List of Product IDs
   * 
   * @throws ExportStagingException
   */
  private List<String> performSearch(BoolQueryBuilder query, QueryBuilder filter) throws ExportStagingException
  {
    List<String> results = new ArrayList<String>();
    if (this.indexName == null) {
      throw new ExportStagingException("IndexName not provided");
    }
    if (client == null)
    {
      throw new ExportStagingException("[ESA Exception] Elasticsearch connection error. Retry");
    }
    try
    {
      SearchResponse scrollResp = client.prepareSearch(indexName)
       .setTypes("item")
       .setQuery(query)
       .setPostFilter(filter)
       .setFetchSource(new String[]{"_id"}, null)
       .setScroll(new TimeValue(60000))
       .setSize(10000).get();
      do
      {
        for (SearchHit hit : scrollResp.getHits().getHits())
        {
          results.add(hit.getId().split("_")[0]);
        }

        scrollResp = client.prepareSearchScroll(scrollResp.getScrollId())
          .setScroll(new TimeValue(60000))
          .execute().actionGet();
      } while (scrollResp.getHits().getHits().length != 0);

    } catch (Exception e) {
      LEDemandwareExportApi.printStackTrace(e);
      throw new ExportStagingException("Error cause: " + e.getMessage());
    }

    return results;
  }


  /**
   * Create Search Query
   * 
   * @param startTime Start time
   * @param endTime   End time
   * 
   * @return BoolQueryBuilder Search query object
   */
  private BoolQueryBuilder createSearchQuery(
      String startTime,
      String endTime,
      boolean must
  )
  {
    return startDateEndDateFilter(startTime, endTime, must);
  }


  /**
   * Create Post filters to be applied on search results
   * 
   * @param languageID  Language ID
   * @param attributeID Reference to PIM attribute ID for Item Type
   * @param targetID    PIM item ID that represents the Item Type Size
   * 
   * @return QueryBuilder Post filters
   */
  private QueryBuilder createPostFilterForItemType(String languageID, String attributeID, String targetID)
  {
    return QueryBuilders.boolQuery().must(
      matchQuery("LanguageID", languageID)
    ).must(
      QueryBuilders.matchQuery("Reference." + attributeID + ".TargetID", targetID)
    );
  }


  /**
   * Create post filters to get the Size items based on the allowed work flow states
   * 
   * @param languageID  Language ID
   * @param attributeID Item type attribute ID
   * @param targetID    PIM ID as item type
   * @param stateIDs    Allowed work flow State IDs
   * 
   * @return QueryBuilder object
   */
  private QueryBuilder addStateIDFilter(String languageID, String attributeID, String targetID, List<String> stateIDs)
  {
    return QueryBuilders.boolQuery().must(
      matchQuery("LanguageID", languageID)
    ).must(
      QueryBuilders.matchQuery("Reference." + attributeID + ".TargetID", targetID)
    ).must(
      QueryBuilders.termsQuery("StateID", stateIDs)
    );
  }


  /**
   * Gets the IDs of Color products having item type COLOR.
   * 
   * @param idProductAttributeID "ID product" Attribute ID
   * @param idProduct            value of "ID product" attribute
   * @param itemTypeAttributID   Item Type attribute
   * @param itemTypeColor        Item Type Color
   * @param activeAttributeEuID  Active Demandware EU attribute
   * @param languageID           Language ID
   * 
   * @return List of COLOR IDs
   * 
   * @throws ExportStagingException on failure
   */
  public List<String> getColorsForIDProduct(
    String idProductAttributeID,
    String idProduct,
    String itemTypeAttributID,
    String itemTypeColor,
    String activeAttributeEuID,
    String languageID
  ) throws ExportStagingException
  {
    return performSearch(
      createSearchQueryForColors(idProductAttributeID, idProduct, activeAttributeEuID),
      createPostFilterForItemType(languageID, itemTypeAttributID, itemTypeColor)
    );
  }


  /**
   * Created the Query builder for Elasticsearch
   * 
   * @param idProductAttributeID "ID product" Attribute ID
   * @param idProduct            value of "ID product" attribute
   * @param activeAttributeEuID  Active Demandware EU attribute
   * 
   * @return Object of BoolQueryBuilder
   */
  private BoolQueryBuilder createSearchQueryForColors(
    String idProductAttributeID,
    String idProduct,
    String activeAttributeEuID
  )
  {
    BoolQueryBuilder builders = new BoolQueryBuilder();

    builders.should(QueryBuilders.boolQuery().must(
      matchQuery(idProductAttributeID + ":Value", idProduct)
    ).must(
      existsQuery("Reference." + activeAttributeEuID))
    );

    return builders;
  }


  /**
   * Gets the List of Size IDs updated due to Reference link.
   * 
   * @param startTime                    Start time
   * @param endTime                      End time
   * @param languageID                   Language ID
   * @param attributeID                  Attribute ID
   * @param targetID                     Target ID
   * @param articlereferenceAttributeIDs List of Attribute IDs
   * @param allowedStates                Allowed work flow states
   * 
   * @return Set of Updated size IDs due to reference link
   * 
   * @throws ExportStagingException
   */
  public Set<String> getUpdatedSizeIDsDueToReferenceLink(
      String startTime,
      String endTime,
      String languageID,
      String attributeID,
      String targetID,
      Set<Long> articlereferenceAttributeIDs,
      List<String> allowedStates
  ) throws ExportStagingException
  {

    //update Product IDs since last run (target IDs)
    List<String> updatedNonSizeItemsSinceLastRun = performSearch(
      createSearchQuery(startTime, endTime, true),
      createPostFilterForOtherThanSize(languageID, attributeID, targetID)
    );

    String [] sources = new String[articlereferenceAttributeIDs.size()];
    int i = 0;
    for(long id : articlereferenceAttributeIDs)
    {
      sources[i++] = "Reference." + id + ".TargetID";
    }

    Set<String> notUpdatedSizeIdsSinceLastRun = new HashSet<>();
    if(updatedNonSizeItemsSinceLastRun != null && updatedNonSizeItemsSinceLastRun.size() != 0)
    {
      //update size IDs due to reference link
      notUpdatedSizeIdsSinceLastRun = getNotUpdatedSizeIDs(
        startTime,
        endTime,
        languageID,
        attributeID,
        targetID,
        allowedStates,
        sources,
        updatedNonSizeItemsSinceLastRun
      );
    }

    return notUpdatedSizeIdsSinceLastRun;
  }


  /**
   * Create Post filters to be applied on search results
   * 
   * @param languageID  Language ID
   * @param attributeID Reference to PIM attribute ID for Item Type
   * @param targetID    PIM item ID that represents the Item Type Size
   * 
   * @return QueryBuilder Post filters
   */
  private QueryBuilder createPostFilterForOtherThanSize(String languageID, String attributeID, String targetID)
  {
    return QueryBuilders.boolQuery().must(
      matchQuery("LanguageID", languageID)
    ).mustNot(
      QueryBuilders.matchQuery("Reference." + attributeID + ".TargetID", targetID)
    );
  }


  /**
   * Gets the Product Items
   * 
   * @param itemTypeProduct      Item type ID of product items
   * @param itemTypeAttributeID  Item type attribute
   * @param productIDAttributeID ID product attribute
   * @param sizeIDs              List of Size IDs
   * 
   * @return List of product item IDs for provided size items
   * 
   * @throws ExportStagingException On error
   */
  public List<String> getProductItems(
    String itemTypeProduct,
    String itemTypeAttributeID,
    String productIDAttributeID,
    List<String> sizeIDs
  ) throws ExportStagingException
  {
    BoolQueryBuilder builders = new BoolQueryBuilder();
    productIDAttributeID += ":Value";
    Set<String> productIDs = new HashSet<>(
      getProductIDsForSizes(productIDAttributeID, sizeIDs)
    );

    if(productIDs.size() != 0) {
    for(String productID : productIDs) {
      builders.should(QueryBuilders.matchQuery(productIDAttributeID, productID));
    }

    return performSearch(
      builders,
      createPostFilterForItemType(LEDemandwareExportApi.LANGUAGE_ID, itemTypeAttributeID, itemTypeProduct)
    );
    }
    else
    {
      return new ArrayList<String>();
    }
  }


  /**
   * Gets the list of ID product values for size items
   * 
   * @param itemTypeProduct      Item type ID of product items
   * @param itemTypeAttributeID  Item type attribute
   * @param productIDAttributeID ID product attribute
   * @param sizeIDs              List of Size IDs
   * 
   * @return List of ID product values for size items
   * 
   * @throws ExportStagingException On error
   */
  public List<String> getProductIDsForSizes(
    String productIDAttributeID,
    List<String> sizeIDs
  ) throws ExportStagingException
  {
    BoolQueryBuilder builders = new BoolQueryBuilder();

    builders.must(QueryBuilders.matchAllQuery());

    return performSearch(
      builders,
      createPostFilterForGetProductIDsForSizes(LEDemandwareExportApi.LANGUAGE_ID, sizeIDs),
      productIDAttributeID
    );
  }


  /**
   * Create Post filters to be applied on search results
   * 
   * @param languageID  Language ID
   * @param sizeIDs     Size IDs
   * 
   * @return QueryBuilder Post filters
   */
  private QueryBuilder createPostFilterForGetProductIDsForSizes(String languageID, List<String> sizeIDs)
  {
    return QueryBuilders.boolQuery().must(
      matchQuery("LanguageID", languageID)
    ).must(
      QueryBuilders.termsQuery("ID", sizeIDs)
    );
  }


  /**
   * Perform search in Elasticsearch & also prepares cache for size & product mapping
   *
   * @param query       BoolQueryBuilder object represents the Elasticsearch query
   * @param filter      QueryBuilder object represents the filters to be applied on search results
   * @param attributeID source attribute ID
   * 
   * @return List of product IDs
   * 
   * @throws ExportStagingException On error
   */
  private List<String> performSearch(
    BoolQueryBuilder query,
    QueryBuilder filter,
    String attributeID
  ) throws ExportStagingException
  {
    List<String> results = new ArrayList<String>();
    if (this.indexName == null) {
      throw new ExportStagingException("IndexName not provided");
    }
    if (client == null)
    {
      throw new ExportStagingException("[ESA Exception] Elasticsearch connection error. Retry");
    }
    try
    {
      SearchResponse scrollResp = client.prepareSearch(indexName)
       .setTypes("item")
       .setQuery(query)
       .setPostFilter(filter)
       .setFetchSource(new String[]{attributeID, "_id"}, null)
       .setScroll(new TimeValue(60000))
       .setSize(10000).get();
      do
      {
        for (SearchHit hit : scrollResp.getHits().getHits())
        {
          String id = hit.getId().split("_")[0];
          if(hit.getSource().get(attributeID) != null) {
            String productID = hit.getSource().get(attributeID).toString();
            if(!productID.trim().isEmpty())
            {
              results.add(productID);

              if(LEDemandwareExportXmlApi.productSizeCache.containsKey(productID))
              {
                List<String> sizes = LEDemandwareExportXmlApi.productSizeCache.get(productID);
                sizes.add(id);
                LEDemandwareExportXmlApi.productSizeCache.put(productID, sizes);
              }
              else
              {
                List<String> sizes = new ArrayList<>();
                sizes.add(id);
                LEDemandwareExportXmlApi.productSizeCache.put(productID, sizes);
              }
            }
          }
        }

        scrollResp = client.prepareSearchScroll(scrollResp.getScrollId())
          .setScroll(new TimeValue(60000))
          .execute().actionGet();
      } while (scrollResp.getHits().getHits().length != 0);

    } catch (Exception e) {
      throw new ExportStagingException("Error cause: " + e.getMessage());
    }

    return results;
  }


  /**
   * Perform search in Elasticsearch
   * 
   * @param query  BoolQueryBuilder object represents the Elasticsearch query
   * @param filter QueryBuilder object represents the filters to be applied on search results
   * @param sources Sources for query
   * @param updatedNonSizeItems List of updated products and not size items
   * 
   * @return List of Product IDs
   * 
   * @throws ExportStagingException
   */
  private Set<String> performSearch(
      BoolQueryBuilder query,
      QueryBuilder filter,
      String[] sources,
      List<String> updatedNonSizeItems
  ) throws ExportStagingException
  {
    Set<String> results = new HashSet<String>();
    if (this.indexName == null) {
      throw new ExportStagingException("IndexName not provided");
    }
    if (client == null)
    {
      throw new ExportStagingException("[ESA Exception] Elasticsearch connection error. Retry");
    }
    try
    {
      SearchResponse scrollResp = client.prepareSearch(indexName)
       .setTypes("item")
       .setQuery(query)
       .setPostFilter(filter)
       .setFetchSource(sources, null)
       .setScroll(new TimeValue(60000))
       .setSize(10000).get();
      do
      {
        for (SearchHit hit : scrollResp.getHits().getHits())
        {
          String sizeID = hit.getId().split("_")[0];

          try {
            Map<String, Object> data = hit.getSource();

            for(String key : data.keySet())
            {
              if(data.get(key) instanceof Map) {
                Map<String, List<Map<String, String>>> ref = (Map<String, List<Map<String, String>>>) data.get(key);
                if(ref != null)
                {
                  for(List<Map<String, String>> targetIDs : ref.values())
                  {
                    for(Map<String, String> target : targetIDs) {
                      for(String targetID : target.values())
                      if(updatedNonSizeItems.contains(targetID))
                      {
                        results.add(sizeID);
                      }
                    }
                  }
                }
              }
            }
          }
          catch(Exception e)
          {
            LEDemandwareExportApi.printStackTrace(e);
          }
        }

        scrollResp = client.prepareSearchScroll(scrollResp.getScrollId())
          .setScroll(new TimeValue(60000))
          .execute().actionGet();
      } while (scrollResp.getHits().getHits().length != 0);

    } catch (Exception e) {
      LEDemandwareExportApi.printStackTrace(e);
      throw new ExportStagingException("Error cause: " + e.getMessage());
    }

    return results;
  }


  /**
   * Filter size IDs for current Size Multiple Plugin configurations
   *
   * @param attributesToCheck   Attribute IDs to check
   * @param validDivisions      Valid Divisions IDs
   * @param sizeIDs             Size IDs
   * @param attributeIdDivision Attribute ID division
   *
   * @return Filtered List of Size IDs
   *
   * @throws ExportStagingException
   */
  public List<String> filterSizeIDsForCurrentConfigurations(
    String attributesToCheck[],
    String validDivisions[],
    List<String> sizeIDs,
    String attributeIdDivision
  ) throws ExportStagingException
  {

    if(validDivisions.length != 0)
    {
      List<String> invalidSizeIDs = getSizeElementsWithValidDivisionsAndEmptyAttributes(
        attributesToCheck,
        validDivisions,
        sizeIDs,
        attributeIdDivision
      );

      sizeIDs.removeAll(invalidSizeIDs);
    }
    else {
      sizeIDs = getSizeElementsByFilledAttributes(
        attributesToCheck,
        sizeIDs
      );
    }


    return sizeIDs;
  }


  /**
   * Gets Size Elements With Valid Divisions And Empty Attributes
   *
   * @param attributesToCheck   Attribute IDs to check
   * @param validDivisions      Valid Divisions IDs
   * @param sizeIDs             Size IDs
   * @param attributeIdDivision Attribute ID division
   *
   * @return List of filtered Size IDs
   *
   * @throws ExportStagingException
   */
  public List<String> getSizeElementsWithValidDivisionsAndEmptyAttributes(
      String attributesToCheck[],
      String validDivisions[],
      List<String> sizeIDs,
      String attributeIdDivision) throws ExportStagingException
  {
    BoolQueryBuilder builders = new BoolQueryBuilder();

    builders.must(
      QueryBuilders.boolQuery().must(
        QueryBuilders.termsQuery("Reference." + attributeIdDivision + ".TargetID", validDivisions)
      )
    );

    BoolQueryBuilder builder = QueryBuilders.boolQuery().must(
      matchQuery("LanguageID", LEDemandwareExportApi.LANGUAGE_ID)
    );

    for(String attributeID : attributesToCheck)
    {
      builder.mustNot(QueryBuilders.existsQuery(attributeID + ":Value"));
    }

    QueryBuilder postFilter = builder.must(
      QueryBuilders.termsQuery("ID", sizeIDs)
    );

    sizeIDs = performSearch(
      builders,
      postFilter
    );

    return sizeIDs;
  }


  /**
   * Gets the Size items in which at least one attribute is filled
   *
   * @param attributesToCheck Attribute IDs to check
   * @param sizeIDs           Size IDs
   *
   * @return Size Items
   *
   * @throws ExportStagingException
   */
  public List<String> getSizeElementsByFilledAttributes(
    String attributesToCheck[],
    List<String> sizeIDs
  ) throws ExportStagingException
  {
    BoolQueryBuilder builders = new BoolQueryBuilder();
    builders.must(QueryBuilders.matchAllQuery());

    BoolQueryBuilder builder = QueryBuilders.boolQuery().must(
      matchQuery("LanguageID", LEDemandwareExportApi.LANGUAGE_ID)
    );

    for(String attributeID : attributesToCheck)
    {
      builder.should(QueryBuilders.existsQuery(attributeID + ":Value"));
    }

    QueryBuilder postFilter = builder.minimumNumberShouldMatch(1).must(
      QueryBuilders.termsQuery("ID", sizeIDs)
    );

    sizeIDs = performSearch(
      builders,
      postFilter
    );

    return sizeIDs;
  }


  /**
   * Filter Product IDs by Plugin configurations
   *
   * @param productIDs                  Product IDs
   * @param attributesToCheck           Attribute IDs to check
   * @param validDivisions              Valid Divisions
   * @param isCheckingForValidDivisions Is Checking For Valid Divisions
   *
   * @return List of Product IDs
   *
   * @throws ExportStagingException
   */
  public List<String> filterProductIDsByPluginConfigurations(
    List<String> productIDs,
    String[] attributesToCheck,
    String[] validDivisions,
    boolean isCheckingForValidDivisions,
    String productIDAttributeID
  ) throws ExportStagingException
  {
    List<String> filteredProductIDs = new ArrayList<>();

    BoolQueryBuilder builders = new BoolQueryBuilder();

    builders.must(QueryBuilders.matchAllQuery());

    productIDs = filterProductsByDivisions(productIDs, validDivisions, isCheckingForValidDivisions);

    Map<String, String> values = getValuesFromElasticSearch(
      builders,
      createPostFilterForGetProductIDsForSizes(LEDemandwareExportApi.LANGUAGE_ID, productIDs),
      productIDAttributeID + ":Value"
    );

    for(String productID : productIDs)
    {
      String IDProduct = values.get(productID);
      if(IDProduct != null && !IDProduct.isEmpty())
      {
        List<String> sizeIDs = LEDemandwareExportXmlApi.productSizeCache.get(IDProduct);
        sizeIDs              = getSizeElementsByFilledAttributes(attributesToCheck, sizeIDs);
        if(sizeIDs != null && !sizeIDs.isEmpty())
        {
          filteredProductIDs.add(productID);
        }
      }
    }

    return filteredProductIDs;
  }


  /**
   * Filter products by Divisions
   *
   * @param productIDs                  Product IDs
   * @param divisions                   Valid Divisions
   * @param isCheckingForValidDivisions Is Checking For Valid Divisions
   *
   * @return Filterd list of Product IDs
   *
   * @throws ExportStagingException
   */
  public List<String> filterProductsByDivisions(List<String> productIDs, String[] divisions, boolean isCheckingForValidDivisions) throws ExportStagingException
  {
    BoolQueryBuilder builders = new BoolQueryBuilder();
    BoolQueryBuilder builder  = new BoolQueryBuilder();
    QueryBuilder postFilter   = null;

    if(isCheckingForValidDivisions)
    {
      for(String divisionID : divisions)
      {
        builders.should(QueryBuilders.matchQuery("Reference." + LEDemandwareExportApi.getAttributeIdDivisions() + ".TargetID", divisionID));
      }

      postFilter = builder.minimumNumberShouldMatch(1).must(
        QueryBuilders.termsQuery("ID", productIDs)
      ).must(
        matchQuery("LanguageID", LEDemandwareExportApi.LANGUAGE_ID)
      );
    }
    else
    {
      builders = new BoolQueryBuilder();

      BoolQueryBuilder invalidDivisionQueryBuilder = new BoolQueryBuilder();

      invalidDivisionQueryBuilder.must(
        QueryBuilders.termsQuery("Reference." + LEDemandwareExportApi.getAttributeIdDivisions() + ".TargetID", divisions)
      );

      builders.mustNot(invalidDivisionQueryBuilder);

      postFilter = QueryBuilders.boolQuery().must(
        matchQuery("LanguageID", LEDemandwareExportApi.LANGUAGE_ID)
      ).must(
        QueryBuilders.termsQuery("ID", productIDs)
      );
    }

    productIDs = performSearch(
      builders,
      postFilter
    );

    return productIDs;
  }


  /**
   * Perform search in Elasticsearch & also prepares cache for size & product mapping
   *
   * @param query       BoolQueryBuilder object represents the Elasticsearch query
   * @param filter      QueryBuilder object represents the filters to be applied on search results
   * @param attributeID source attribute ID
   * 
   * @return Map of product ID and attribute value
   * 
   * @throws ExportStagingException On error
   */
  private Map<String, String> getValuesFromElasticSearch(
    BoolQueryBuilder query,
    QueryBuilder filter,
    String attributeID
  ) throws ExportStagingException
  {
    Map<String, String> results = new HashMap<>();
    if (this.indexName == null) {
      throw new ExportStagingException("IndexName not provided");
    }
    if (client == null)
    {
      throw new ExportStagingException("[ESA Exception] Elasticsearch connection error. Retry");
    }
    try
    {
      SearchResponse scrollResp = client.prepareSearch(indexName)
       .setTypes("item")
       .setQuery(query)
       .setPostFilter(filter)
       .setFetchSource(new String[]{attributeID, "_id"}, null)
       .setScroll(new TimeValue(60000))
       .setSize(10000).get();
      do
      {
        for (SearchHit hit : scrollResp.getHits().getHits())
        {
          String id = hit.getId().split("_")[0];
          if(hit.getSource().get(attributeID) != null) {
            String value = hit.getSource().get(attributeID).toString();
            if(!value.trim().isEmpty())
            {
              results.put(id, value);
            }
          }
        }

        scrollResp = client.prepareSearchScroll(scrollResp.getScrollId())
          .setScroll(new TimeValue(60000))
          .execute().actionGet();
      } while (scrollResp.getHits().getHits().length != 0);

    } catch (Exception e) {
      throw new ExportStagingException("Error cause: " + e.getMessage());
    }

    return results;
  }


  /**
   * Filter size IDs for current Size Multiple Plugin configurations
   *
   * @param attributesToCheck   Attribute IDs to check
   * @param divisions           Valid Divisions IDs
   * @param sizeIDs             Size IDs
   * @param isValidDivisions    Attribute ID division
   *
   * @return Filtered List of Size IDs
   *
   * @throws ExportStagingException
   */
  public List<String> filterSizeIDsByDivisions(
    String attributesToCheck[],
    String divisions[],
    List<String> sizeIDs,
    boolean isValidDivisions
  ) throws ExportStagingException
  {

    if(divisions.length != 0)
    {
      sizeIDs = filterProductsByDivisions(sizeIDs, divisions, isValidDivisions);

      if(!sizeIDs.isEmpty()) {
        sizeIDs = getSizeElementsByFilledAttributes(
          attributesToCheck,
          sizeIDs
        );
      }
    }

    return sizeIDs;
  }
}
