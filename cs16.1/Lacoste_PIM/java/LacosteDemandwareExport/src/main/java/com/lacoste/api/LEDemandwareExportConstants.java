package com.lacoste.api;

import java.io.File;


/**
 * Defines constants for Demandware Export process
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class LEDemandwareExportConstants
{
  public static String CONFIG_XML = "." + File.separator + "LacosteDemandwareExport.xml";
  public static String LOCK_FILE  = ".." + File.separator + ".." + File.separator + ".."
    + File.separator + "data" + File.separator + "cache"
    + File.separator + "locks" + File.separator + "LacosteDemandwareExport.lock";
  public static String ERROR_LOG  = ".." + File.separator + "logs" + File.separator + "demandware_export_error.log";
}
