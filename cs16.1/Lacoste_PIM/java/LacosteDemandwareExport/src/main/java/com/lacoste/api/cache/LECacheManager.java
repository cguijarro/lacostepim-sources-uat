package com.lacoste.api.cache;

import java.util.HashMap;

import org.ehcache.Cache;
import org.ehcache.CachePersistenceException;
import org.ehcache.PersistentCacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.ehcache.config.units.MemoryUnit;

import com.lacoste.api.LEDemandwareExportApi;


/**
 * This class provides APIs to store and retrieve data from Persistence cache
 *
 * @since CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class LECacheManager
{

  private static final String CACHE_LOCATION = "../../../data/cache/demandwareexportcache/productvalues";

  PersistentCacheManager persistentCacheManager;
  Cache<Long, HashMap<String, String>> cache;

  public LECacheManager()
  {
    HashMap<String, String> map = new HashMap<>();

    persistentCacheManager = CacheManagerBuilder.newCacheManagerBuilder().with(
      CacheManagerBuilder.persistence(LECacheManager.CACHE_LOCATION)).withCache(
        "persistence-cache", CacheConfigurationBuilder.newCacheConfigurationBuilder(
          Long.class,
          map.getClass(),
          ResourcePoolsBuilder.newResourcePoolsBuilder().heap(10, EntryUnit.ENTRIES).disk(10, MemoryUnit.GB, true)
        )
      ).build(true);

    cache = getCache();
    cache.clear();
  }


  /**
   * Gets the instance of Cache
   *
   * @return Instance of Cache.
   */
  public Cache<Long, HashMap<String, String>> getCache()
  {
    HashMap<String, String> map = new HashMap<>();

    return (Cache<Long, HashMap<String, String>>) persistentCacheManager.getCache(
      "persistence-cache",
      Long.class,
      map.getClass()
    );
  }


  /**
   * Gets the values from Cache by Product ID
   *
   * @param productID Cache Key Product ID
   *
   * @return Values from Cache
   */
  public HashMap<String, String> getDataFromCache(long productID)
  {
    return cache.get(productID);
  }

  public void putDataInCache(long productID, HashMap<String, String> data)
  {
    cache.put(productID, data);
  }


  /**
   * Closes the cache & cache Manager and destroys cache manager.
   */
  public void close()
  {
    cache.clear();
    persistentCacheManager.removeCache("persistence-cache");
    persistentCacheManager.close();

    try
    {
      persistentCacheManager.destroy();
    }
    catch(CachePersistenceException exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
    }
  }


  /**
   * Checks if product is present in Cache
   *
   * @param productID Cache Key
   *
   * @return TRUE if key is present in Cache else FALSE
   */
  public boolean isContainsKey(long productID)
  {
    return cache.containsKey(productID);
  }


  /**
   * Clears cache
   */
  public void clear()
  {
    cache.clear();
  }
}
