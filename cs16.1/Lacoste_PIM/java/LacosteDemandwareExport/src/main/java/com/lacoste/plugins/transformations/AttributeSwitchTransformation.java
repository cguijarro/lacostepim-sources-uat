package com.lacoste.plugins.transformations;


import com.lacoste.api.LEDemandwareExportXmlApi;
import com.lacoste.api.LETransformationPlugin;
import com.lacoste.api.TreeNode;


/**
 * Transformation plugin to get the value from other attribute when the value of first attribute is empty.
 * Two attribute-IDs for each XML node is configured.
 * If the value of the first attribute is not empty, export it.
 * If the value of the first attribute is empty, export the value of the second attribute.
 *
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class AttributeSwitchTransformation extends LETransformationPlugin
{


  @Override
  public int getPriority()
  {
    return 0;
  }


  @Override
  public String transform(TreeNode node, String nodeValue)
  {
    String value = "";

    if(this.getValue().equalsIgnoreCase("true"))
    {
      String xValue = node.getValue();
      if(xValue.contains("|")) {
        xValue = xValue.replaceAll(node.getLanguage(), "");
        String attributeIds[] = xValue.split(",");
        for(String attribute : attributeIds) {
          if(attribute.contains("."))
          {
            String switchingAttributes[] = attribute.split("\\.");
            String referenceAttribute = switchingAttributes[0];
            String textAttributes[] = switchingAttributes[1].split("\\|");
            for(String textAttribute : textAttributes)
            {
              String tempValue = referenceAttribute + "." + textAttribute + node.getLanguage();
              tempValue = LEDemandwareExportXmlApi.getValue(tempValue, node.getNodeName());
              if(tempValue != null && !tempValue.isEmpty())
              {
                return tempValue;
              }
            }
          }
          else
          {
            String tempValue = attribute + node.getLanguage();
            tempValue = LEDemandwareExportXmlApi.getValue(tempValue, node.getNodeName());
            if(tempValue != null && !tempValue.isEmpty())
            {
              return tempValue;
            }
          }
        }
      }
      else
      {
        String attributeIds[] = node.getValue().trim().split(",");
        value = "";

        for(int i = 0; i < attributeIds.length; i++)
        {
          value = LEDemandwareExportXmlApi.getValue(attributeIds[i], node.getNodeName());

          if(!value.isEmpty())
          {
            break;
          }
        }
      }
    }

    return value;
  }


  @Override
  public String transform(TreeNode node, String nodeName, String nodeValue)
  {
    String value = "";

    if(this.getValue().equalsIgnoreCase("true"))
    {
      String xValue = node.getAttributes().get(nodeName);
      if(xValue.contains("|")) {
        xValue = xValue.replaceAll(node.getLanguage(), "");
        String attributeIds[] = xValue.split(",");
        for(String attribute : attributeIds) {
          if(attribute.contains("."))
          {
            String switchingAttributes[] = attribute.split("\\.");
            String referenceAttribute = switchingAttributes[0];
            String textAttributes[] = switchingAttributes[1].split("\\|");
            for(String textAttribute : textAttributes)
            {
              String tempValue = referenceAttribute + "." + textAttribute + node.getLanguage();
              tempValue = LEDemandwareExportXmlApi.getValue(tempValue, nodeName, node.getLanguage());
              if(tempValue != null && !tempValue.isEmpty())
              {
                return tempValue;
              }
            }
          }
          else
          {
            String tempValue = attribute + node.getLanguage();
            tempValue = LEDemandwareExportXmlApi.getValue(tempValue, nodeName, node.getLanguage());
            if(tempValue != null && !tempValue.isEmpty())
            {
              return tempValue;
            }
          }
        }
      }
      else
      {
        String attributeIds[] = node.getAttributes().get(nodeName).trim().split(",");

        for(int i = 0; i < attributeIds.length; i++)
        {
          value = LEDemandwareExportXmlApi.getValue(attributeIds[i], nodeName, node.getLanguage());

          if(!value.isEmpty())
          {
            break;
          }
        }
      }
    }

    return value;
  }
}
