package com.lacoste.plugins.transformations;

import com.lacoste.api.LEDemandwareExportApi;
import com.lacoste.api.LEDemandwareExportXmlApi;
import com.lacoste.api.LETransformationPlugin;
import com.lacoste.api.TreeNode;

/**
 * Transformation plug-in to append the finishing code for valid Divisions.
 * User need to configure the 2 attributes at node level for this transformation.
 * For example, 
 * <test X-VALUE-TRANSFORM="ConditionalFinishingCode=Configurations.ProductIdSuffix" X-VALUE="101,189"/>
 * In above example "ConditionalFinishingCode" transformation is applied for X-VALUE.
 * So, for X-VALUE two attributes are configured i.e. (X-VALUE="101,189").
 * The attribute ID: 101 is a "Division" attribute in which valid division is attached. 
 * If attribute "Division" has valid division the append Finishing code.
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class ConditionalFinishingCodeTransformation extends LETransformationPlugin
{

  @Override
  public int getPriority()
  {
    return 0;
  }


  @Override
  public String transform(TreeNode node, String nodeValue)
  {
    String transformedValue = "";
    if(node.isTransformationFlag())
    {
      String xValue = node.getValue();

      if(xValue != null) {
        String attributeIds[] = xValue.trim().split(",");

        if(attributeIds.length == 2)
        {
          transformedValue = LEDemandwareExportXmlApi.getValue(attributeIds[1] + node.getLanguage(), node.getNodeName());
          String valueToBeChecked = LEDemandwareExportXmlApi.getValue(attributeIds[0], node.getNodeName());

          if(!valueToBeChecked.isEmpty() && !transformedValue.isEmpty() && isValidDivision(valueToBeChecked))
          {
            transformedValue = transformedValue.trim();
            transformedValue += LEDemandwareExportXmlApi.getValue(this.getValue(), "");
          }
        }
      }
    }

    return transformedValue;
  }


  @Override
  public String transform(TreeNode node, String nodeName, String nodeValue)
  {
    String transformedValue = "";
    if(node.isAttributeTransformationFlag())
    {
      if(!this.getValue().isEmpty())
      {
        String attributeValue = node.getAttributes().get(nodeName);
        if(attributeValue != null) {
          String attributeIds[] = attributeValue.trim().split(",");

          if(attributeIds.length == 2)
          {
            transformedValue = LEDemandwareExportXmlApi.getValue(attributeIds[1], nodeName, node.getLanguage());
            String valueToBeChecked = LEDemandwareExportXmlApi.getValue(attributeIds[0], nodeName, node.getLanguage());

            if(!valueToBeChecked.isEmpty() && !transformedValue.isEmpty() && isValidDivision(valueToBeChecked))
            {
              transformedValue = transformedValue.trim();
              transformedValue += LEDemandwareExportXmlApi.getValue(this.getValue(), nodeName, node.getLanguage());
            }
          }
        }
      }
    }

    return transformedValue;
  }


  /**
   * Check if division in valid or not
   * 
   * @param division ID of division PIM item
   * 
   * @return TRUE if valid division else false
   */
  private boolean isValidDivision(String division)
  {
    division = division.trim();
    if(LEDemandwareExportApi.getValidDivisions().contains(division))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}
