package com.lacoste.plugins.multiple;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.exportstaging.api.domain.Reference;
import com.exportstaging.api.wraper.ItemWrapper;
import com.lacoste.api.LEDemandwareExportApi;
import com.lacoste.api.LEDemandwareExportXmlApi;
import com.lacoste.api.LEMultiplePlugin;
import com.lacoste.api.LEDemandwareExportLabelComaparator;
import com.lacoste.api.TreeNode;

public class ProductVariantsMultiple extends LEMultiplePlugin
{

  private static String ATTRIBUTES_TO_CHECK = "AttributesToCheck";
  private static String CHECK_FOR_DIVISION  = "CheckForDivisions";

  @Override
  public List<String> getData(TreeNode node)
  {
    List<String> sizeIDs = new ArrayList<>();
    String idProduct = "";
    String colorItemID = "";
    String sizeItemID = "";
    try
    {
      String attributeID = LEDemandwareExportXmlApi.getValue("Configurations.AttributeProductID", "");
      String colorAttributeID = LEDemandwareExportXmlApi.getValue("Configurations.AttributeColor", "");
      String esbCodeAttributeID = LEDemandwareExportXmlApi.getValue("Configurations.AttributeEsbCode", "");
      idProduct = LEDemandwareExportXmlApi.getValue(attributeID, "");

      Map<String, Map<String, String>> map = new HashMap<>();

      if(LEDemandwareExportXmlApi.productSizeCache.containsKey(idProduct))
      {
    	
        sizeIDs = new ArrayList<>(LEDemandwareExportXmlApi.productSizeCache.get(idProduct));
        sizeIDs = filterVariants(sizeIDs);

        for(String sizeID : sizeIDs)
        {
          ItemWrapper itemWrapper = LEDemandwareExportApi.getProductByID(Long.parseLong(sizeID));
          String label = "";
          String colorEsbCode = "";
          sizeItemID = sizeID;
          if(itemWrapper != null)
          {
            label = itemWrapper.getFormattedValue("Label");
            List<Reference> colors = itemWrapper.getReferencesByID(Long.parseLong(colorAttributeID));

            if(colors != null)
            {
              for(Reference color : colors)
              {
                long itemID = color.getTargetID();
                colorItemID = Long.toString(itemID);
                colorEsbCode = LEDemandwareExportApi.getTargetByID(itemID).get(esbCodeAttributeID);
              }
            }
          }
          if(colorEsbCode != null)
          {  
	          if(map.containsKey(colorEsbCode.trim()))
	          {
	            Map<String, String> map2 = map.get(colorEsbCode.trim());
	            map2.put(sizeID, label);
	            map.put(colorEsbCode, map2);
	          }
	          else
	          {
	            Map<String, String> map2 = new HashMap<>();
	            map2.put(sizeID, label);
	            map.put(colorEsbCode, map2);
	          }
	          
	      	LEDemandwareExportApi.logError("Normal Log");
          	LEDemandwareExportApi.logError("Product : " + idProduct.toString());
          	LEDemandwareExportApi.logError("Color : " + colorItemID);
          	LEDemandwareExportApi.logError("Size : " + sizeItemID);
        	LEDemandwareExportApi.logError("Code ESB : " + colorEsbCode);
        	LEDemandwareExportApi.logError("Label : " + label);
          	
          } else {
        	  	LEDemandwareExportApi.logError("Code ESB Null");
	          	LEDemandwareExportApi.logError("Product : " + idProduct.toString());
	          	LEDemandwareExportApi.logError("Color : " + colorItemID);
	          	LEDemandwareExportApi.logError("Size : " + sizeItemID);
          }
        }
      }

      map = new TreeMap<>(map);
      sizeIDs = new ArrayList<>();

      for(String key : map.keySet())
      {
        Map<String, String> unsortMap = map.get(key);

        for(String sizeID : this.sortByValues(unsortMap).keySet())
        {
          sizeIDs.add(sizeID);
        }
      }
    }
    catch(NumberFormatException exception)
    {
      LEDemandwareExportApi.printStackTrace(exception);
      LEDemandwareExportApi.logError(exception.getMessage());
    }
    catch(Exception exception)
    {
    	LEDemandwareExportApi.logError("Global Exception");
    	LEDemandwareExportApi.logError("Product : " + idProduct.toString());
    	LEDemandwareExportApi.logError("Color : " + colorItemID);
    	LEDemandwareExportApi.logError("Size : " + sizeItemID);
    	
    }
 
    return sizeIDs;
  }


  /**
   * Apply filter on Size IDs based on Plugin Configurations
   *
   * @return List<String> List of filtered Size IDs
   */
  public List<String> filterVariants(List<String> variants)
  {
    String attributesToCheck[] = new String[0];
    String divisions[] = new String[0];

    if(getAttributesToCheck() == null || getAttributesToCheck().isEmpty())
    {
      return variants;
    }

    if(getDivisionsForCheck() != null && !getDivisionsForCheck().isEmpty())
    {
      divisions = getDivisionsForCheck().split(",");
    }

    attributesToCheck = getAttributesToCheck().split(",");

    try
    {
      variants = LEDemandwareExportApi.filterSizeIDsForCurrentConfigurations(
        attributesToCheck,
        divisions,
        variants,
        LEDemandwareExportApi.getAttributeIdDivisions()
      );
    }
    catch (Exception e)
    {
      variants = new ArrayList<>();
    }

    return variants;
  }


  /**
   * Gets the comma separated list of attributes.
   *
   * @return String Attribute IDs
   */
  private String getAttributesToCheck()
  {
    return this.getPluginConf().get(ATTRIBUTES_TO_CHECK);
  }

  /**
   * Gets the comma separated list of divisions.
   *
   * @return String Division IDs
   */
  private String getDivisionsForCheck()
  {
    return this.getPluginConf().get(CHECK_FOR_DIVISION);
  }


  /**
   * Sort Map Keys by Values
   * 
   * @param unsortMap Unsorted Map for sorting
   * 
   * @return Sorted Map by values
   */
  private Map<String, String> sortByValues(Map<String, String> unsortMap) {

    List<Map.Entry<String, String>> list =
            new LinkedList<Map.Entry<String, String>>(unsortMap.entrySet());

    Collections.sort(list, new LEDemandwareExportLabelComaparator<String>());

    Map<String, String> sortedMap = new LinkedHashMap<String, String>();
    for (Map.Entry<String, String> entry : list) {
        sortedMap.put(entry.getKey(), entry.getValue());
    }

    return sortedMap;
  }

}
