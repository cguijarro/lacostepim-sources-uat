package com.lacoste.plugins.transformations;

import java.util.HashMap;
import java.util.Map;

import com.lacoste.api.LETransformationPlugin;
import com.lacoste.api.TreeNode;


/**
 * Transformation plugin to separate the letters of care instructions by hyphen(-)
 * when their length is between 4 to 7.
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  DemandwareExport
 */
public class CareInstructionsTransformation extends LETransformationPlugin
{

  private static Map<String, String> transformedCareInstructions = new HashMap<>();

  @Override
  public int getPriority()
  {
    return 100;
  }


  @Override
  public String transform(TreeNode node, String nodeValue)
  {
    String value = nodeValue;

    if(this.getValue().equalsIgnoreCase("true"))
    {
      if(!transformedCareInstructions.containsKey(nodeValue))
      {
        if(value.length() >= 4 && value.length() <= 7)
        {
          value = String.join("-", value.split(""));
        }
        transformedCareInstructions.put(nodeValue, value);
      }
      else
      {
        value = transformedCareInstructions.get(nodeValue);
      }
    }

    return value;
  }


  @Override
  public String transform(TreeNode node, String nodeName, String nodeValue)
  {
    return nodeValue;
  }


  public static void clearCache()
  {
    transformedCareInstructions.clear();
  }
}
