package com.lacoste.api;

/**
 * Defines the constants for property name
 * Properties started with "O_" can be empty
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  PublicationList
 */
public class LEPublicationListConstants
{

  /**
   * CassandraDB connection properties
   */
  public static final String CASSANDRA_ADDRESS   = "CASSANDRA_ADDRESS";
  public static final String CASSANDRA_KEYSPACE  = "CASSANDRA_KEYSPACE";

  /**
   * Import database properties
   */
  public static final String IMPORT_DATABASE_HOST                 = "IMPORT_DATABASE_HOST";
  public static final String IMPORT_DATABASE_PORT                 = "IMPORT_DATABASE_PORT";
  public static final String IMPORT_DATABASE_NAME                 = "IMPORT_DATABASE_NAME";
  public static final String IMPORT_DATABASE_USER                 = "IMPORT_DATABASE_USER";
  public static final String O_IMPORT_DATABASE_PASSWORD           = "O_IMPORT_DATABASE_PASSWORD";
  public static final String IMPORT_DATABASE_RESTSERVICE_URL      = "IMPORT_DATABASE_RESTSERVICE_URL";
  public static final String CONTENTSERV_RESTSERVICE_USER         = "CONTENTSERV_RESTSERVICE_USER";
  public static final String CONTENTSERV_RESTSERVICE_PASSWORD     = "CONTENTSERV_RESTSERVICE_PASSWORD";
  public static final String IMPORT_DATABASE_JOB_ID               = "IMPORT_DATABASE_JOB_ID";
  public static final String IMPORT_DATABASE_REFERENCE_EXTERNALKEY_DEFAULT_FORMAT = "IMPORT_DATABASE_REFERENCE_EXTERNALKEY_DEFAULT_FORMAT";
  public static final String IMPORT_DATABASE_REFERENCE_EXTERNALKEY_GENERAL_FORMAT = "IMPORT_DATABASE_REFERENCE_EXTERNALKEY_GENERAL_FORMAT";

  public static final String PRODUCT_HISTORY_RESTSERVICE_URL      = "PRODUCT_HISTORY_RESTSERVICE_URL";

  /**
   * Last run start time stamp property
   */
  public static final String O_LAST_RUN_TIMESTAMP  = "O_LAST_RUN_TIMESTAMP";

  /**
   * Properties for Attribute IDs
   */
  public static final String LE_ATTR_PERIOD_ACTIVE_DWARE_EU  = "LE_ATTR_PERIOD_ACTIVE_DWARE_EU";
  public static final String LE_ATTR_PERIOD_ACTIVE_DWARE_NA  = "LE_ATTR_PERIOD_ACTIVE_DWARE_NA";
  public static final String LE_ATTR_PERIOD_ACTIVE_DWARE_LA  = "LE_ATTR_PERIOD_ACTIVE_DWARE_LA";
  public static final String LE_ATTR_PERIOD_ACTIVE_MPLACE_EU = "LE_ATTR_PERIOD_ACTIVE_MPLACE_EU";
  public static final String LE_ATTR_PERIOD_ACTIVE_MPLACE_NA = "LE_ATTR_PERIOD_ACTIVE_MPLACE_NA";
  public static final String LE_ATTR_PERIOD_SCHED_DWARE_EU   = "LE_ATTR_PERIOD_SCHED_DWARE_EU";
  public static final String LE_ATTR_PERIOD_SCHED_DWARE_NA   = "LE_ATTR_PERIOD_SCHED_DWARE_NA";
  public static final String LE_ATTR_PERIOD_SCHED_DWARE_LA   = "LE_ATTR_PERIOD_SCHED_DWARE_LA";
  public static final String LE_ATTR_PERIOD_SCHED_MPLACE_EU  = "LE_ATTR_PERIOD_SCHED_MPLACE_EU";
  public static final String LE_ATTR_PERIOD_SCHED_MPLACE_NA  = "LE_ATTR_PERIOD_SCHED_MPLACE_NA";
  public static final String LE_ATTR_PERIOD_ARCHIVED         = "LE_ATTR_PERIOD_ARCHIVED";
  public static final String LE_ATTR_ACTIVE                  = "LE_ATTR_ACTIVE";
  public static final String LE_ATTR_FALSE_IMPORT            = "LE_ATTR_FALSE_IMPORT";
  public static final String LE_ATTR_BEGIN_DATE              = "LE_ATTR_BEGIN_DATE";
  public static final String LE_ATTR_END_DATE                = "LE_ATTR_END_DATE";
  public static final String LE_ATTR_ITEMTYPE                = "LE_ATTR_ITEMTYPE";
  public static final String LE_ATTR_CHANNEL                 = "LE_ATTR_CHANNEL";
  public static final String LE_ATTR_CHANNEL_ACTIVE_ATTR     = "LE_ATTR_CHANNEL_ACTIVE_ATTR";
  public static final String LE_ATTR_SEASONAL_DATA           = "LE_ATTR_SEASONAL_DATA";

  /**
   * Item types Product IDs
   */
  public static final String ITEMTYPE_SIZE                   = "ITEMTYPE_SIZE";
  public static final String ITEMTYPE_COLOR                  = "ITEMTYPE_COLOR";

  /**
   * Elasticsearch properties
   */
  public static final String ELASTICSEARCH_CLUSTER  = "ELASTICSEARCH_CLUSTER";
  public static final String ELASTICSEARCH_ADDRESS  = "ELASTICSEARCH_ADDRESS";
  public static final String ELASTICSEARCH_TCP_PORT = "ELASTICSEARCH_TCP_PORT";
  public static final String ELASTICSEARCH_IDX_NAME = "ELASTICSEARCH_IDX_NAME";
}
