package com.lacoste.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.exportstaging.api.ExternalItemAPI;
import com.exportstaging.api.domain.Attribute;
import com.exportstaging.api.exception.ExportStagingException;
import com.exportstaging.api.wraper.ItemWrapper;
import com.exportstaging.utils.ExportCassandraConfigurator;
import com.importstaging.api.imports.ExternalItemAPIs;
import com.importstaging.api.imports.ItemAPIs;

import com.importstaging.api.connection.ImportDBConnection;
import com.importstaging.api.domain.Item;
import com.importstaging.api.exception.ImportStagingException;

/**
 * This class defines various APIs for publication list export
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  PublicationList
 */
public class LEPublicationListApi
{


  /**
   * Default values
   */
  public static final String LANGUAGE_CODE       = "en_en";
  public static final String LANGUAGE_ID         = "1";
  public static final String LACOSTE_PROPERTIES  = "config.properties";
  public static final String ERROR_LOG           = "publication_list_error.log";


  /**
   * Export database APIs, import database APIs & Configuration properties
   */
  private static ExternalItemAPI externalItemEsaAPI        = null;
  private static ImportDBConnection importDbConnection     = null;
  private static ExternalItemAPIs externalItemIsaAPI       = null;
  private static HashMap<String, String> lacosteProperties = null;
  private static LEElasticsearchApi elasticsearchApi       = null;


  /**
   * Cache
   */
  private static HashMap<String, String> productIdExternalKeyMap   = new HashMap<>();
  private static HashMap<String, String> attributeIdExternalKeyMap = new HashMap<>();
  private static List<String> dectivatedPeriodsIds                 = new ArrayList<>();


  /**
   * Log file name
   */
  private static String logFileName = null;



  /**
   * Gets the Cassandra Export configurator
   * 
   * @throws LEPublicationListException When Cassandra key space & address is not configured 
   */
  public static void getCassandraExportConfigurator() throws LEPublicationListException
  {
    String cassandrAddress  = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.CASSANDRA_ADDRESS);
    String cassandrKeyspace = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.CASSANDRA_KEYSPACE);

    new ExportCassandraConfigurator(cassandrAddress, cassandrKeyspace);
  }


  /**
   * Gets the instance of Export database External Item API
   * 
   * @return External Item API
   * 
   * @throws LEPublicationListException 
   */
  public static ExternalItemAPI getExternalItemEsaAPI() throws LEPublicationListException
  {
    try
    {
      if(externalItemEsaAPI == null)
      {
        getCassandraExportConfigurator();
        externalItemEsaAPI = new ExternalItemAPI(ExternalItemAPI.ITEM_TYPE_PRODUCT);
      }
    }
    catch(ExportStagingException e)
    {
      throw new LEPublicationListException(
        LEPublicationListException.LE_EXCEPTION_CASSANDRA_NOT_RUNNING,
        e.getMessage()
      );
    }

    return externalItemEsaAPI;
  }


  /**
   * Gets the instance of External Item API
   * 
   * @return External Item ISA API
   * 
   * @throws LEPublicationListException 
   */
  public static ExternalItemAPIs getExternalItemIsaAPI() throws LEPublicationListException
  {
    try
    {
      getImportDbConnection();

      if(externalItemIsaAPI == null)
      {
        externalItemIsaAPI = new ExternalItemAPIs(ItemAPIs.IMPORT_PDMARTICLE, importDbConnection);
      }
    }
    catch (ImportStagingException e)
    {
      LEPublicationListApi.printStackTrace(e);
      throw new LEPublicationListException(LEPublicationListException.LE_EXCEPTION_ISA_API_CREATION_FAILED);
    }

    return externalItemIsaAPI;
  }


  /**
   * Force Garbage collector
   */
  public static void checkMemory()
  {
    System.gc();
  }


  /**
   * Error log in file
   * 
   * @param errorMessage Exception message
   * 
   * @throws IOException 
   */
  public static void logError(String errorMessage)
  {
    String errorLogFileName = "." + File.separator + "logs" + File.separator + LEPublicationListApi.ERROR_LOG;
    String logMessage = "[" + getDate("") + "] " + errorMessage + "\n";
    try
    {
      Files.write(
        Paths.get(errorLogFileName),
        logMessage.getBytes(),
        StandardOpenOption.APPEND,
        StandardOpenOption.CREATE
      );
    }
    catch (IOException e) {
      System.out.println("PublicationListApi.logError" + e.getMessage());
    }
  }


  /**
   * Gets the current date in given format
   * 
   * @param format Format or empty string for format "yyyy-MM-dd HH:mm:ss"
   * 
   * @return Formatted date
   */
  public static String getDate(String format)
  {
    Date date = new Date();

    format = format.isEmpty() ? "yyyy-MM-dd HH:mm:ss" : format;
    String formattedDate = new SimpleDateFormat(format).format(date);

    return formattedDate;
  }


  /**
   * Gets the formatted date
   * 
   * @param date   Date object or null for current date
   * @param format Format or empty string for format "yyyy-MM-dd HH:mm:ss"
   * 
   * @return Formatted date
   */
  public static String getDate(Date date, String format)
  {
    if(date == null)
    {
      date = new Date();
    }

    format = format.isEmpty() ? "yyyy-MM-dd HH:mm:ss" : format;
    String formattedDate = new SimpleDateFormat(format).format(date);

    return formattedDate;
  }


  /**
   * Gets Unix time stamp for the given date
   * 
   * @param date Date object or null for current date
   * 
   * @return String Unix time stamp
   */
  public static String getUnixTimestamp(Date date)
  {
    if(date == null) {
      date = new Date();
    }
    String unixTimestamp = date.getTime() + "";

    return unixTimestamp;
  }


  /**
   * Gets the Import DB connection object
   * 
   * @return ImportDBConnection
   * 
   * @throws LEPublicationListException On failure
   */
  public static ImportDBConnection getImportDbConnection() throws LEPublicationListException
  {
    if(importDbConnection == null)
    {
      importDbConnection = ImportDBConnection.getImportDBConnection(
        LEPublicationListApi.getPropertyValue(LEPublicationListConstants.IMPORT_DATABASE_HOST),
        LEPublicationListApi.getPropertyValue(LEPublicationListConstants.IMPORT_DATABASE_PORT),
        LEPublicationListApi.getPropertyValue(LEPublicationListConstants.IMPORT_DATABASE_NAME),
        LEPublicationListApi.getPropertyValue(LEPublicationListConstants.IMPORT_DATABASE_USER),
        base64Decode(LEPublicationListApi.getPropertyValue(LEPublicationListConstants.O_IMPORT_DATABASE_PASSWORD))
      );
    }

    return importDbConnection;
  }


  /**
   * Stores Item in Import database
   * 
   * @param item Import Item to store in import database
   * 
   * @return TRUE if item stored successfully else FALSE
   * 
   * @throws LEPublicationListException On failure
   */
  public static boolean storeItem(Item item) throws LEPublicationListException
  {
    boolean isStored = true;
    try
    {
      String result = getExternalItemIsaAPI().storeItem(item);
      log("Item for External Key '" + result + "' inserted in IMPORT database");
    }
    catch (ImportStagingException e)
    {
      isStored = false;
      LEPublicationListApi.printStackTrace(e);
      throw new LEPublicationListException(LEPublicationListException.LE_EXCEPTION_ISA_API_CREATION_FAILED, e.getMessage());
    }

    return isStored;
  }


  /**
   * Gets the properties from config.properties file and store in to Map
   * 
   * @throws LEPublicationListException On Invalid configuration
   */
  private static void getProperties() throws LEPublicationListException
  {
    Properties properties   = new Properties();
    InputStream inputStream = null;

    try
    {
      inputStream = new FileInputStream(LEPublicationListApi.LACOSTE_PROPERTIES);
      properties.load(inputStream);
      lacosteProperties = new HashMap<>();

      Class<?> controllerClass = Class.forName("com.lacoste.api.LEPublicationListConstants");

      Field[] fields = controllerClass.getDeclaredFields();

      for ( Field field : fields )
      {
        String propertyName  = field.getName();
        String propertyValue = properties.getProperty(propertyName);
        if(!properties.containsKey(propertyName) || (isKeyMust(propertyName) && isEmpty(propertyValue)))
        {
          throw new LEPublicationListException(
            LEPublicationListException.LE_EXCEPTION_INVALID_CONFIG,
            "Property '" + propertyName + "' / value missing in config.properties"
          );
        }
        else
        {
          lacosteProperties.put(propertyName, propertyValue);
        }
      }

      for(Object key : properties.keySet())
      {
        if(key.toString().matches("^CUSTOMIZEDTEXT_([0-9]+)$"))
        {
          lacosteProperties.put(key.toString(), properties.getProperty(key.toString()));
        }
      }
    }
    catch (IOException | ClassNotFoundException ex)
    {
      throw new LEPublicationListException(
        LEPublicationListException.LE_EXCEPTION_CONFIG_FILE_MISSING,
        ex.getMessage()
      );
    }
    finally
    {
      if (inputStream != null) {
        try {
          inputStream.close();
        } catch (IOException e) {
          LEPublicationListApi.printStackTrace(e);
        }
      }
    }
  }


  /**
   * Check if Property value is mandatory or not
   * 
   * @param propertyName
   * 
   * @return true if Property value should not empty
   */
  private static boolean isKeyMust(String propertyName)
  {
    if(!propertyName.startsWith("O_"))
    {
      return true;
    }

    return false;
  }


  /**
   * Checks if a given string is empty or null
   * 
   * @param data String to be tested
   * 
   * @return true if empty or null else false
   */
  public static boolean isEmpty(String data)
  {
    if(data == null || data.trim().isEmpty())
    {
      return true;
    }
    else
    {
      return false;
    }
  }


  /**
   * Gets the Value of specified configuration property
   * 
   * @param properyName Name of the property
   *  
   * @return Value of property from configuration file
   *  
   * @throws LEPublicationListException On invalid configuration
   */
  public static String getPropertyValue(String properyName) throws LEPublicationListException
  {
    String propertyValue = "";
    if(lacosteProperties == null)
    {
      getProperties();
    }

    if(lacosteProperties.containsKey(properyName))
    {
      propertyValue = lacosteProperties.get(properyName);
    }

    return propertyValue;
  }


  /**
   * Gets the External Key from Cassandra Database
   * 
   * @param attributeID Attribute ID
   * 
   * @return External key of the attribute
   * 
   * @throws LEPublicationListException On invalid configuration or No external key for attribute
   */
  public static String getAttributeExternalKeyById(String attributeID) throws LEPublicationListException
  {
    String attributeExKey   = "";
    
    if(!attributeIdExternalKeyMap.containsKey(attributeID))
    {
      ExternalItemAPI itemAPI = LEPublicationListApi.getExternalItemEsaAPI();

      int attrID = Integer.parseInt(attributeID);

      try
      {
        Attribute attribute = itemAPI.getAttributeByID(attrID);
        if(attribute != null)
        {
          attributeExKey      = attribute.getExternalKey();
          attributeIdExternalKeyMap.put(attributeID, attributeExKey);
        }
        else
        {
          throw new LEPublicationListException(0, "Attribute ID " + attrID + " not present in Export database!");
        }
      }
      catch (ExportStagingException e)
      {
        throw new LEPublicationListException(LEPublicationListException.LE_EXCEPTION_INVALID_CONFIG, "Get ExternalKey from Export database failed for Attribute ID: " + attributeID);
      }

      if(attributeExKey.trim().isEmpty())
      {
        throw new LEPublicationListException(LEPublicationListException.LE_EXCEPTION_INVALID_CONFIG, "No ExternalKey for Attribute ID: " + attributeID);
      }
    }
    else
    {
      attributeExKey = attributeIdExternalKeyMap.get(attributeID);
    }

    return attributeExKey;
  }


  /**
   * Decode the base 64 encoded string
   * 
   * @param base64EncodedString Base64 encoded string
   * 
   * @return Base64 decoded value of input string
   */
  public static String base64Decode(String base64EncodedString)
  {
    return new String(Base64.getDecoder().decode(base64EncodedString));
  }


  /**
   * Gets the process log file name
   * 
   * @return Log file name
   * @throws IOException 
   */
  private static String getLogFileName() throws IOException
  {
    String logFileName = ".." + File.separator 
        + ".." + File.separator
        + "data" + File.separator
        + "logs" + File.separator
        + "processes" + File.separator
        + "PublicationList" + File.separator
        + getDate("yyyy") + File.separator
        + getDate("MM") + File.separator
        + getDate("yyyy-MM-dd") + ".log";

    File file = new File(logFileName);
    file.getParentFile().mkdirs();

    return logFileName;
  }


  /**
   * Gets the elastic search API
   * 
   * @return instance of LEElasticsearchApi class
   * 
   * @throws LEPublicationListException On failure
   */
  public static LEElasticsearchApi getElasticsearchApi() throws LEPublicationListException
  {
    if(elasticsearchApi == null)
    {
      elasticsearchApi = new LEElasticsearchApi();
      try{
        elasticsearchApi.setConnection(
          LEPublicationListApi.getPropertyValue(LEPublicationListConstants.ELASTICSEARCH_CLUSTER),
          LEPublicationListApi.getPropertyValue(LEPublicationListConstants.ELASTICSEARCH_ADDRESS),
          Integer.parseInt(LEPublicationListApi.getPropertyValue(LEPublicationListConstants.ELASTICSEARCH_TCP_PORT)),
          LEPublicationListApi.getPropertyValue(LEPublicationListConstants.ELASTICSEARCH_IDX_NAME)
        );
      }
      catch(Exception e)
      {
        throw new LEPublicationListException(LEPublicationListException.LE_EXCEPTION_ELASTICSEARCH_NOT_RUNNING, " " + e.getMessage());
      }
    }

    return elasticsearchApi;
  }


  /**
   * Gets the product's external key by ID from Cassandra database
   * 
   * @param productID Product ID
   * 
   * @return External key given product ID
   * 
   * @throws LEPublicationListException on failure
   */
  public static String getProductExternalKeyById(String productID) throws LEPublicationListException
  {
    String externalKey = "";

    if(!productIdExternalKeyMap.containsKey(productID))
    {
      try
      {
        ExternalItemAPI itemAPI = LEPublicationListApi.getExternalItemEsaAPI();
        ItemWrapper itemWrapper = itemAPI.getItemById(Long.parseLong(productID));

        if(!LEPublicationListApi.isNull(itemWrapper))
        {
          externalKey = itemWrapper.getExternalKey();
          if(!externalKey.isEmpty())
          {
            productIdExternalKeyMap.put(productID, externalKey);
          }
          else
          {
            log("PIM item having ID " + productID + " do not have external key");
          }
        }
      }
      catch(Exception e)
      {
        throw new LEPublicationListException(
          0,
          "Get external key for product ID '" + productID + "' failed due to " + e.getMessage()
        );
      }
    }
    else
    {
      externalKey = productIdExternalKeyMap.get(productID);
    }

    return externalKey;
  }


  /**
   * Checks if object is null
   * 
   * @param object
   * 
   * @return true if object is null else false
   */
  public static boolean isNull(Object object)
  {
    if(object == null)
    {
      return true;
    }
    else
    {
      return false;
    }
  }


  /**
   * Stores the message in Log file
   * 
   * @param logMessage Message
   */
  public static void log(String logMessage)
  {
    try
    {
      if(logFileName == null)
      {
        logFileName = getLogFileName();
      }

      logMessage = "[" + getDate("") + "] " + logMessage + "\n";

      Files.write(
        Paths.get(logFileName),
        logMessage.getBytes(),
        StandardOpenOption.APPEND,
        StandardOpenOption.CREATE
      );
    }
    catch (IOException e) {
      LEPublicationListApi.printStackTrace(e);
    }
  }


  /**
   * Sets the Last run time value in config.properties
   * 
   * @param properyName  Key 
   * @param properyValue Value
   * 
   * @throws LEPublicationListException When config.properties file in not exists
   */
  public static void setPropertyValue(String properyName, String properyValue) throws LEPublicationListException
  {
    Properties properties = new Properties();

    try
    {
      properties.load(new FileInputStream(LEPublicationListApi.LACOSTE_PROPERTIES));
      properties.setProperty(properyName, properyValue);
      properties.store(new FileOutputStream(LEPublicationListApi.LACOSTE_PROPERTIES), "");
    }
    catch (IOException ex)
    {
      LEPublicationListApi.printStackTrace(ex);
      throw new LEPublicationListException(LEPublicationListException.LE_EXCEPTION_CONFIG_FILE_MISSING);
    }
  }


  /**
   * Writes stack trace to file
   * 
   * @param ex Exception
   */
  public static void printStackTrace(Exception ex)
  {
    StringWriter errors = new StringWriter();
    ex.printStackTrace(new PrintWriter(errors));
    logError(errors.toString());
  }


  /**
   * Checks if process is running or not
   * 
   * @return false if process is not running
   * 
   * @throws LEPublicationListException If process is running
   */
  public static boolean isProcessRunning() throws LEPublicationListException
  {
    boolean processRunning = false;
    String lockFileName = getLockFileName();

    try
    {
      File file = new File(lockFileName);
  
      if(!file.exists())
      {
        file.getParentFile().mkdirs();
        String lockMessage = "";
        Files.write(
          Paths.get(lockFileName),
          lockMessage.getBytes(),
          StandardOpenOption.APPEND,
          StandardOpenOption.CREATE
        );
      }
      else
      {
        processRunning = true;
        System.out.println("Publication list Process is already running!");
        throw new LEPublicationListException(LEPublicationListException.LE_EXCEPTION_ALREADY_RUNNING);
      }
    }
    catch(IOException e)
    {
      System.out.println("Do not have permission to create/delete file at path " + getLockFileName());
      throw new LEPublicationListException(LEPublicationListException.LE_EXCEPTION_FILESYSTEM_ERROR);
    }

    return processRunning;
  }


  /**
   * Gets the lock file name
   * 
   * @return Lock file name
   */
  public static String getLockFileName()
  {
    String lockFileName = ".." + File.separator 
        + ".." + File.separator
        + "data" + File.separator
        + "cache" + File.separator
        + "locks" + File.separator
        + "PublicationList.lock";

    return lockFileName;
  }


  /**
   * Delete the lock file
   * 
   * @return true on delete else false
   */
  public static boolean deleteLockFile()
  {
    String lockFileName = getLockFileName();
    File file = new File(lockFileName);

    return file.delete();
  }


  /**
   * Sets the List of periods that will be deactivated in current run to used in Active case
   * 
   * @param dectivatedPeriodsIds
   */
  public static void setDeactivatedPeriods(List<String> dectivatedPeriodsIds)
  {
    LEPublicationListApi.dectivatedPeriodsIds = dectivatedPeriodsIds;
  }


  /**
   * Checks if a Period ID is present in Deactivated Period cache
   * 
   * @param periodID
   * 
   * @return TRUE if Period ID is present in Deactivate cache else FALSE
   */
  public static boolean isPeriodDeactivated(String periodID)
  {
    if(dectivatedPeriodsIds.contains(periodID))
    {
      return true;
    }
    else
    {
      return false;
    }
  }


  /**
   * Gets the changed data for given Period since last run start time
   * 
   * @param periodID Id of a Period PIM item.
   * 
   * @return Changed data for the Period - Map: AttributeID => (NewValue, OldValue), ...
   */
  public static Map<String, Map<String, Set<String>>> getChangedData(String periodID)
  {
    Map<String, Map<String, Set<String>>> changedData = new HashMap<>(); 
    try 
    {
      String activeAttributeID   = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_ACTIVE);
      String channelAttributeID  = getPropertyValue(LEPublicationListConstants.LE_ATTR_CHANNEL);

      String jsonResponce        = LERestClient.getProductHistoryByID(periodID);
      JSONParser parser          = new JSONParser();
      JSONObject history         = (JSONObject) parser.parse(jsonResponce);
      JSONObject versions        = (JSONObject) history.get("Versions");
      Set<String> versionNumbers = versions.keySet();
      int versionNr[]            = new int[versionNumbers.size()];
      Iterator<String> iterator  = versionNumbers.iterator();
      int vi                     = 0;

      while(iterator.hasNext())
      {
        versionNr[vi++] = Integer.parseInt(iterator.next());
      }

      Arrays.sort(versionNr);

      Set<String> channelsNoChange = new HashSet<>();
      Set<String> channelsAdded    = new HashSet<>();
      Set<String> channelsRemoved  = new HashSet<>();

      boolean isFirstChange = true;
      Set<String> oldRef = new HashSet<>();
      Set<String> newRef = new HashSet<>();

      for(int versionNumber : versionNr)
      {
        JSONObject version = (JSONObject) versions.get(versionNumber+"");
        String versionTime = version.get("Time").toString();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String lastRunStartTime = getPropertyValue(LEPublicationListConstants.O_LAST_RUN_TIMESTAMP);

        if(lastRunStartTime.isEmpty())
        {
          lastRunStartTime = getDate(new Date(0), "");
        }

        if(dateFormat.parse(versionTime).after(dateFormat.parse(lastRunStartTime)))
        {
          JSONArray changes  = (JSONArray) version.get("Changes");

          if(changes == null)
          {
            LEPublicationListApi.logError("Changes for Period " + periodID + " is empty for version " + versionNumber);
            continue;
          }

          for(int i = 0; i < changes.size(); i++)
          {
            JSONObject change = (JSONObject) changes.get(i);
            if(change == null || change.get("AttributeID") == null || change.get("AttributeID").equals("LastChange"))
            {
              continue;
            }

            String attributeID = change.get("AttributeID").toString();
            String newValue =  "";

            if(attributeID.equals(activeAttributeID))
            {
              newValue = change.get("NewValue").toString();
              if(newValue.equals("1"))
              {
                changedData.put(activeAttributeID, null);
              }
            }
            else if(attributeID.equals(channelAttributeID))
            {

              newRef = new HashSet<>();
              if(!isEmpty(change.get("NewValue").toString())) {
                newRef = new HashSet<>();
                JSONObject reference = (JSONObject) parser.parse(change.get("NewValue").toString());
                JSONArray selected = (JSONArray)reference.get("Selected");
                for(int r = 0; r<selected.size(); r++)
                {
                  JSONObject selectedItems = (JSONObject) selected.get(r);
                  newRef.add(selectedItems.get("RecordID").toString());
                }
              }
              if(change.get("OldValue") != null && isFirstChange) {
                JSONObject oreference = (JSONObject) parser.parse(change.get("OldValue").toString());
                JSONArray oselected = (JSONArray)oreference.get("Selected");
                for(int rx = 0; rx<oselected.size(); rx++) {
                  if(oselected.get(rx)!=null) {
                    JSONObject oselectedItems = (JSONObject) oselected.get(rx);
                    oldRef.add(oselectedItems.get("RecordID").toString());
                  }
                }
              }
              isFirstChange = false;
            }
            else
            {
              changedData.put(attributeID, null);
            }
          }
        }
      }

      Set<String> union = new HashSet<String>(newRef);
      union.addAll(oldRef);

      Set<String> added = new HashSet<String>(union);
      added.removeAll(oldRef);

      Set<String> deleted = new HashSet<String>(union);
      deleted.removeAll(newRef);

      Set<String> noChange = new HashSet<String>(union);
      noChange.removeAll(added);
      noChange.removeAll(deleted);

      channelsAdded.addAll(added);
      channelsRemoved.addAll(deleted);

      if(!channelsAdded.isEmpty() || !channelsNoChange.isEmpty() || !channelsRemoved.isEmpty())
      {
        HashMap<String, Set<String>> changeInChannel = new HashMap<>();
        changeInChannel.put("NoChange", channelsNoChange);
        changeInChannel.put("Added", channelsAdded);
        changeInChannel.put("Removed", channelsRemoved);
        changedData.put(channelAttributeID, changeInChannel);
      }
    }
    catch(LEPublicationListException | java.text.ParseException | ParseException  e )
    {
      LEPublicationListApi.printStackTrace(e);
      LEPublicationListApi.log("Get Product History Webservice failed");
    }

    return changedData;
  }


  /**
   * Set the Property value in config.properties file
   *
   * @param propertyName  Key
   * @param propertyValue Value
   *
   * @throws LEPublicationListException When config.properties file in not exists
   */
  public static void setProperty(String propertyName, String propertyValue) throws LEPublicationListException
  {
    try
    {
      BufferedReader file = new BufferedReader(new FileReader(LEPublicationListApi.LACOSTE_PROPERTIES));
      String line;
      StringBuffer inputBuffer = new StringBuffer();

      while ((line = file.readLine()) != null)
      {

        if(line.startsWith(propertyName))
        {
          line = propertyName + "=" + propertyValue;
        }

        inputBuffer.append(line);
        inputBuffer.append('\n');
      }
      String inputStr = inputBuffer.toString();

      file.close();

      FileOutputStream fileOut = new FileOutputStream(LEPublicationListApi.LACOSTE_PROPERTIES);
      fileOut.write(inputStr.getBytes());
      fileOut.close();

    }
    catch (Exception exception)
    {
      LEPublicationListApi.printStackTrace(exception);
      throw new LEPublicationListException(LEPublicationListException.LE_EXCEPTION_CONFIG_FILE_MISSING);
    }
  }


  /**
   * Gets the overlapped Periods
   *
   * @param endDateAttributeIDs End date attribute IDs
   * @param overlappingPeriods  Overlapping periods IDs
   *
   * @return List of Overlapped periods IDs
   *
   * @throws Exception On Failure to read property
   */
  public static List<String> getOverlappedPeriods() throws Exception
  {
    String endDateAttr       = getPropertyValue(LEPublicationListConstants.LE_ATTR_END_DATE);
    String activeAttributeID = getPropertyValue(LEPublicationListConstants.LE_ATTR_ACTIVE);

    ArrayList<String> endDateAttributeIDs = new ArrayList<String>();
    endDateAttributeIDs.addAll(Arrays.asList(endDateAttr.split(",")));

    String[] sources = ("LastChange," + endDateAttr).split(",");

    for(int index = 0; index < sources.length; index++)
    {
      if(!sources[index].equals("LastChange"))
      {
        sources[index] = sources[index] + ":Value";
      }
    }

    List<String> overlappingPeriods  = getElasticsearchApi().getOverlappingPeriods(
      endDateAttributeIDs,
      activeAttributeID
    );

    Date currentDate = getFormattedDate(LEPublicationListApi.getDate("yyyy-MM-dd"), "yyyy-MM-dd");
    List<String> overlappedPeriods = new ArrayList<>();

    for(String periodID : overlappingPeriods)
    {
      Map<String, String> periodData = getElasticsearchApi().getProductByID(periodID, sources, 1);
      String lastChange = periodData.get("LastChange");

      Date lastChangeDate = removeTimeFromDate(lastChange);

      for(String endDateAttributeID : endDateAttributeIDs)
      {
        String endDateStr = periodData.get(endDateAttributeID + ":Value");

        if(endDateStr != null && !endDateStr.isEmpty())
        {
          Date endDate = getFormattedDate(endDateStr, "yyyy-MM-dd");

          if(currentDate.after(endDate) && (endDate.equals(lastChangeDate) || endDate.after(lastChangeDate)))
          {
            overlappedPeriods.add(periodID);
            break;
          }
        }
      }
    }

    return overlappedPeriods;
  }


  /**
   * Converts date string into the given format 
   *
   * @param dateString Date string
   * @param format     Date format
   *
   * @return Date object for given string
   *
   * @throws Exception If date format is invalid for give date string
   */
  public static Date getFormattedDate(String dateString, String format) throws Exception
  { 
    Date date = new SimpleDateFormat(format).parse(dateString);

    return date;
  }


  /**
   * Removes time from Date String.
   *
   * @param dateString Date string must be in format yyyy-MM-dd HH:mm:ss
   *
   * @return Date object after removing the time.
   *
   * @throws Exception If input date format is not as per the requirement
   */
  public static Date removeTimeFromDate(String dateString) throws Exception
  {
    Date date = getFormattedDate(dateString, "yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    dateString = dateFormat.format(date);

    return getFormattedDate(dateString, "yyyy-MM-dd");
  }
}
