package com.lacoste.api;

/**
 * Defines error codes & error messages
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  PublicationList
 */
public class LEPublicationListException
  extends Exception
{
  private static final long serialVersionUID = 1L;

  /**
   * Defines Exception code
   */
  public static final int LE_EXCEPTION_CASSANDRA_NOT_RUNNING      = 1;
  public static final int LE_EXCEPTION_INVALID_CONFIG             = 2;
  public static final int LE_EXCEPTION_INVALID_LAST_RUN_DATE      = 3;
  public static final int LE_EXCEPTION_EXCECUTE_IMPORT_JOB_FAILED = 4;
  public static final int LE_EXCEPTION_ISA_API_CREATION_FAILED    = 5;
  public static final int LE_EXCEPTION_ELASTICSEARCH_NOT_RUNNING  = 6;
  public static final int LE_EXCEPTION_CONFIG_FILE_MISSING        = 7;
  public static final int LE_EXCEPTION_HISTORY_REST_SERVICE       = 8;
  public static final int LE_EXCEPTION_ALREADY_RUNNING            = 9;
  public static final int LE_EXCEPTION_FILESYSTEM_ERROR           = 10;

  /**
   * Constructor to set the error code
   * 
   * @param exceptionCode Error code from defined constants
   */
  public LEPublicationListException(int exceptionCode)
  {
    super(getMessageForCode(exceptionCode));
  }


  /**
   * Constructor to set the Exception message with additional error information
   * 
   * @param exceptionCode  Error code from defined constants
   * @param additionalInfo Error information
   */
  public LEPublicationListException(int exceptionCode, String additionalInfo)
  {
    super(getMessageForCode(exceptionCode) + ": " + additionalInfo);
  }


  /**
   * Gets the error code for error message
   * 
   * @param exceptionCode
   * 
   * @return Error message for error code
   */
  private static String getMessageForCode(int exceptionCode)
  {
    String message = "";

    switch (exceptionCode)
    {
      case LE_EXCEPTION_CASSANDRA_NOT_RUNNING :
        message = "Cassandra is not running";
        break;

      case LE_EXCEPTION_ELASTICSEARCH_NOT_RUNNING :
        message = "Elasticsearch is not running";
        break;

      case LE_EXCEPTION_INVALID_CONFIG :
        message = "Invalid configuration";
        break;

      case LE_EXCEPTION_INVALID_LAST_RUN_DATE :
        message = "Invalid last run date format (yyyy/MM/dd HH:mm:ss)";
        break;

      case LE_EXCEPTION_EXCECUTE_IMPORT_JOB_FAILED :
        message = "Import database job failed";
        break;

      case LE_EXCEPTION_ISA_API_CREATION_FAILED :
        message = "Import database API creation failed";
        break;

      case LE_EXCEPTION_CONFIG_FILE_MISSING :
        message = "Configuration file missing";
        break;

      case LE_EXCEPTION_HISTORY_REST_SERVICE :
        message = "Get Period History web service failed.";
        break;

      case LE_EXCEPTION_ALREADY_RUNNING : 
        message = "Publication list process is already running.";
        break;

      case LE_EXCEPTION_FILESYSTEM_ERROR : 
        message = "Do not have permission to create/delete lock file";
        break;

      default:
        message = "Error";
    }

    return message;
  }
}
