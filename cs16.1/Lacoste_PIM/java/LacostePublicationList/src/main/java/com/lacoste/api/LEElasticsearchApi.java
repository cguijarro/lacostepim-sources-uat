package com.lacoste.api;

import com.datastax.driver.core.exceptions.NoHostAvailableException;
import com.exportstaging.api.exception.ExportStagingException;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;
import static org.elasticsearch.index.query.QueryBuilders.*;


/**
 * This class provides APIs to search in Elasticsearch
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  PublicationList
 */
public class LEElasticsearchApi implements ILELacosteElasticsearch
{
  private static Client client;
  private String indexName;

  /**
   * Creates connection to Elasticsearch for current session. check elasticsearch.properties file for this parameter
   *
   * @param clusterName Name of the cluster
   * @param hostName    Host Address
   * @param hostPort    TCP address
   * @param indexName   Name of the Index. eg elasticsearch.index.prefix + "_" + itemType (Note: everything in lowercase) export_pdmarticle for PIM itemType
   * @throws UnknownHostException If unable to connect to elasticsearch
   * @throws com.exportstaging.api.exception.ExportStagingException
   */
  public void setConnection(String clusterName, String hostName, int hostPort, String indexName) throws UnknownHostException, ExportStagingException {
    try
    {
      if ((indexName == null) || ("".equals(indexName)))
      {
        throw new ExportStagingException("[ESA Exception] IndexName cannot be null");
      }
      this.indexName = indexName;
      if ((clusterName == null) || ("".equals(clusterName))) {
        throw new ExportStagingException("[ESA Exception] clusterName cannot be null");
      }
      Settings settings = Settings.settingsBuilder().put("cluster.name", clusterName).build();
      client = TransportClient.builder().settings(settings).build().addTransportAddress
      (new InetSocketTransportAddress(InetAddress.getByName(hostName), hostPort));
    }
    catch (NoHostAvailableException e)
    {
      throw new ExportStagingException("Unable to create connection to ElasticSearch. Error cause: " + e.getMessage());
    }
  }

  /**
   * Gets the list of products where the target identifier is attached to the mentioned reference attributeID
   *
   * @param refID    String of attribute ID
   * @param targetID String of target Identifier
   * @param languageID String of language ID
   * @return List of productIDS that matches the condition for the references.
   */
  public List<String> getReferenceSearchResults(String refID, String targetID, String languageID) throws ExportStagingException
  {

    return performSearch(createReferenceSearch(refID, targetID), createLanguageFilter(languageID));
  }


  /**
   * Gets the IDs of active periods
   *
   * @param lastRunTime       String of the last run time stamp
   * @param activeAttributeID String of the active script job ID
   * @param languageID String of language ID
   * @return List of products which matches the filter or active.
   */
  @Override
  public List<String> getActivePeriods(String lastRunTime, String activeAttributeID, String languageID) throws ExportStagingException {

    return performSearch(
      createActivePeriodsSearch(lastRunTime, activeAttributeID),
      createLanguageFilter(languageID)
    );
  }


  /**
   * Gets the list periods which needs to be deactivated
   *
   * @param currentTime
   * @param endDateAttributeIDs
   * @param activeAttributeID
   * @param languageID String of language ID
   * @return list of product IDs which matches the filter or deactivated.
   */
  @Override
  public List<String> getDeactivatePeriods(
    String currentTime,
    List<String> endDateAttributeIDs,
    String activeAttributeID,
    String languageID
  ) throws ExportStagingException
  {

    return performSearch(
      createDeactivatePeriodsSearch(currentTime, endDateAttributeIDs, activeAttributeID),
      createLanguageFilter(languageID)
    );
  }


  /**
   * Gets the list of periods which needs to be activated
   *
   * @param currentTime
   * @param startDateEndDateMap
   * @param languageID String of language ID
   * @return list of products that matches the filter.
   */
  @Override
  public List<String> getActivatePeriods(
    String currentTime,
    Map<String, String> startDateEndDateMap,
    String activeAttributeID,
    String languageID
  ) throws ExportStagingException
  {

    return performSearch(
      createActivatedPeriodsSerach(currentTime, startDateEndDateMap, activeAttributeID),
      createLanguageFilter(languageID)
    );
  }


  /**
   * Gets the list of products where any period ID is attached under any refToPIMAttributeIDs
   * filters : Searches periods IDs in any Reference to PIM attribute both are as the list
   *
   * @param periodsIDs          product array (This need to search in the target identifier)
   * @param refToPIMAttributeID array of PIM attributes (return products array if we found any of targetID
   *                            in the any of reference attributes . )
   * @param languageID          String of language ID
   * 
   * @return List of products that found in the PIM attributes as target ID
   */
  @Override
  public List<String> searchColoursForPeriods(
    List<String> periodsIDs,
    List<String> refToPIMAttributeID,
    String languageID,
    String itemTypeAttributeID,
    String itemTypeID
    
  ) throws ExportStagingException
  {

    return performSearch(
      createsearchColoursForPeriods(periodsIDs, refToPIMAttributeID),
      createPostFilterForPeriodsSearch(languageID, itemTypeAttributeID, itemTypeID)
    );
  }


  /**
   * Immediate child of the mentioned folder or the parent
   *
   * @param folderID   Parent ID or the folder ID of the PIM .
   * @param languageID Language ID
   * 
   * @return list of immediate children of the folder ID.
   */
  @Override
  public List<String> getChildrenIDByFolderID(
      String folderID,
      String languageID,
      String itemTypeAttributeID,
      String itemTypeProductID
  ) throws ExportStagingException {

    return performSearch(
      createGetChildrenByFolderID(folderID),
      createPostFilterForPeriodsSearch(languageID, itemTypeAttributeID, itemTypeProductID)
    );
  }


  /**
   * Create the query for the active periods search that matches the current time is between the start and end time
   *
   * @param currentTime         Current time stamp.
   * @param startDateEndDateMap Key value pair of the start and end attribute IDs
   * @param activeAttributeID   String of active attribute ID
   * 
   * @return list of immediate children of the folder ID.
   */
  private BoolQueryBuilder createActivatedPeriodsSerach(
    String currentTime,
    Map<String,
    String> startDateEndDateMap,
    String activeAttributeID
  )
  {

    return prepareNestedQueryForActivatedPeriods(currentTime, startDateEndDateMap).mustNot(
      matchQuery(activeAttributeID + ":Value", 1)
    );
  }

  /**
   * creating query for the to get the immediate children of the the folder or parent. If there is no child present
   * will return the empty list
   *
   * @param FolderID String of the parent ID or the folder ID of the PIM.
   * 
   * @return BoolQueryBuilder object of query.
   */
    private BoolQueryBuilder createGetChildrenByFolderID(String FolderID)
    {

      return QueryBuilders.boolQuery().must(matchQuery("ParentID", FolderID));
    }


  /**
   * creating the query and added the filter on the language ID
   *
   * @param lastRunTime       Last run time stamp
   * @param activeAttributeID Check box attribute ID
   * 
   * @return BoolQueryBuilder object of query.
   */
  private BoolQueryBuilder createActivePeriodsSearch(String lastRunTime, String activeAttributeID) {

    return QueryBuilders.boolQuery().must(matchQuery(activeAttributeID + ":Value", 1))
      .must(rangeQuery("LastChange").gte(lastRunTime)
    );
  }


  /**
   * Preparing the builder query object based on the condition . here added the two conditions with the must query.
   * finding the current time stamp between the end attributes and activated is true.
   *
   * @param currentTime         LString of current time stamp
   * @param endDateAttributeIDs List of end attributeIDS
   * @param activeAttributeID   String of active attribute ID
   * 
   * @return BoolQueryBuilder object of the query builder
   */
  private BoolQueryBuilder createDeactivatePeriodsSearch(
    String currentTime,
    List<String> endDateAttributeIDs,
    String activeAttributeID
  )
  {

    return prepareNestedQueryWithList(endDateAttributeIDs, currentTime).must(
      matchQuery(activeAttributeID + ":Value", 1)
    );
  }


  private BoolQueryBuilder prepareNestedQueryWithList(List<String> attributeIDs, String currentTime)
  {
    BoolQueryBuilder builders = new BoolQueryBuilder();
    for (String attrID : attributeIDs)
    {
      builders.must(perpare(currentTime + " 00:00:00", attrID ));
    }

    return builders;
  }


  private BoolQueryBuilder perpare(String date, String attrID)
  {
    BoolQueryBuilder builder = new BoolQueryBuilder();
    builder.mustNot(existsQuery(attrID+":Value"));
    BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
    queryBuilder.should(QueryBuilders.rangeQuery(attrID + ":Value.date").lt(date)).should(builder);

    return queryBuilder;
  }


  /**
   * prepared the internal query for the reference identifier in the mentioned list of attributes and targetIDs
   *
   * @param periodsIDs          List target product IDs
   * @param refToPIMAttributeID List of reference attributes
   * 
   * @return BoolQueryBuilder object of the query builder
   */
  private BoolQueryBuilder createsearchColoursForPeriods(
      List<String> periodsIDs,
      List<String> refToPIMAttributeID
  )
  {
    BoolQueryBuilder builders = new BoolQueryBuilder();
    for (String attrID : refToPIMAttributeID) {
      builders.should(QueryBuilders.termsQuery("Reference." + attrID + ".TargetID", periodsIDs));
    }

    return builders;
  }


  /**
   * prepared the internal loop query based on the start and end attribute ID and added should condition to
   * to match any of the start and end attribute IDs
   *
   * @param currentTime  String of current time stamp.
   * @param startEndAttr key value pair of the start and end attribute ids
   * @return BoolQueryBuilder object of the query builder
   */

  private BoolQueryBuilder prepareNestedQueryForActivatedPeriods(
    String currentTime,
    Map<String, String> startEndAttr
  )
  {
    BoolQueryBuilder builders = new BoolQueryBuilder();

    for (Map.Entry<String, String> data : startEndAttr.entrySet())
    {
      builders.should(QueryBuilders.boolQuery().must(
        QueryBuilders.rangeQuery(data.getKey() + ":Value.date").lte(currentTime + " 23:59:59"))
        .must(QueryBuilders.rangeQuery(data.getValue() + ":Value.date").gte(currentTime + " 00:00:00"))
      );
    }

    return builders;
  }


  /**
   * prepared the internal query based on the reference search. searched for the target ID in the mentioned
   * reference attribute ID.
   *
   * @param refernceAttributeID String of Reference Attribute ID.
   * @param targetID            string of target attached product to reference attribute ID
   * 
   * @return BoolQueryBuilder object of the query builder
   */
    private BoolQueryBuilder createReferenceSearch(String refernceAttributeID, String targetID)
    {
      return QueryBuilders.boolQuery().should(
        matchQuery("Reference." + refernceAttributeID + ".TargetID", targetID)
      );
    }


  /**
   * prepared the internal filter for the language dependent data for all conditions.
   *
   * @param languageID String of Language ID.
   * 
   * @return BoolQueryBuilder object of the query builder
   */
    private QueryBuilder createLanguageFilter(String languageID)
    {
      return QueryBuilders.boolQuery().must(matchQuery("LanguageID", languageID));
    }


    /**
     * prepared the internal filter for the language dependent data for all conditions.
     *
     * @param languageID String of Language ID.
     * 
     * @return BoolQueryBuilder object of the query builder
     */
     private QueryBuilder createPostFilterForPeriodsSearch(String languageID, String attrID, String targetID)
     {
       return QueryBuilders.boolQuery().must(matchQuery("LanguageID", languageID)).must(QueryBuilders.matchQuery("Reference." + attrID + ".TargetID", targetID));
     }


  /**
   * Searched the data depends on the condition on the server side.
   *
   * @param query  BoolQueryBuilder object which contains the query and the object
   * @param filter BoolQueryBuilder object which having the filter for the data.
   * @return List of the result
   */
  private List<String> performSearch(BoolQueryBuilder query, QueryBuilder filter) throws ExportStagingException
  {
    List<String> results = new ArrayList<>();
    if (this.indexName == null) {
      throw new ExportStagingException("IndexName not provided");
    }
    if (client == null)
    {
      throw new ExportStagingException("[ESA Exception] Elasticsearch connection error. Retry");
    }
    try
    {
      SearchResponse scrollResp = client.prepareSearch(indexName)
       .setTypes("item")
       .setQuery(query)
       .setPostFilter(filter)
       .setFetchSource(new String[]{"_id"}, null)
       .setScroll(new TimeValue(60000))
       .setSize(10000).get();
      do
      {
        for (SearchHit hit : scrollResp.getHits().getHits())
        {
          results.add(hit.getId().split("_")[0]);
        }

        scrollResp = client.prepareSearchScroll(scrollResp.getScrollId())
          .setScroll(new TimeValue(60000))
          .execute().actionGet();
      } while (scrollResp.getHits().getHits().length != 0);

    } catch (Exception e) {
      throw new ExportStagingException("Error cause: " + e.getMessage());
    }

    return results;
  }


  @Override
  public Map<String, Map<String, List<String>>> getColoursForPeriods(
      List<String> periodsIDs,
      List<String> refToPIMAttributeID,
      String languageID,
      String itemTypeAttributeID,
      String itemTypeID
  ) throws ExportStagingException
  {
    String[] sourcePath = new String[refToPIMAttributeID.size() + 1];
    sourcePath[0] = "_ID";
    for (int i = 1; i <= refToPIMAttributeID.size() ; i++) {
      sourcePath[i] = "Reference." + refToPIMAttributeID.get(i-1) + ".TargetID";
    }

    return performSearchForReferences(
      createsearchColoursForGetPeriods(periodsIDs, refToPIMAttributeID),
      createPostFilterForPeriodsSearch(languageID, itemTypeAttributeID, itemTypeID),
      sourcePath,
      periodsIDs
    );
  }


  private Map<String, Map<String, List<String>>> performSearchForReferences(BoolQueryBuilder query, QueryBuilder filter, String[] sourcePath, List<String> target) throws ExportStagingException {
    if (this.indexName == null) {
      throw new ExportStagingException("IndexName not provided");
    }
    if (client == null) {
      throw new ExportStagingException("[ESA Exception] Elasticsearch connection error. Retry");
    }
    Map<String, Map<String, List<String>>> results = new HashMap<>();
    try
    {
      SearchResponse scrollResp = client.prepareSearch(indexName)
        .setTypes("item")
        .setQuery(query)
        .setPostFilter(filter)
        .setFetchSource(sourcePath, null)
        .setScroll(new TimeValue(60000))
        .setSize(10000).get();

      do
      {
        for (SearchHit hit : scrollResp.getHits())
        {
          String itemID = hit.getId().split("_")[0];
          results.put(itemID, new HashMap<>());
          Map<String, List<Map<String, String>>> ref = new HashMap<>();
          ref.putAll((Map<? extends String, ? extends List<Map<String, String>>>) hit.getSource().get("Reference"));
          for (Map.Entry<String, List<Map<String, String>>> entry : ref.entrySet())
          {
            List<String> targetIDs = new ArrayList<>();
            for (Map<String, String> targetID : entry.getValue())
            {
              if (target.contains(targetID.get("TargetID")))
              {
                targetIDs.add(targetID.get("TargetID"));
              }
            }
            if (!targetIDs.isEmpty())
            {
              results.get(itemID).putIfAbsent(entry.getKey(), targetIDs);
            }
          }
        }

        scrollResp = client.prepareSearchScroll(scrollResp.getScrollId())
          .setScroll(new TimeValue(60000))
          .execute().actionGet();
      } while (scrollResp.getHits().getHits().length != 0);

    }
    catch (Exception e)
    {
      LEPublicationListApi.printStackTrace(e);
      throw new ExportStagingException("Error cause: " + e.getMessage());
    }

    return results;
  }


  private BoolQueryBuilder createsearchColoursForGetPeriods(
    List<String> periodsIDs,
    List<String> refToPIMAttributeID
  )
  {
    BoolQueryBuilder builders = new BoolQueryBuilder();

    for (String attrID : refToPIMAttributeID)
    {
      builders.should(QueryBuilders.termsQuery("Reference." + attrID + ".TargetID", periodsIDs));
    }

    return builders;
  }


  /**
   * Gets the IDs of overlapping periods.
   *
   * @param endDateAttributeIDs End date attribute IDs
   * @param activeAttributeID   Active Attribute ID
   *
   * @return List of overlapping periods
   *
   * @throws ExportStagingException
   */
  public List<String> getOverlappingPeriods(
    List<String> endDateAttributeIDs,
    String activeAttributeID
  ) throws ExportStagingException
  {
    BoolQueryBuilder queryBuilder   = createSearchForGetOverlappingPeriods(endDateAttributeIDs, activeAttributeID);
    List<String> overlappingPeriods = performSearch(queryBuilder, createLanguageFilter(LEPublicationListApi.LANGUAGE_ID)); 

    return overlappingPeriods;
  }


  /**
   * Create search for overlapping periods
   *
   * @param endDateAttributeIDs End date attribute IDs
   * @param activeAttributeID   Active Attribute ID
   *
   * @return BoolQueryBuilder object
   */
  private BoolQueryBuilder createSearchForGetOverlappingPeriods(
    List<String> endDateAttributeIDs,
    String activeAttributeID
  )
  {
    String currentDate = LEPublicationListApi.getDate("yyyy-MM-dd");;

    BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery()
      .must(matchQuery(activeAttributeID + ":Value", 1));

    for(String attributeID : endDateAttributeIDs)
    {
      queryBuilder.should(rangeQuery(attributeID + ":Value.date").lt(currentDate));
    }

    queryBuilder.minimumNumberShouldMatch(1);

    return queryBuilder;
  }


  /**
   * Gets the product data from Elasticsearch
   *
   * @param productID  Product ID
   * @param sources    Sources
   * @param languageID Language ID
   *
   * @return Map of product data
   *
   * @throws ExportStagingException 
   */
  public Map<String, String> getProductByID(
    String productID,
    String[] sources,
    int languageID
  ) throws ExportStagingException
  {
    Map<String, String> productData = new HashMap<>();

    BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery()
      .must(matchQuery("ID", productID));

    QueryBuilder filter = createLanguageFilter(languageID + "");

    if (this.indexName == null) {
      throw new ExportStagingException("IndexName not provided");
    }

    if (client == null) {
      throw new ExportStagingException("[ESA Exception] Elasticsearch connection error. Retry");
    }

    try
    {
      SearchResponse scrollResp = client.prepareSearch(indexName)
        .setTypes("item")
        .setQuery(queryBuilder)
        .setPostFilter(filter)
        .setFetchSource(sources, null)
        .setScroll(new TimeValue(60000))
        .setSize(10000).get();

      do
      {
        for (SearchHit hit : scrollResp.getHits())
        {
          Map<String, Object> data = hit.getSource();

          for(int index = 0; index < sources.length; index++)
          {
            if(data.containsKey(sources[index]))
            {
              String value = ((data.get(sources[index]) == null) ? "" : data.get(sources[index]).toString());
              productData.put(sources[index], value);
            }
          }
        }

        scrollResp = client.prepareSearchScroll(scrollResp.getScrollId())
          .setScroll(new TimeValue(60000))
          .execute().actionGet();
      } while (scrollResp.getHits().getHits().length != 0);

    }
    catch (Exception e)
    {
      LEPublicationListApi.printStackTrace(e);
      throw new ExportStagingException("Error cause: " + e.getMessage());
    }

    return productData;
  }
}
