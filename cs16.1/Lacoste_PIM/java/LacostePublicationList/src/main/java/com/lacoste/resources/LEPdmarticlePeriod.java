package com.lacoste.resources;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.lacoste.api.LEPublicationListApi;
import com.lacoste.api.LEPublicationListConstants;
import com.lacoste.api.LEPublicationListException;


/**
 * Period item to be inserted in Import database
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  PublicationList
 */
public class LEPdmarticlePeriod extends LEPdmarticle
{


  /**
   * Active flag. If 1 then active else not active 
   */
  private int isActive;

  /**
   * Active attribute IDs
   */
  private Set<String> activeAttributeIds;

  /**
   * TRUE if channel attribute changed since last run
   */
  private boolean isChannelAttributeChanged = false;


  /**
   * Constructor to set the Active flag
   *  
   * @param isActive
   */
  public LEPdmarticlePeriod(int isActive)
  {
    super();
    try
    {
      setActive(isActive);
    }
    catch (LEPublicationListException e)
    {
      LEPublicationListApi.printStackTrace(e);
    }
  }


  /**
   * Default constructor
   */
  public LEPdmarticlePeriod()
  {
  }


  /**
   * Gets the active status of period
   * 
   * @return 1 if active else 0
   */
  public int isActive()
  {

    return isActive;
  }


  /**
   * Gets the active attribute ID for the Period
   * 
   * @return Set of Active attribute
   */
  public Set<String> getActiveAttributeIds() {
    return activeAttributeIds;
  }


  /**
   * Sets the active attribute IDs for a Period
   * 
   * @param activeAttributeIds Set of Attribute IDs in String format
   */
  public void setActiveAttributeIds(Set<String> activeAttributeIds) {
    this.activeAttributeIds = activeAttributeIds;
  }

  /**
   * Sets the active status of the period
   * 
   * @param isActive
   * 
   * @throws LEPublicationListException
   */
  public void setActive(int isActive) throws LEPublicationListException
  {
    this.isActive                       = isActive;
    Map<String, String> attributeValues = new HashMap<String, String>();
    String attribute                    = "";
    attribute                           = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_ACTIVE);
    attribute                           = LEPublicationListApi.getAttributeExternalKeyById(attribute);
    attributeValues.put(attribute, intToString(this.isActive));
    this.setAttributes(attributeValues, LEPublicationListApi.LANGUAGE_CODE);
  }


  /**
   * Converts integer value to string
   * 
   * @param value
   * 
   * @return String representation of the supplied integer
   */
  private String intToString(int value)
  {
    return value + "";
  }


  /**
   * TRUE if channel attribute is changed
   * 
   * @return boolean
   */
  public boolean isChannelAttributeChanged() {
    return isChannelAttributeChanged;
  }


  /**
   * Sets the instance variable isChannelAttributeChanged
   * 
   * @param isChannelAttributeChanged
   */
  public void setChannelAttributeChanged(boolean isChannelAttributeChanged) {
    this.isChannelAttributeChanged = isChannelAttributeChanged;
  }
}
