package com.lacoste.periods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.exportstaging.api.ExternalItemAPI;
import com.exportstaging.api.exception.ExportStagingException;
import com.exportstaging.api.wraper.ItemWrapper;
import com.lacoste.api.LEPublicationListApi;
import com.lacoste.api.LEPublicationListConstants;
import com.lacoste.api.LEPublicationListException;
import com.lacoste.resources.LEPdmarticleColor;
import com.lacoste.resources.LEPdmarticlePeriod;
import com.lacoste.resources.LEPdmarticleSize;


/**
 * Active period- This class contains APIs to handle the Active period case
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  PublicationList
 */
public class LEPublicationListActivePeriod extends LEPublicationListPeriod
{

  List<String> activePeriodsScheduledButNotModifiedSinceLastRun = new ArrayList<>();

  /**
   * Default constructor
   * 
   * @throws LEPublicationListException
   */
  public LEPublicationListActivePeriod() throws LEPublicationListException
  {
    super();
  }


  @Override
  public ArrayList<LEPdmarticlePeriod> getContextPeriods() throws LEPublicationListException
  {
    ArrayList<LEPdmarticlePeriod> activePeriods = new ArrayList<>();
    try
    {
      ExternalItemAPI itemAPI = LEPublicationListApi.getExternalItemEsaAPI();

      String lastRunTime = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.O_LAST_RUN_TIMESTAMP);
      String activeAttributeID = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_ACTIVE);
      String channelAttributeID = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_CHANNEL);

      if(itemAPI != null)
      {

        List<String> activePeriodsList = LEPublicationListApi.getElasticsearchApi().getActivePeriods(
          lastRunTime,
          activeAttributeID,
          LEPublicationListApi.LANGUAGE_ID
        );

        List<String> validPeriodIds = new ArrayList<String>();

        for(String itemID : activePeriodsList)
        {
          if(!LEPublicationListApi.isPeriodDeactivated(itemID))
          {
            Map<String, Map<String, Set<String>>> changedData = LEPublicationListApi.getChangedData(itemID);

            if(hasInsignificantChanges(changedData))
            {
              continue;
            }
            else
            {
              ItemWrapper itemWrapper = itemAPI.getItemById(Long.parseLong(itemID));
              if(itemWrapper != null)
              {
                Set<String> activeAttributeIds = this.getChannelForPeriod(itemWrapper);

                if(changedData.containsKey(channelAttributeID))
                {
                  String externalKey      = itemWrapper.getExternalKey();
                  if(!LEPublicationListApi.isEmpty(externalKey))
                  {
                    LEPdmarticlePeriod period = new LEPdmarticlePeriod();
                    period.setID(itemID);
                    period.setItemExternalKey(externalKey);
                    period.setActiveAttributeIds(activeAttributeIds);
                    period.setChannelAttributeChanged(true);
                    activePeriods.add(period);
                    validPeriodIds.add(itemID);
                  }
                  else
                  {
                    LEPublicationListApi.log("Active Period ID: " + itemID + " do not have external key.");
                  }
                }
                else if (isBeginOrEndDateAttributeChanged(changedData) || isSeasonalDataAttributeChanged(changedData))
                {
                  String externalKey = itemWrapper.getExternalKey();
                  if(!LEPublicationListApi.isEmpty(externalKey))
                  {
                    LEPdmarticlePeriod period = new LEPdmarticlePeriod();
                    period.setID(itemID);
                    period.setItemExternalKey(externalKey);
                    period.setActiveAttributeIds(activeAttributeIds);
                    activePeriods.add(period);
                    validPeriodIds.add(itemID);
                  }
                  else
                  {
                    LEPublicationListApi.log("Active Period ID: " + itemID + " do not have external key.");
                  }
                }
              }
              else
              {
                LEPublicationListApi.log("Active Period ID: " + itemID + " do not found in export database.");
              }
            }
          }
        }

        this.activePeriodsScheduledButNotModifiedSinceLastRun = getActiveAndScheduledPeriods(validPeriodIds);

        for(String itemID : this.activePeriodsScheduledButNotModifiedSinceLastRun)
        {
          if(!LEPublicationListApi.isPeriodDeactivated(itemID))
          {
            ItemWrapper itemWrapper = itemAPI.getItemById(Long.parseLong(itemID));
            if(itemWrapper != null)
            {
              Set<String> activeAttributeIds = this.getChannelForPeriod(itemWrapper);

              String externalKey      = itemWrapper.getExternalKey();
              if(!LEPublicationListApi.isEmpty(externalKey))
              {
                LEPdmarticlePeriod period = new LEPdmarticlePeriod();
                period.setID(itemID);
                period.setItemExternalKey(externalKey);
                period.setActiveAttributeIds(activeAttributeIds);
                period.setChannelAttributeChanged(true);
                activePeriods.add(period);
              }
              else
              {
                LEPublicationListApi.log("Active Period ID: " + itemID + " do not have external key.");
              }
            }
            else
            {
              LEPublicationListApi.log("Active Period ID: " + itemID + " do not found in export database.");
            }
          }
        }

        List<String> overlappedPeriodIDs = LEPublicationListApi.getOverlappedPeriods();
        LEPublicationListApi.log("Overlapping Active Periods: " + overlappedPeriodIDs.toString());

        for(String itemID : overlappedPeriodIDs)
        {
          String falseImportID = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_FALSE_IMPORT);
          String falseImportAttributeExKey = LEPublicationListApi.getAttributeExternalKeyById(falseImportID);

          if(!LEPublicationListApi.isPeriodDeactivated(itemID))
          {
            ItemWrapper itemWrapper = itemAPI.getItemById(Long.parseLong(itemID));

            if(itemWrapper != null)
            {
              String externalKey             = itemWrapper.getExternalKey();
              Set<String> activeAttributeIds = this.getChannelForPeriod(itemWrapper);

              if(activeAttributeIds.isEmpty())
              {
                LEPublicationListApi.log("Overlapping Period ID: "
                  + itemID + " do not have active attributes hence, skipped."
                );
              }
              else if(!LEPublicationListApi.isEmpty(externalKey))
              {
                LEPdmarticlePeriod period = new LEPdmarticlePeriod();
                period.setID(itemID);
                period.setItemExternalKey(externalKey);
                String label = itemWrapper.getFormattedValue("Label");
                period.setLabel(label, LEPublicationListApi.LANGUAGE_CODE);
                period.setFalseImportAttribute(falseImportAttributeExKey);
                period.setActiveAttributeIds(activeAttributeIds);

                //stores overlapped period
                LEPublicationListApi.storeItem(period);

                activePeriods.add(period);
              }
              else
              {
                LEPublicationListApi.log("Ovelapping Active Period ID: " + itemID + " do not have external key.");
              }
            }
            else
            {
              LEPublicationListApi.log("Ovelapping Active Period ID: " + itemID + " do not found in export database.");
            }
          }
        }

      }
    }
    catch(Exception e)
    {
      LEPublicationListApi.printStackTrace(e);
      throw new LEPublicationListException(0, "Get Periods for 'Active' case failed due to " + e.getMessage());
    }

    return activePeriods;
  }


  @Override
  public ArrayList<LEPdmarticleColor> getColors(ArrayList<LEPdmarticlePeriod> periods) throws LEPublicationListException
  {
    ArrayList<LEPdmarticleColor> colors = new ArrayList<>();
    ExternalItemAPI itemAPI             = null;
    try
    {
      itemAPI = LEPublicationListApi.getExternalItemEsaAPI();

      ArrayList<String> periodsIDs = new ArrayList<>();
      Map<String, LEPdmarticlePeriod> periodsMap = new HashMap<>();
      Set<String> activePeriodsWithNoChannles = new HashSet<>();

      for(LEPdmarticlePeriod period : periods)
      {
        String periodID = period.getID();
        periodsMap.put(periodID, period);
        periodsIDs.add(periodID);
      }

      LEPublicationListApi.log("Active Periods - " + Arrays.deepToString(periodsIDs.toArray()));

      ArrayList<String> refToPIMAttributeIDs = new ArrayList<>();

      String activePeriodDemandwareEuId  = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_ACTIVE_DWARE_EU);
      String activePeriodDemandwareNaId  = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_ACTIVE_DWARE_NA);
      String activePeriodDemandwareLaId  = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_ACTIVE_DWARE_LA);
      String activePeriodMarketplaceEuId = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_ACTIVE_MPLACE_EU);
      String activePeriodMarketplaceNaId = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_ACTIVE_MPLACE_NA);

      String scheduledPeriodDemandwareEuId  = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_SCHED_DWARE_EU);
      String scheduledPeriodDemandwareNaId  = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_SCHED_DWARE_NA);
      String scheduledPeriodDemandwareLaId  = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_SCHED_DWARE_LA);
      String scheduledPeriodMarketplaceEuId = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_SCHED_MPLACE_EU);
      String scheduledPeriodMarketplaceNaId = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_SCHED_MPLACE_NA);

      Map<String, String> activeVsScheduledAttributes = new HashMap<String, String>();
      activeVsScheduledAttributes.put(activePeriodDemandwareEuId, scheduledPeriodDemandwareEuId);
      activeVsScheduledAttributes.put(activePeriodDemandwareNaId, scheduledPeriodDemandwareNaId);
      activeVsScheduledAttributes.put(activePeriodDemandwareLaId, scheduledPeriodDemandwareLaId);
      activeVsScheduledAttributes.put(activePeriodMarketplaceEuId, scheduledPeriodMarketplaceEuId);
      activeVsScheduledAttributes.put(activePeriodMarketplaceNaId, scheduledPeriodMarketplaceNaId);

      refToPIMAttributeIDs.addAll(activeVsScheduledAttributes.keySet());
      refToPIMAttributeIDs.addAll(activeVsScheduledAttributes.values());

      String itemTypeAttributeID = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_ITEMTYPE);
      String itemTypeProductID = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.ITEMTYPE_COLOR);

      Map<String, Map<String, List<String>>> activeColors = LEPublicationListApi.getElasticsearchApi().getColoursForPeriods(
          periodsIDs,
          refToPIMAttributeIDs,
          LEPublicationListApi.LANGUAGE_ID,
          itemTypeAttributeID,
          itemTypeProductID
      );

      Set<String> activeColorIds = activeColors.keySet();

      LEPublicationListApi.log("Colors for Active case - " + Arrays.deepToString(activeColorIds.toArray()));

      for(String colorID : activeColorIds)
      {

        boolean addFlag = false;

        ItemWrapper item = itemAPI.getItemById(Long.parseLong(colorID));
        if(item != null)
        {
          LEPdmarticleColor color = new LEPdmarticleColor();
          String externalKey      = item.getExternalKey();
          String label            = item.getValue("Label");

          color.setID(colorID);
          color.setItemExternalKey(externalKey);
          color.setIsFolder(item.getValue("IsFolder"));
          color.setLabel(label, LEPublicationListApi.LANGUAGE_CODE);

          Map<String, List<String>> targets = activeColors.get(colorID);
          Set<String> attributeIDs = targets.keySet();

          for(String attrID : attributeIDs)
          {
            List<String> periodIds = activeColors.get(colorID).get(attrID);
            for(String periodID : periodIds)
            {
              LEPdmarticlePeriod period = periodsMap.get(periodID);
              Set<String> activeAttributeForPeriod = period.getActiveAttributeIds();
              if(activeAttributeForPeriod.size() == 0)
              {
                activePeriodsWithNoChannles.add(periodID);
              }

              if(activeVsScheduledAttributes.values().contains(attrID))
              {

                for(String id : activeAttributeForPeriod)
                {
                  addFlag = true;
                  color.setStoreColorFlag(true);
                  this.setPeriod(period, color, id);
                }

                if(!activeAttributeForPeriod.isEmpty())
                {
                  addFlag = true;
                  color.setStoreColorFlag(true);
                  this.removePeriod(period, color, attrID);
                }
                continue;
              }
              else
              {

                if(this.activePeriodsScheduledButNotModifiedSinceLastRun.contains(periodID))
                {
                  continue;
                }

                addFlag = true;
                if(!activeAttributeForPeriod.contains(attrID))
                {
                  color.setStoreColorFlag(true);
                  this.removePeriod(period, color, attrID);
                }

                if(activeAttributeForPeriod.isEmpty())
                {
                  color.setStoreColorFlag(true);
                  String scheduledAttributeID = activeVsScheduledAttributes.get(attrID);
                  this.setPeriod(period, color, scheduledAttributeID);
                }
                Set<String> tempActiveAttributeForPeriod = new HashSet<>(activeAttributeForPeriod);
                tempActiveAttributeForPeriod.removeAll(attributeIDs);
                for(String id : tempActiveAttributeForPeriod)
                {
                  color.setStoreColorFlag(true);
                  this.setPeriod(period, color, id);
                }
              }
            }
          }
          if(addFlag) {
            colors.add(color);
          }
        }
      }
      if(!activePeriodsWithNoChannles.isEmpty())
      {
        LEPublicationListApi.log("Active Periods with no Channels: " + Arrays.toString(activePeriodsWithNoChannles.toArray()));
      }
    }
    catch(Exception e)
    {
      LEPublicationListApi.printStackTrace(e);
      throw new LEPublicationListException(0, "Get colors for 'Active' case failed due to - " + e.getMessage());
    }

    return colors;
  }


  /**
   * Processing active periods case
   */
  public void start()
  {
    LEPublicationListApi.log("//////////////////////////Started processing Active Period//////////////////////////");
    try
    {
      ArrayList<LEPdmarticlePeriod> periodList = this.getContextPeriods();

      ArrayList<LEPdmarticleColor> colorList = this.getColors(periodList);

      for(LEPdmarticleColor color : colorList)
      {

        if(color.isStoreColorFlag())
        {
          LEPublicationListApi.storeItem(color);
        }

        ArrayList<LEPdmarticleSize> sizeList = this.getSizes(color);

        LEPublicationListApi.log("Sizes for Color ID " + color.getID() + ": " + this.getSizeList());

        if(sizeList != null && sizeList.size() != 0)
        {

          for(LEPdmarticleSize size : sizeList)
          {
            LEPublicationListApi.storeItem(size);
          }
        }
      }
    }
    catch(Exception e)
    {
      LEPublicationListApi.log(e.getMessage());
      LEPublicationListApi.printStackTrace(e);
    }
    finally
    {
      LEPublicationListApi.log("//////////////////////////End processing Active Period//////////////////////////////");
    }
  }


  /**
   * Check if begin and end date attribute is changed
   * 
   * @param changedData attribute ID => new value & old value
   * 
   * @return boolean if Begin & End date attributes are changed since last run start time
   * 
   * @throws LEPublicationListException
   */
  private boolean isBeginOrEndDateAttributeChanged(Map<String, Map<String, Set<String>>> changedData) throws LEPublicationListException
  {
    String beginDateAttributes[] = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_BEGIN_DATE).split(",");
    String endDateAttributes[] = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_BEGIN_DATE).split(",");

    Set<String> beginDateEndDateAttribute = new HashSet<>(Arrays.asList(beginDateAttributes));
    beginDateEndDateAttribute.addAll(Arrays.asList(endDateAttributes));

    for(String key : changedData.keySet())
    {
      if(beginDateEndDateAttribute.contains(key))
      {
        return true;
      }
    }

    return false;
  }


  /**
   * Check if Seasonal Data attribute is changed
   * 
   * @param changedData attribute ID => new value & old value
   * 
   * @return boolean if Seasonal Data attribute is changed since last run start time
   * 
   * @throws LEPublicationListException
   */
  private boolean isSeasonalDataAttributeChanged(Map<String, Map<String, Set<String>>> changedData) throws LEPublicationListException
  {
    String seasonalDataAttributeID = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_SEASONAL_DATA);

    if(changedData.containsKey(seasonalDataAttributeID))
    {
      return true;
    }

    return false;
  }


  /**
   * Gets the periods which are active but not modified since last run start time & present in 
   * Scheduled attributes of any color.
   * 
   * @param activePeriodsChangedSinceLastRun List of active periods IDs those are changed since last run
   * 
   * @return List of Period IDs
   * 
   * @throws LEPublicationListException 
   */
  public List<String> getActiveAndScheduledPeriods(List<String> activePeriodsChangedSinceLastRun) throws LEPublicationListException
  {
    List<String> activePeriodsList = new ArrayList<>();

    try
    {
      String firstDate         = LEPublicationListApi.getDate(new Date(0), "");
      String activeAttributeID = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_ACTIVE);

      activePeriodsList = LEPublicationListApi.getElasticsearchApi().getActivePeriods(
        firstDate,
        activeAttributeID,
        LEPublicationListApi.LANGUAGE_ID
      );

      ArrayList<String> scheduledPeriodsAttributeIDs = new ArrayList<>();

      String scheduledPeriodDemandwareEU  = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_SCHED_DWARE_EU);
      String scheduledPeriodDemandwareNA  = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_SCHED_DWARE_NA);
      String scheduledPeriodDemandwareLA  = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_SCHED_DWARE_LA);
      String scheduledPeriodMarketplaceEU = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_SCHED_MPLACE_EU);
      String scheduledPeriodMarketplaceNA = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_SCHED_MPLACE_NA);

      scheduledPeriodsAttributeIDs.add(scheduledPeriodDemandwareEU);
      scheduledPeriodsAttributeIDs.add(scheduledPeriodDemandwareNA);
      scheduledPeriodsAttributeIDs.add(scheduledPeriodDemandwareLA);
      scheduledPeriodsAttributeIDs.add(scheduledPeriodMarketplaceEU);
      scheduledPeriodsAttributeIDs.add(scheduledPeriodMarketplaceNA);

      String itemTypeAttributeID = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_ITEMTYPE);
      String itemTypeProductID   = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.ITEMTYPE_COLOR);

      Map<String, Map<String, List<String>>> activeColors = LEPublicationListApi.getElasticsearchApi().getColoursForPeriods(
        activePeriodsList,
        scheduledPeriodsAttributeIDs,
        LEPublicationListApi.LANGUAGE_ID,
        itemTypeAttributeID,
        itemTypeProductID
      );

      Set<String> activePeriodsButNotModifiedSinceLastRun = new HashSet<>();

      for(String colorID : activeColors.keySet())
      {
        for(String scheduledAttributeID : activeColors.get(colorID).keySet())
        {
          List<String> periodIds = activeColors.get(colorID).get(scheduledAttributeID);
          activePeriodsButNotModifiedSinceLastRun.addAll(periodIds);
        }
      }

      activePeriodsList.retainAll(activePeriodsButNotModifiedSinceLastRun);
      activePeriodsList.removeAll(activePeriodsChangedSinceLastRun);
    }
    catch(LEPublicationListException exception)
    {
      LEPublicationListApi.printStackTrace(exception);
      throw exception;
    }
    catch(ExportStagingException exception)
    {
      LEPublicationListApi.printStackTrace(exception);
      throw new LEPublicationListException(0, exception.getMessage());
    }

    return activePeriodsList;
  }


  /**
   * Checks if changed data contains any insignificant changes.
   *
   * @param changedData Changed data
   *
   * @return TRUE if unimportant changes else FALSE
   *
   * @throws Exception
   */
  private boolean hasInsignificantChanges(Map<String, Map<String, Set<String>>> changedData) throws Exception
  {
    String activeAttribute = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_ACTIVE);
    String falseImport = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_FALSE_IMPORT);

    if(changedData.size() == 1 && (changedData.containsKey(activeAttribute) || changedData.containsKey(falseImport)))
    {

      return true;
    }

    return false;
  }
}
