package com.lacoste.main;

import com.lacoste.api.LEPublicationListApi;
import com.lacoste.api.LEPublicationListConstants;
import com.lacoste.api.LEPublicationListException;
import com.lacoste.api.LERestClient;
import com.lacoste.periods.LEPublicationListActivatePeriod;
import com.lacoste.periods.LEPublicationListActivePeriod;
import com.lacoste.periods.LEPublicationListDeactivatePeriod;


/**
 * Main class
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  PublicationList
 */
public class LEPublicatioListMainClass
{

  /**
   * Starting point of the Publication list process
   * 
   * @param args Command line arguments
   */
  public static void main(String args[])
  {
    boolean processRunning = true;
    try
    {
      System.out.println(LEPublicationListApi.getDate("") + " Process started");

      LEPublicationListApi.log("Publication list process started");

      processRunning = LEPublicationListApi.isProcessRunning();

      String startTime = LEPublicationListApi.getDate("");

      String lastRunTimeStamp = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.O_LAST_RUN_TIMESTAMP);
      LEPublicationListApi.log("Last run Time : " + lastRunTimeStamp);

      LEPublicationListDeactivatePeriod lePublicationListDeactivatePeriod = new LEPublicationListDeactivatePeriod();
      lePublicationListDeactivatePeriod.start();

      LEPublicationListActivePeriod lePublicationListActivePeriod = new LEPublicationListActivePeriod();
      lePublicationListActivePeriod.start();

      LEPublicationListActivatePeriod lePublicationListActivatePeriod = new LEPublicationListActivatePeriod();
      lePublicationListActivatePeriod.start();

      LERestClient.start();
      LEPublicationListApi.setProperty(LEPublicationListConstants.O_LAST_RUN_TIMESTAMP, startTime);
    }
    catch(Exception e)
    {
      LEPublicationListApi.log(e.getMessage());
      LEPublicationListApi.printStackTrace(e);
    }
    finally
    {
      if(!processRunning)
      {
        if(!LEPublicationListApi.deleteLockFile())
        {
          LEPublicationListApi.log("Please delete the lock file present at path " + LEPublicationListApi.getLockFileName());
        }
      }

      LEPublicationListApi.log("Publication list process finished");
      
      System.out.println(LEPublicationListApi.getDate("") + " Process Ended");
      try
      {
        LEPublicationListApi.getExternalItemEsaAPI().close();
      }
      catch (LEPublicationListException ex)
      {
        LEPublicationListApi.printStackTrace(ex);
      }
    }
  }
}
