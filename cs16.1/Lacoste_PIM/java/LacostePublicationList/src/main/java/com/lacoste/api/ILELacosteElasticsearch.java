package com.lacoste.api;

import java.util.List;
import java.util.Map;


/**
 * Provides general functions to be implemented to handle Period case
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  PublicationList
 */
interface ILELacosteElasticsearch
{


  /**
   * Gets the IDs of active periods
   * 
   * Filters : Filter on (all « Availability » list PIM items) AND (“Active” attribute value = true) AND
   *            (time/date last run => “Last Change” attribute value).
   * 
   * @param lastRunTime       (yyyy/MM/dd HH:MM:SS)
   * @param activeAttributeID Check box attribute ID
   * @param languageID        Language ID
   * 
   * @return List of Active Periods IDs
   */
  public List<String> getActivePeriods(String lastRunTime, String activeAttributeID, String languageID) throws Exception;


  /**
   * Gets the list of periods which needs to be deactivated
   * 
   * Filters : Filter on (all « Availability » list PIM items) 
   *           AND (“Active” attribute value = true) 
   *           AND ((date of today > “End date A” attribute value)
   *             AND (date of today > “End date B” attribute value) AND ...)
   * 
   * @param currentTime
   * @param endDateAttributeIDs
   * @param activeAttributeID
   * 
   * @return List of Deactivate Periods IDs
   */
  public List<String> getDeactivatePeriods(String currentTime, List<String> endDateAttributeIDs, String activeAttributeID, String languageID) throws Exception;


  /**
   * Gets the list of periods which needs to be activated
   * 
   * Filters : Filter on (all « Availability » list PIM items) AND (“Active” attribute value = false) AND
   *           ((date of today <= “End date A” attribute value) OR (date of today <= “End date B”
   *           attribute value) OR ...)
   * 
   * @param currentTime
   * @param startDateEndDateMap
   * @param activeAttributeID
   * @param languageID
   * 
   * @return List of period IDs needs to activated
   */
  public List<String> getActivatePeriods(String currentTime, Map<String, String> startDateEndDateMap, String activeAttributeID, String languageID) throws Exception;


  /**
   * Gets the list of products where any period ID is attached under any refToPIMAttributeID
   * 
   * filters : Searches periods IDs in all reference to PIM attribute
   *  
   * @param periodsIDs
   * @param refToPIMAttributeID
   * @param languageID
   * 
   * @return List of colors in which given periods are attached
   */
  public List<String> searchColoursForPeriods(
      List<String> periodsIDs,
      List<String> refToPIMAttributeID,
      String languageID, 
      String itemTypeAttributeID,
      String itemTypeID
  ) throws Exception;


  /**
   * External keys of the nodes present under a folder
   * 
   * @param folderID
   * @param languageID
   * 
   * @return List of children IDs
   */
  public List<String> getChildrenIDByFolderID(
      String folderID,
      String languageID, 
      String itemTypeAttributeID,
      String itemTypeProductID
  ) throws Exception;


  /**
   * Gets colors in which given periods are linked
   * 
   * @param periodsIDs
   * @param refToPIMAttributeID
   * @param languageID
   * 
   * @return Map<ColorID => <ReferenceToPimAttribueID => PeriodID>>
   * 
   * @throws Exception
   */
  public Map<String, Map<String, List<String>>> getColoursForPeriods(
      List<String> periodsIDs,
      List<String> refToPIMAttributeID,
      String languageID, 
      String itemTypeAttributeID,
      String itemTypeID
  ) throws Exception;
}
