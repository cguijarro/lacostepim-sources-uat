package com.lacoste.periods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.exportstaging.api.ExternalItemAPI;
import com.exportstaging.api.wraper.ItemWrapper;

import com.lacoste.api.LEPublicationListApi;
import com.lacoste.api.LEPublicationListConstants;
import com.lacoste.api.LEPublicationListException;
import com.lacoste.resources.LEPdmarticleColor;
import com.lacoste.resources.LEPdmarticlePeriod;
import com.lacoste.resources.LEPdmarticleSize;


/**
 * Deactivate period
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  PublicationList
 */
public class LEPublicationListDeactivatePeriod extends LEPublicationListPeriod
{

  /**
   * Default constructor
   */
  public LEPublicationListDeactivatePeriod() throws LEPublicationListException
  {
    super();
  }


  @Override
  public ArrayList<LEPdmarticlePeriod> getContextPeriods() throws LEPublicationListException
  {
    ArrayList<LEPdmarticlePeriod> dectivatePeriods = new ArrayList<>();
    ExternalItemAPI externalItemAPI;
    try
    {
      externalItemAPI = LEPublicationListApi.getExternalItemEsaAPI();

      String endDateAttr       = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_END_DATE);
      String activeAttributeID = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_ACTIVE);

      ArrayList<String> endDateAttributeIDs = new ArrayList<String>();
      endDateAttributeIDs.addAll(Arrays.asList(endDateAttr.split(",")));

      String currentTime = LEPublicationListApi.getDate("yyyy-MM-dd");

      List<String> dectivatedPeriodsIds = LEPublicationListApi.getElasticsearchApi().getDeactivatePeriods(
        currentTime,
        endDateAttributeIDs,
        activeAttributeID,
        LEPublicationListApi.LANGUAGE_ID
      );

      LEPublicationListApi.setDeactivatedPeriods(dectivatedPeriodsIds);

      LEPublicationListApi.log("Periods to be Deactivated - " + Arrays.deepToString(dectivatedPeriodsIds.toArray()));

      for(String periodID : dectivatedPeriodsIds)
      {
        ItemWrapper periodItem = externalItemAPI.getItemById(Long.parseLong(periodID));

        if(periodItem != null)
        {
          String externalKey = periodItem.getExternalKey();
  
          if(!externalKey.isEmpty())
          {
            LEPdmarticlePeriod period = new LEPdmarticlePeriod();
            String label = periodItem.getValue("Label");
  
            period.setID(periodID);
            period.setLabel(label, LEPublicationListApi.LANGUAGE_CODE);
            period.setIsFolder(periodItem.getValue("IsFolder"));
            period.setItemExternalKey(externalKey);
            period.setActive(0);
  
            dectivatePeriods.add(period);
          }
          else
          {
            LEPublicationListApi.log("Period ID '" + periodID + "' do not have external key");
          }
        }
        else
        {
          LEPublicationListApi.log("Period ID '" + periodID + "' do not present in export database");
        }
      }
    }
    catch(Exception e)
    {
      throw new LEPublicationListException(0, "Get Periods for 'Deactivate' case failed due to " + e.getMessage());
    }

    return dectivatePeriods;
  }


  @Override
  public ArrayList<LEPdmarticleColor> getColors(ArrayList<LEPdmarticlePeriod> periods) throws LEPublicationListException
  {
    ArrayList<LEPdmarticleColor> colors = new ArrayList<>();
    ExternalItemAPI itemAPI             = null;
    try
    {
      itemAPI = LEPublicationListApi.getExternalItemEsaAPI();

      ArrayList<String> periodsIDs = new ArrayList<>();
      HashMap<String, String> periodIdLabelCache = new HashMap<>();
      HashMap<String, LEPdmarticlePeriod> periodCache = new HashMap<>();

      for(LEPdmarticlePeriod period : periods)
      {
        String periodID = period.getID();
        periodsIDs.add(periodID);
        periodIdLabelCache.put(periodID, period.getLanguageLabels().get(LEPublicationListApi.LANGUAGE_CODE));
        periodCache.put(periodID, period);
      }

      ArrayList<String> referenceToPIMAttributeIDs = new ArrayList<>();

      String activePeriodDemandwareEuId  = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_ACTIVE_DWARE_EU);
      String activePeriodDemandwareNaId  = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_ACTIVE_DWARE_NA);
      String activePeriodDemandwareLaId  = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_ACTIVE_DWARE_LA);
      String activePeriodMarketplaceEuId = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_ACTIVE_MPLACE_EU);
      String activePeriodMarketplaceNaId = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_ACTIVE_MPLACE_NA);

      referenceToPIMAttributeIDs.add(activePeriodDemandwareEuId);
      referenceToPIMAttributeIDs.add(activePeriodDemandwareNaId);
      referenceToPIMAttributeIDs.add(activePeriodDemandwareLaId);
      referenceToPIMAttributeIDs.add(activePeriodMarketplaceEuId);
      referenceToPIMAttributeIDs.add(activePeriodMarketplaceNaId);

      String itemTypeAttributeID = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_ITEMTYPE);
      String itemTypeProductID = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.ITEMTYPE_COLOR);

      Map<String, Map<String, List<String>>> deactivateColors = 
      LEPublicationListApi.getElasticsearchApi().getColoursForPeriods(
        periodsIDs,
        referenceToPIMAttributeIDs,
        LEPublicationListApi.LANGUAGE_ID,
        itemTypeAttributeID,
        itemTypeProductID
      );

      Set<String> deactivateColorIds = deactivateColors.keySet();

      LEPublicationListApi.log("Colors for Deactivate case - " + Arrays.deepToString(deactivateColorIds.toArray()));

      for(String colorID : deactivateColorIds)
      {
        ItemWrapper item = itemAPI.getItemById(Long.parseLong(colorID));

        String tempArchievedPeriods = "";
        Set<String> archievedPeriodsIds = new HashSet<String>();

        if(item != null)
        {
          LEPdmarticleColor color = new LEPdmarticleColor();
          String archivedPeriods  = item.getValue(LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_ARCHIVED));
          String externalKey      = item.getExternalKey();
          String label            = item.getValue("Label");

          color.setID(colorID);
          color.setItemExternalKey(externalKey);
          color.setIsFolder(item.getValue("IsFolder"));
          color.setLabel(label, LEPublicationListApi.LANGUAGE_CODE);

          Map<String, List<String>> targets = deactivateColors.get(colorID);
          Set<String> attributeIDs = targets.keySet();

          for(String attrID : attributeIDs)
          {
            List<String> targetsIDs = deactivateColors.get(colorID).get(attrID);

            for(String periodID : targetsIDs)
            {
              this.removePeriod(periodCache.get(periodID), color, attrID);

              if(!archievedPeriodsIds.contains(periodID))
              {
                archievedPeriodsIds.add(periodID);
                if(tempArchievedPeriods.trim().isEmpty())
                {
                  tempArchievedPeriods = periodIdLabelCache.get(periodID);
                }
                else
                {
                  tempArchievedPeriods = (tempArchievedPeriods + "\n" + periodIdLabelCache.get(periodID));
                }
              }
            }
          }

          if(archivedPeriods.trim().isEmpty())
          {
            archivedPeriods = tempArchievedPeriods;
          }
          else
          {
            archivedPeriods = ( archivedPeriods + "\n" + tempArchievedPeriods);
          }
          color.setArchivedPeriods(archivedPeriods);

          colors.add(color);
        }
      }
    }
    catch(Exception exception)
    {
      LEPublicationListApi.printStackTrace(exception);
      throw new LEPublicationListException(0, "Get colors for 'Deactivate' case failed due to - " + exception.getMessage());
    }

    return colors;
  }


  /**
   * Processing Deactivate periods case
   */
  public void start()
  {
    LEPublicationListApi.log("//////////////////////////Started processing Deactivate Period//////////////////////");
    try
    {
      ArrayList<LEPdmarticlePeriod> deactivatePeriodsList = this.getContextPeriods();

      ArrayList<LEPdmarticleColor> deactivateColorsList = this.getColors(deactivatePeriodsList);

      for(LEPdmarticlePeriod period : deactivatePeriodsList)
      {
        LEPublicationListApi.storeItem(period);
      }

      for(LEPdmarticleColor color : deactivateColorsList)
      {

        LEPublicationListApi.storeItem(color);

        ArrayList<LEPdmarticleSize> sizeList = this.getSizes(color);

        LEPublicationListApi.log("Sizes for Color ID " + color.getID() + ": " + this.getSizeList());

        if(sizeList != null && sizeList.size() != 0)
        {
          for(LEPdmarticleSize size : sizeList)
          {
            LEPublicationListApi.storeItem(size);
          }
        }
      }
    }
    catch(Exception e)
    {
      LEPublicationListApi.log(e.getMessage());
      LEPublicationListApi.printStackTrace(e);
    }
    finally
    {
      LEPublicationListApi.log("//////////////////////////End processing Deactivate Period//////////////////////////");
    }
  }
}
