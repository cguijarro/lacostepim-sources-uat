package com.lacoste.api;

import java.util.ArrayList;

import com.lacoste.resources.LEPdmarticlePeriod;
import com.lacoste.resources.LEPdmarticleColor;
import com.lacoste.resources.LEPdmarticleSize;

/**
 * Provides general functions to be implemented to handle Period case
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  PublicationList
 */
public interface ILEPublicationListPeriod
{


  /**
   * Gets the Periods for the specified criteria
   * 
   * @return List of Periods
   */
  public ArrayList<LEPdmarticlePeriod> getContextPeriods() throws LEPublicationListException;


  /**
   * Gets the list of Color Objects for given periods
   * 
   * @param periods List of Periods
   * 
   * @return List of Color Objects 
   * @throws com.lacoste.api.LEPublicationListException 
   */
  public ArrayList<LEPdmarticleColor> getColors(ArrayList<LEPdmarticlePeriod> periods) throws LEPublicationListException;


  /**
   * Gets the sizes of given Color PIM object
   * 
   * @param color
   * 
   * @return List of sizes
   * @throws com.lacoste.api.LEPublicationListException
   */
  public ArrayList<LEPdmarticleSize> getSizes(LEPdmarticleColor color) throws LEPublicationListException;


  /**
   * Perform false update of object on ISA database
   * 
   * @param size Size PIM item for false Update
   */
  public void falseUpdateSize(LEPdmarticleSize size);


  /**
   * Sets the Active Flag on Period
   * 
   * @param period Period object to set the active flag
   * 
   * @param isPeriodActive true or false
   */
  public void setActiveFlag(LEPdmarticlePeriod period, boolean isPeriodActive);


  /**
   * Sets the period in Color object under given reference to PIM attribute
   * 
   * @param period Period object
   * @param color  Color Object
   * @param attribute Reference to PIM attribute ID
   * @throws com.lacoste.api.LEPublicationListException
   */
  public void setPeriod(LEPdmarticlePeriod period, LEPdmarticleColor color, String attribute)throws LEPublicationListException;


  /**
   * Removes given Period from the Color Object
   * 
   * @param period Period object
   * @param color Color Object
   * @param attribute Reference to PIM attribute ID
   * @throws com.lacoste.api.LEPublicationListException
   */
  public void removePeriod(LEPdmarticlePeriod period, LEPdmarticleColor color, String attribute) throws LEPublicationListException;


  /**
   * Sets the period in Color object under given reference to PIM attribute
   * 
   * @param period Period object
   * @param color  Color Object
   * @param attribute Reference to PIM attribute ID
   */
  public void setAttribute(LEPdmarticlePeriod period, LEPdmarticleColor color, String attribute);
}
