package com.lacoste.resources;

import java.util.HashMap;

import com.importstaging.api.domain.Item;
import com.lacoste.api.LEPublicationListApi;


/**
 * PIM item to be inserted in Import database
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  PublicationList
 */
public class LEPdmarticle extends Item
{

  /**
   * Product ID
   */
  String pdmarticleID;


  /**
   * Gets the product ID
   * 
   * @return ID of Product
   */
  public String getID()
  {
    return pdmarticleID;
  }


  /**
   * Sets the product ID
   * 
   * @param productID Product ID
   */
  public void setID(String productID)
  {
    pdmarticleID = productID;
  }


  /**
   * Sets the Attribute value to current time stamp to update the Size.
   * 
   * @param attributeExternalKey External key of attribute
   */
  public void setFalseImportAttribute(String attributeExternalKey)
  {
    String currentTime = LEPublicationListApi.getUnixTimestamp(null);
    HashMap<String, String> attributeValues = new HashMap<>();
    attributeValues.put(attributeExternalKey, currentTime);
    this.setAttributes(
      attributeValues,
      LEPublicationListApi.LANGUAGE_CODE
    );
  }
}
