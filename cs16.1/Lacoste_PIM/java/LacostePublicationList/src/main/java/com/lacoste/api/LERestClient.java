package com.lacoste.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  PublicationList
 */
public class LERestClient
{

  /**
   * Starts Import database job
   * 
   * @throws LEPublicationListException on failure
   */
  public static void start() throws LEPublicationListException
  {
    try
    {
      String restUrl = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.IMPORT_DATABASE_RESTSERVICE_URL);
      String user    = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.CONTENTSERV_RESTSERVICE_USER);
      String pass    = LEPublicationListApi.base64Decode(LEPublicationListApi.getPropertyValue(LEPublicationListConstants.CONTENTSERV_RESTSERVICE_PASSWORD));
      String jobID   = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.IMPORT_DATABASE_JOB_ID);
      int iJobID     = Integer.parseInt(jobID);
      LEPublicationListApi.log("Started Import database Job");
      executeImportDatabaseJob(restUrl, iJobID, user, pass);
      LEPublicationListApi.log("Finished Import database Job");
    }
    catch (LEPublicationListException e)
    {
      throw new LEPublicationListException(LEPublicationListException.LE_EXCEPTION_EXCECUTE_IMPORT_JOB_FAILED, e.getMessage());
    }
  }


  /**
   * Execute Import database Job using REST web service
   * 
   * @param restUrl      REST service URL
   * @param jobID        Import database Job ID
   * @param ctsUser      User name
   * @param ctsPasssword User Password
   * 
   * @return void
   * @throws LEPublicationListException on REST service not reachable
   */
  private static void executeImportDatabaseJob(
    String restUrl,
    int jobID,
    String ctsUser,
    String ctsPasssword
  ) throws LEPublicationListException
  {
    HttpURLConnection httpURLConnection = null;
    BufferedReader bufferedReader       = null;
    try
    {
      String restServiceUrl = restUrl + "/" + jobID + "?ctsUser=" + ctsUser + "&ctsPassword=" + ctsPasssword;

      LEPublicationListApi.log("Executing Import database Job ID- " + jobID);

      URL url = new URL(restServiceUrl);      
      httpURLConnection = (HttpURLConnection) url.openConnection();
      httpURLConnection.setRequestMethod("GET");
      httpURLConnection.setConnectTimeout(0);
      httpURLConnection.setReadTimeout(0);

      if (httpURLConnection.getResponseCode() != 200)
      {
        String message = "Execute Import database Job failed. Http response "
          + httpURLConnection.getResponseCode() + " " 
          + httpURLConnection.getResponseMessage();

        LEPublicationListApi.log(message);
        throw new LEPublicationListException(LEPublicationListException.LE_EXCEPTION_EXCECUTE_IMPORT_JOB_FAILED);
      }

      bufferedReader = new BufferedReader(
        new InputStreamReader(
          (httpURLConnection.getInputStream())
        )
      );

      String restResponse = "";
      String output;
      while ((output = bufferedReader.readLine()) != null)
      {
        restResponse += output;
      }

      LEPublicationListApi.log(restResponse);
    }
    catch (MalformedURLException e)
    {
      throw new LEPublicationListException(LEPublicationListException.LE_EXCEPTION_EXCECUTE_IMPORT_JOB_FAILED, e.getMessage());
    }
    catch (IOException e)
    {
      throw new LEPublicationListException(LEPublicationListException.LE_EXCEPTION_EXCECUTE_IMPORT_JOB_FAILED, e.getMessage());
    }
    finally
    {
      try
      {
        if(bufferedReader != null)
        {
          bufferedReader.close();
        }
        if(httpURLConnection != null)
        {
          httpURLConnection.disconnect();
        }
      }
      catch(IOException e)
      {
        LEPublicationListApi.printStackTrace(e);
      }
    }
  }


  /**
   * Gets the History of Product last version
   * 
   * @param restUrl
   * @param ctsUser
   * @param ctsPasssword
   * 
   * @return
   * 
   * @throws LEPublicationListException
   */
  private static String executeHistoryWebservice(
    String restUrl,
    String ctsUser,
    String ctsPasssword
  ) throws LEPublicationListException
  {
    HttpURLConnection httpURLConnection = null;
    BufferedReader bufferedReader       = null;
    String restResponse                 = "";
    try
    {
      String restServiceUrl = restUrl + "/" + "?ctsUser=" + ctsUser + "&ctsPassword=" + ctsPasssword;

      URL url = new URL(restServiceUrl);
      httpURLConnection = (HttpURLConnection) url.openConnection();
      httpURLConnection.setRequestMethod("GET");
      httpURLConnection.setConnectTimeout(0);
      httpURLConnection.setReadTimeout(0);

      if (httpURLConnection.getResponseCode() != 200)
      {
        throw new LEPublicationListException(LEPublicationListException.LE_EXCEPTION_HISTORY_REST_SERVICE, httpURLConnection.getResponseMessage());
      }

      bufferedReader = new BufferedReader(
        new InputStreamReader(
          (httpURLConnection.getInputStream())
        )
      );

      String output;
      while ((output = bufferedReader.readLine()) != null)
      {
        restResponse += output;
      }
    }
    catch (LEPublicationListException | IOException e)
    {
      LEPublicationListApi.printStackTrace(e);
      throw new LEPublicationListException(LEPublicationListException.LE_EXCEPTION_HISTORY_REST_SERVICE, e.getMessage());
    }
    finally
    {
      try
      {
        if(bufferedReader != null)
        {
          bufferedReader.close();
        }
        if(httpURLConnection != null)
        {
          httpURLConnection.disconnect();
        }
      }
      catch(IOException e)
      {
        LEPublicationListApi.printStackTrace(e);
      }
    }
    return restResponse;
  }


  /**
   * Gets product history by ID
   * 
   * @param periodID
   * 
   * @return
   * 
   * @throws LEPublicationListException
   */
  public static String getProductHistoryByID(String periodID) throws LEPublicationListException
  {
    String restUrl      = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.PRODUCT_HISTORY_RESTSERVICE_URL) 
        + "/" + periodID + "/" 
        + "?expand=Changes";

    String ctsUser        = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.CONTENTSERV_RESTSERVICE_USER);
    String ctsPasssword   = LEPublicationListApi.base64Decode(LEPublicationListApi.getPropertyValue(LEPublicationListConstants.CONTENTSERV_RESTSERVICE_PASSWORD));
    String restServiceUrl = restUrl + "&ctsUser=" + ctsUser + "&ctsPassword=" + ctsPasssword;
    String response       = executeHistoryWebservice(restServiceUrl, ctsUser, ctsPasssword);

    return response;
  }
}
