package com.lacoste.periods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.exportstaging.api.ExternalItemAPI;
import com.exportstaging.api.wraper.ItemWrapper;
import com.lacoste.api.LEPublicationListApi;
import com.lacoste.api.LEPublicationListConstants;
import com.lacoste.api.LEPublicationListException;
import com.lacoste.resources.LEPdmarticleColor;
import com.lacoste.resources.LEPdmarticlePeriod;
import com.lacoste.resources.LEPdmarticleSize;


/**
 * Activate Period- This class contains APIs to handle the Activate period case
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  PublicationList
 */
public class LEPublicationListActivatePeriod extends LEPublicationListPeriod
{


  public LEPublicationListActivatePeriod() throws LEPublicationListException
  {
    super();
  }


  @Override
  public ArrayList<LEPdmarticlePeriod> getContextPeriods() throws LEPublicationListException
  {
    ArrayList<LEPdmarticlePeriod> activatePeriods = new ArrayList<>();
    try
    {
      ExternalItemAPI itemAPI = LEPublicationListApi.getExternalItemEsaAPI();

      String currentTime = LEPublicationListApi.getDate("yyyy-MM-dd");

      if(itemAPI != null)
      {
        HashMap<String, String> startDateEndDateMap = getStartDateEndDateMap();

        List<String> activePeriodsResultSet = LEPublicationListApi.getElasticsearchApi().getActivatePeriods(
          currentTime,
          startDateEndDateMap,
          LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_ACTIVE),
          LEPublicationListApi.LANGUAGE_ID
        );

        LEPublicationListApi.log("Periods for Activate case - " + Arrays.deepToString(activePeriodsResultSet.toArray()));

        for(String itemID : activePeriodsResultSet)
        {
          long periodID = Long.parseLong(itemID);
          ItemWrapper itemWrapper = itemAPI.getItemById(periodID);

          if(!LEPublicationListApi.isNull(itemWrapper))
          {
            String externalKey      = itemWrapper.getExternalKey();

            if(!LEPublicationListApi.isEmpty(externalKey))
            {
              Set<String> activeAttributeIds = this.getChannelForPeriod(itemWrapper);
              if(activeAttributeIds.size() == 0)
              {
                LEPublicationListApi.log("Active Period ID: " + itemID + " has invalid channel, hence it will be skipped.");
                continue;
              }
              String label              = itemWrapper.getValue("Label");
              LEPdmarticlePeriod period = new LEPdmarticlePeriod();
              period.setIsFolder(itemWrapper.getValue("IsFolder"));
              period.setID(itemID);
              period.setItemExternalKey(externalKey);
              period.setLabel(label, LEPublicationListApi.LANGUAGE_CODE);
              period.setActiveAttributeIds(activeAttributeIds);
              period.setActive(1);
              activatePeriods.add(period);
            }
            else
            {
              LEPublicationListApi.log("Activate Period ID: " + itemID + " do not have external key.");
            }
          }
          else
          {
            LEPublicationListApi.log("Activate Period ID: " + itemID + " do not found in export database.");
          }
        }
      }
    }
    catch(Exception e)
    {
      throw new LEPublicationListException(0, "Get Periods for 'Activate' case failed due to " + e.getMessage());
    }

    return activatePeriods;
  }


  /**
   * Gets the start date end date MAP from the properties file
   * 
   * @return Map - {StartDate1 => EndDate2, ... , StartDateN => EndDateN}
   * 
   * @throws LEPublicationListException On invalid configuration for Begin date & End date
   */
  private static HashMap<String, String> getStartDateEndDateMap() throws LEPublicationListException
  {
    String beginDate = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_BEGIN_DATE);
    String endDate   = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_END_DATE);

    String beginDates[] = beginDate.split(",");
    String endDates[]   = endDate.split(",");

    if(beginDate.length() != endDate.length())
    {
      throw new LEPublicationListException(LEPublicationListException.LE_EXCEPTION_INVALID_CONFIG, "Begin Date & End Date attribute list count missmatch");
    }

    HashMap<String, String> startDateEndDateMap = new HashMap<>();

    for(int i=0; i<endDates.length; i++)
    {
      startDateEndDateMap.put(beginDates[i], endDates[i]);
    }

    return startDateEndDateMap;
  }


  @Override
  public ArrayList<LEPdmarticleColor> getColors(ArrayList<LEPdmarticlePeriod> periods) throws LEPublicationListException
  {
    ArrayList<LEPdmarticleColor> colors = new ArrayList<>();
    ExternalItemAPI itemAPI             = null;
    try
    {
      itemAPI = LEPublicationListApi.getExternalItemEsaAPI();

      ArrayList<String> periodsIDs = new ArrayList<>();
      HashMap<String, Set<String>> activeAttributesIDs = new HashMap<>();

      for(LEPdmarticlePeriod period : periods)
      {
        String periodID = period.getID();
        periodsIDs.add(periodID);
        activeAttributesIDs.put(periodID, period.getActiveAttributeIds());
      }

      ArrayList<String> refToPIMAttributeIDs = new ArrayList<>();

      String scheduledPeriodDemandwareEu  = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_SCHED_DWARE_EU);
      String scheduledPeriodDemandwareNa  = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_SCHED_DWARE_NA);
      String scheduledPeriodDemandwareLa  = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_SCHED_DWARE_LA);
      String scheduledPeriodMarketplaceEu = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_SCHED_MPLACE_EU);
      String scheduledPeriodMarketplaceNa = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_SCHED_MPLACE_NA);

      refToPIMAttributeIDs.add(scheduledPeriodDemandwareEu);
      refToPIMAttributeIDs.add(scheduledPeriodDemandwareNa);
      refToPIMAttributeIDs.add(scheduledPeriodDemandwareLa);
      refToPIMAttributeIDs.add(scheduledPeriodMarketplaceEu);
      refToPIMAttributeIDs.add(scheduledPeriodMarketplaceNa);

      String itemTypeAttributeID = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_ITEMTYPE);
      String itemTypeProductID = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.ITEMTYPE_COLOR);

      Map<String, Map<String, List<String>>> activateColors = 
      LEPublicationListApi.getElasticsearchApi().getColoursForPeriods(
        periodsIDs,
        refToPIMAttributeIDs,
        LEPublicationListApi.LANGUAGE_ID,
        itemTypeAttributeID,
        itemTypeProductID
      );

      LEPublicationListApi.log("Colors for Activate case - " + Arrays.deepToString(activateColors.keySet().toArray()));

      Set<String> activateColorIds = activateColors.keySet();

      for(String colorID : activateColorIds)
      {
        ItemWrapper item = itemAPI.getItemById(Long.parseLong(colorID));

        if(!LEPublicationListApi.isNull(item))
        {
          LEPdmarticleColor color = new LEPdmarticleColor();

          String externalKey = item.getExternalKey();
          String label       = item.getValue("Label");

          color.setIsFolder(item.getValue("IsFolder"));
          color.setID(colorID);
          color.setItemExternalKey(externalKey);
          color.setLabel(label, LEPublicationListApi.LANGUAGE_CODE);

          Map<String, List<String>> targets = activateColors.get(colorID);
          Set<String> attributeIDs = targets.keySet();

          for(String attrID : attributeIDs)
          {
            List<String> targetsIDs = activateColors.get(colorID).get(attrID);
            String attrExternaKey = LEPublicationListApi.getAttributeExternalKeyById(attrID);

            List<Map<String, Map<String, String>>> targetIDs =new ArrayList<>();



            for(String periodID : targetsIDs)
            {
              Set<String> activeAttributes = activeAttributesIDs.get(periodID);//period.getActiveAttributeExternalKey();

              periodID = LEPublicationListApi.getProductExternalKeyById(periodID);

              Map<String, Map<String, String>> referenceIDs = new HashMap<String, Map<String, String>>();
              Map<String, String> referenceAttributes = new HashMap<String, String>();

              referenceAttributes.put(
                "EXTERNALKEY",
                getReferenceExternalKeyByFormat(externalKey, periodID, attrExternaKey, attrID)
              );

              referenceIDs.put(periodID, referenceAttributes);
              targetIDs.add(referenceIDs);

              LEPdmarticlePeriod period = new LEPdmarticlePeriod();
              period.setItemExternalKey(periodID);

              for(String activeAttributeExternalKey : activeAttributes)
              {
                this.setPeriod(period, color, activeAttributeExternalKey);
              }
            }

            color.removeItemReferences(
              attrExternaKey,
              LEPublicationListApi.LANGUAGE_CODE,
              targetIDs
            );
          }

          colors.add(color);
        }
        else
        {
          LEPublicationListApi.log("Color ID '" + colorID + "' do not found in export database.");
        }
      }
    }
    catch(Exception exception)
    {
      LEPublicationListApi.printStackTrace(exception);
      throw new LEPublicationListException(0, "Get Colors for 'Activate' case failed due to " + exception.getMessage());
    }

    return colors;
  }


  /**
   * Processing activate periods case
   */
  public void start()
  {
    LEPublicationListApi.log("//////////////////////////Started processing Activate Period////////////////////////");
    try
    {
      ArrayList<LEPdmarticlePeriod> periodsList = this.getContextPeriods();

      ArrayList<LEPdmarticleColor> colorsList = this.getColors(periodsList);

      for(LEPdmarticlePeriod period : periodsList)
      {
        LEPublicationListApi.storeItem(period);
      }

      for(LEPdmarticleColor color : colorsList)
      {
        LEPublicationListApi.storeItem(color);

        ArrayList<LEPdmarticleSize> sizes = this.getSizes(color);

        LEPublicationListApi.log("Sizes for Color ID " + color.getID() + ": " + this.getSizeList());

        for(LEPdmarticleSize size : sizes)
        {
          LEPublicationListApi.storeItem(size);
        }
      }
    }
    catch(Exception e)
    {
      LEPublicationListApi.log(e.getMessage());
      LEPublicationListApi.printStackTrace(e);
    }
    finally
    {
      LEPublicationListApi.log("//////////////////////////End processing Activate Period////////////////////////////");
    }
  }
}
