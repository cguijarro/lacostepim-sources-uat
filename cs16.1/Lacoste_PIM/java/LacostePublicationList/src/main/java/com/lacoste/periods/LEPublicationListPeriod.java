package com.lacoste.periods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import com.exportstaging.api.ExternalItemAPI;
import com.exportstaging.api.domain.Reference;
import com.exportstaging.api.exception.ExportStagingException;
import com.exportstaging.api.wraper.ItemWrapper;
import com.importstaging.api.exception.ImportStagingException;
import com.lacoste.api.ILEPublicationListPeriod;
import com.lacoste.api.LEPublicationListApi;
import com.lacoste.api.LEPublicationListConstants;
import com.lacoste.api.LEPublicationListException;
import com.lacoste.resources.LEPdmarticleColor;
import com.lacoste.resources.LEPdmarticlePeriod;
import com.lacoste.resources.LEPdmarticleSize;


/**
 * Base class provides basic functionality needed for JAVA Process
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  PublicationList
 */
public abstract class LEPublicationListPeriod implements ILEPublicationListPeriod
{

  private static String referenceExternalKeyFormat        = "";
  private static String referenceExternalDefaultKeyFormat = "";
  private String sizeList                                 = "";


  /**
   * Default constructor
   * @throws LEPublicationListException 
   */
  public LEPublicationListPeriod() throws LEPublicationListException
  {
    String deafultFormat = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.IMPORT_DATABASE_REFERENCE_EXTERNALKEY_DEFAULT_FORMAT);
    LEPublicationListPeriod.setReferenceExternalDefaultKeyFormat(deafultFormat);

    String format = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.IMPORT_DATABASE_REFERENCE_EXTERNALKEY_GENERAL_FORMAT);
    LEPublicationListPeriod.setReferenceExternalKeyFormat(format);
  }


  /**
   * Gets the size list
   * 
   * @return Size IDs separated with comma
   */
  public String getSizeList() {
    return sizeList;
  }


  /**
   * Sets the Size list
   * 
   * @param sizeList Size IDs separated with comma
   */
  protected void setSizeList(String sizeList) {
    this.sizeList = sizeList;
  }


  /**
   * Gets Reference external key format from config.properties
   * 
   * @return External key format
   */
  public static String getReferenceExternalKeyFormat() {
    return referenceExternalKeyFormat;
  }


  /**
   * Gets the default reference external key format from config.properties
   * 
   * @return External key format
   */
  public static String getReferenceExternalDefaultKeyFormat() {
    return referenceExternalDefaultKeyFormat;
  }


  /**
   * Sets default Reference external key format in instance variable
   * 
   * @param External key format
   */
  public static void setReferenceExternalDefaultKeyFormat(String referenceExternalDefaultKeyFormat) {
    LEPublicationListPeriod.referenceExternalDefaultKeyFormat = referenceExternalDefaultKeyFormat;
  }


  /**
   * Sets Reference external key format in instance variable
   * 
   * @param External key format
   */
  public static void setReferenceExternalKeyFormat(String referenceExternalKeyFormat) {
    LEPublicationListPeriod.referenceExternalKeyFormat = referenceExternalKeyFormat;
  }


  @Override
  public ArrayList<LEPdmarticleSize> getSizes(LEPdmarticleColor color) throws LEPublicationListException
  {
    ArrayList<LEPdmarticleSize> sizes = new ArrayList<LEPdmarticleSize>();
    ExternalItemAPI itemAPI = null;
    try
    {
      itemAPI = LEPublicationListApi.getExternalItemEsaAPI();

      String itemTypeAttributeID = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_ITEMTYPE);
      String itemTypeProductID = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.ITEMTYPE_SIZE);

      List<String> childrenIDs = LEPublicationListApi.getElasticsearchApi().getChildrenIDByFolderID(
          color.getID(),
          LEPublicationListApi.LANGUAGE_ID,
          itemTypeAttributeID,
          itemTypeProductID
      );

      this.setSizeList(Arrays.deepToString(childrenIDs.toArray()));

      String falseImportID = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_FALSE_IMPORT);
      String falseImportAttributeExKey = LEPublicationListApi.getAttributeExternalKeyById(falseImportID);

      if(childrenIDs.size() != 0)
      {
        sizes = new ArrayList<LEPdmarticleSize>();

        for(String sizeID : childrenIDs)
        {
          ItemWrapper item = itemAPI.getItemById(Long.parseLong(sizeID));
          if(item != null)
          {
            String itemExternalKey = item.getExternalKey();
            String label           = item.getValue("Label");
            if(!LEPublicationListApi.isEmpty(itemExternalKey))
            {
              LEPdmarticleSize size = new LEPdmarticleSize();
              size.setID(sizeID);
              size.setItemExternalKey(itemExternalKey);
              size.setLabel(label, LEPublicationListApi.LANGUAGE_CODE);
              size.setFalseImportAttribute(falseImportAttributeExKey);
              size.setIsFolder(item.getValue("IsFolder"));
              sizes.add(size);
            }
            else
            {
              LEPublicationListApi.log("Size ID: " + sizeID + " do not have external key.");
            }
          }
          else
          {
            LEPublicationListApi.log("Size ID: " + sizeID + " do not found in export database.");
          }
        }
      }
    }
    catch (Exception e)
    {
      throw new LEPublicationListException(0, "Get Sizes failed due to " + e.getMessage());
    }

    return sizes;
  }


  @Override
  public void falseUpdateSize(LEPdmarticleSize size)
  {
    try
    {
      LEPublicationListApi.storeItem(size);
    }
    catch (LEPublicationListException e)
    {
      e.printStackTrace();
    }
  }


  @Override
  public void setActiveFlag(LEPdmarticlePeriod period, boolean isPeriodActive)
  {
    int isActive = isPeriodActive ? 1 : 0;
    try {
      period.setActive(isActive);
    } catch (LEPublicationListException e) {
      LEPublicationListApi.printStackTrace(e);
    }
  }


  @Override
  public void setPeriod(LEPdmarticlePeriod period, LEPdmarticleColor color, String attribute)
  {
    try {
      ArrayList<String> targetIDs = new ArrayList<>();
      targetIDs.add(period.getItemExternalKey());

      List<Map<String, Map<String, String>>> references = new ArrayList<>();
      HashMap<String, String> referenceExternalKeyMap = new HashMap<>();
      String attrExternalKey = LEPublicationListApi.getAttributeExternalKeyById(attribute);
      referenceExternalKeyMap.put("EXTERNALKEY", getReferenceExternalKeyByFormat(
          color.getItemExternalKey(),
          period.getItemExternalKey(),
          attrExternalKey,
          attribute
        )
      );

      HashMap<String, Map<String, String>> referenceMap = new HashMap<>();
      referenceMap.put(period.getItemExternalKey(), referenceExternalKeyMap);
      references.add(referenceMap);

      color.setItemReferences(
        attrExternalKey,
        targetIDs,
        LEPublicationListApi.LANGUAGE_CODE,
        references 
      );
    }
    catch(ImportStagingException | LEPublicationListException e)
    {
      LEPublicationListApi.printStackTrace(e);
    }
  }

  @Override
  public void removePeriod(LEPdmarticlePeriod period, LEPdmarticleColor color, String attribute) throws LEPublicationListException
  {
    List<Map<String, Map<String, String>>> targetIDs =new ArrayList<>();

    String attrExternalKey = LEPublicationListApi.getAttributeExternalKeyById(attribute);

    Map<String, Map<String, String>> referenceIDs = new HashMap<String, Map<String, String>>();
    Map<String, String> referenceAttributes = new HashMap<String, String>();

    try
    {
      referenceAttributes.put(
        "EXTERNALKEY",
        getReferenceExternalKeyByFormat(
          color.getItemExternalKey(),
          period.getItemExternalKey(),
          attrExternalKey,
          attribute
        )
      );

      referenceIDs.put(period.getItemExternalKey(), referenceAttributes);
    }
    catch (ImportStagingException e)
    {
      LEPublicationListApi.printStackTrace(e);
    }

    targetIDs.add(referenceIDs);

    color.removeItemReferences(
      attrExternalKey,
      LEPublicationListApi.LANGUAGE_CODE,
      targetIDs
    );
  }


  @Override
  public void setAttribute(LEPdmarticlePeriod period, LEPdmarticleColor color, String attribute)
  {
  }


  /**
   * Gets the Reference external key by format given in config.properties
   * 
   * @param externalKey     Source Item's external key
   * @param periodID        Target item's external key
   * @param attrExternalkey Reference to PIM attribute's external key
   * @param attributeID     Attribute ID
   * 
   * @return Reference external key as per given format
   * @throws LEPublicationListException 
   */
  public String getReferenceExternalKeyByFormat(
      String externalKey,
      String periodID,
      String attributeExternalkey,
      String attributeID
  ) throws LEPublicationListException
  {
    String referenceExternalKey = LEPublicationListPeriod.referenceExternalDefaultKeyFormat;
    String customizedText       = LEPublicationListApi.getPropertyValue("CUSTOMIZEDTEXT_" + attributeID).toString();

    if(!customizedText.isEmpty())
    {
      referenceExternalKey = LEPublicationListPeriod.referenceExternalKeyFormat;
    }

    referenceExternalKey = referenceExternalKey.replaceAll(Pattern.quote("{ITEMID}"), externalKey);
    referenceExternalKey = referenceExternalKey.replaceAll(Pattern.quote("{ATTRIBUTEID}"), attributeExternalkey);
    referenceExternalKey = referenceExternalKey.replaceAll(Pattern.quote("{LANGUAGEID}"), LEPublicationListApi.LANGUAGE_CODE);
    referenceExternalKey = referenceExternalKey.replaceAll(Pattern.quote("{REFERENCEID}"), periodID);
    referenceExternalKey = referenceExternalKey.replaceAll(Pattern.quote("{CUSTOMIZEDTEXT}"), customizedText);

    return referenceExternalKey;
  }


  /**
   * Gets the channel for Period
   * 
   * @param itemWrapper ItemWrapper Object
   * 
   * @return Set of Active attribute IDs for a Period
   * 
   * @throws LEPublicationListException
   */
  public Set<String> getChannelForPeriod(ItemWrapper itemWrapper) throws LEPublicationListException
  {
    Set<String> attributeExternalKey = new HashSet<String>();
    try
    {
      int channelAttributeID = Integer.parseInt(LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_CHANNEL));
      List<Reference> channelReferenceList = itemWrapper.getReferencesByID(channelAttributeID);

      if(channelReferenceList.size() != 0)
      {

        for(int i = 0; i < channelReferenceList.size(); i++)
        {
          long channelID = channelReferenceList.get(i).getTargetID();
          ExternalItemAPI api = LEPublicationListApi.getExternalItemEsaAPI();
          ItemWrapper channel = api.getItemById(channelID);

          if(channel != null)
          {
            int channelActiveAttributeID = Integer.parseInt(LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_CHANNEL_ACTIVE_ATTR));
            String activeAttrID = channel.getValue(channelActiveAttributeID + "");

            if(!activeAttrID.trim().isEmpty())
            {
              String ids[] = activeAttrID.split(",");
              {
                for(String id : ids) {
                  attributeExternalKey.add(id);
                }
              }
            }
            else
            {
              return new HashSet<String>();
            }
          }
        }
      }
    }
    catch(ExportStagingException e)
    {
      throw new LEPublicationListException(0, "Get Channel for period - " + e.getMessage());
    }

    return attributeExternalKey;
  }
}
