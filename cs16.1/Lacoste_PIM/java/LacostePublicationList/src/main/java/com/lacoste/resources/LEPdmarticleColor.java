package com.lacoste.resources;

import java.util.HashMap;
import java.util.Map;

import com.lacoste.api.LEPublicationListApi;
import com.lacoste.api.LEPublicationListConstants;
import com.lacoste.api.LEPublicationListException;

/**
 * Color item to be inserted in Import database
 * 
 * @since    CS16.1
 * @category Lacoste_PIM
 * @package  PublicationList
 */
public class LEPdmarticleColor extends LEPdmarticle
{

  /**
   * List of archived periods
   */
  String archivedPeriods;
  private boolean storeColorFlag = false;


  /**
   * Default constructor
   */
  public LEPdmarticleColor()
  {
    super();
    archivedPeriods = "";
  }


  /**
   * Gets the archived period
   * 
   * @return List of archived period set before
   */
  public String getArchivedPeriods()
  {
    return archivedPeriods;
  }


  /**
   * Sets the archived period
   * 
   * @param archivedPeriods List of archived period separated by new line
   */
  public void setArchivedPeriods(String archivedPeriods) throws LEPublicationListException
  {
    if(!archivedPeriods.trim().isEmpty())
    {
      this.archivedPeriods = archivedPeriods;
      Map<String, String> attributeValues = new HashMap<String, String>();
      String attribute                    = LEPublicationListApi.getPropertyValue(LEPublicationListConstants.LE_ATTR_PERIOD_ARCHIVED);
      attribute                           = LEPublicationListApi.getAttributeExternalKeyById(attribute);
      attributeValues.put(attribute, this.archivedPeriods);
      this.setAttributes(attributeValues, LEPublicationListApi.LANGUAGE_CODE);
    }
  }


  /**
   * Gets the Store item flag
   * 
   * @return boolean TRUE if color must be stored in import database
   */
  public boolean isStoreColorFlag()
  {
    return storeColorFlag;
  }


  /**
   * Sets the store item flag
   * 
   * @param storeColorFlag TRUE if color must be stored in import database else FALSE
   */
  public void setStoreColorFlag(boolean storeColorFlag)
  {
    if(this.storeColorFlag == false)
    {
      this.storeColorFlag = storeColorFlag;
    }
  }
}
